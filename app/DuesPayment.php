<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DuesPayment extends Model
{
    protected $fillable = ['amount', 'payment_date', 'user_id'];
}
