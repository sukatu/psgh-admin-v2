<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model
{
    protected $fillable = ['target', 'message', 'type', 'created_by', 'update_by'];
}
