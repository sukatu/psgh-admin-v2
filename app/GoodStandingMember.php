<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodStandingMember extends Model
{
    public function member(){
      return $this->belongsTo('App\Member', 'Constituent_ID');
    }
}
