<?php
namespace App\Helper;

class PSGHHelper{

  public static function memberColumn($value){
    switch ($value) {
      case '1':
      return 'status';
      break;
      case '2':
      return 'Member_Type_Code';
      break;
      case '3':
      return 'Practice_Group_Association';
      break;
      case '4':
      return 'Interest_Group_Association';
      break;
      case '5':
      return 'Regional_Branch_Chapter';
      break;
      default:
      return 'Membership';
      break;
    }
  }
 public static function setSession($value, $data){
   \Session::put(['value'=> self::memberColumn($value), 'data'=> $data]);
 }

 public static function getSession(){
    return array(
          'value' => \Session::get('value'),
          'data' => \Session::get('data')
    );
 }
}
