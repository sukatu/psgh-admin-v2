<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GoodStandingMember;
use App\Member;
use App\EventRegistrant;
use App\DuesType;
use App\MemberDue;
use App\Transaction;
use App\DuesPayment;
use Illuminate\Support\Facades\Hash;
use App\PaymentTransaction;
use DB;
use App\MemberUser;
use App\User;
class AccountingController extends Controller
{

  public function index()
  {
    $members   = Member::where('status', 'Active')->get(['First_Name', 'Middle_Name', 'Last_Name', 'Constituent_ID', 'id']);
    return view('pages/admin/psgh-pages/good_standings', compact('members'));
  }

  public function registerPaid(Request $request){
    try {
      $good_standing = new GoodStandingMember();
      $good_standing->date = date('Y/m/d');
      $good_standing->registration_no = $request->input('constituent_id');
      $good_standing->amount = $request->input('amount');
      $good_standing->save();
      return back()->with('success', 'Saved!');
    } catch (\Exception $e) {
      return back()->with('error', $e);
    }

  }

  public function filter(Request $request){
    if( $request->input('value') == '1') {
      $title = 'Paid';
      $good_standings = GoodStandingMember::all();
      return view('pages/admin/psgh-pages/filter-good-standings', compact('good_standings', 'title'));
    }elseif($request->input('value') == '2'){
      $title = 'Not Paid';
      $members = Member::where('status', 'Active')->get();
      return view('pages/admin/psgh-pages/filter-good-standings', compact('members', 'title'));
    }elseif($request->input('value') == '3'){
      $title = 'Registered & Paid';
      $good_standings = GoodStandingMember::all();
      return view('pages/admin/psgh-pages/filter-good-standings', compact('good_standings', 'title'));
    }elseif ($request->input('value') == '4') {
      $title = 'Registered but not Paid';
      $reg_mems = EventRegistrant::all();
      return view('pages/admin/psgh-pages/filter-good-standings', compact('reg_mems', 'title'));
    }elseif ($request->input('value') == '5') {
      $title = 'Not Registered but Paid';
      $good_standings = GoodStandingMember::all();
      return view('pages/admin/psgh-pages/filter-good-standings', compact('good_standings', 'title'));
    }elseif ($request->input('value') == '6') {
      $title = 'Not Registered & Not Paid';
      $members = Member::where('status', 'Active')->get();
      return view('pages/admin/psgh-pages/filter-good-standings', compact('members', 'title'));
    }

  }
  public function dues_types(){
    $dues_types = DuesType::all();
    return view('pages/admin/psgh-pages/dues-types', compact('dues_types'));
  }

  public function payment(){
    $to = '';
    $from = '';
    $memberDues = MemberDue::all();
    $transactions = Transaction::all();
    $duesPayments = DuesPayment::all();
    $paymentTransactions = PaymentTransaction::all();

    return view('pages/admin/psgh-pages/payment-received', compact('memberDues', 'transactions', 'duesPayments', 'paymentTransactions','to', 'from'));
  }

  public function filterReport(Request $request){
    $data = $request->all();
    $from = $request->input('from');
    $to = $request->input('to');
    $memberDues = MemberDue::whereBetween('created_at', array($data['from'], $data['to']))->get();;
    $transactions = Transaction::whereBetween('created_at', array($data['from'], $data['to']))->get();;
    $duesPayments = DuesPayment::whereBetween('created_at', array($data['from'], $data['to']))->get();;
    $paymentTransactions = PaymentTransaction::whereBetween('created_at', array($data['from'], $data['to']))->get();;

    return view('pages/admin/psgh-pages/payment-received', compact('memberDues', 'transactions', 'duesPayments', 'paymentTransactions', 'from', 'to'));
  }
  public static function iPay($transID){
    $url = "https://community.ipaygh.com/v1/gateway/json_status_chk?invoice_id=$transID&merchant_key=0731a7ce-126c-11ea-ac9a-f23c9170642f";
    $ch = curl_init($url); // such as http://example.com/example.xml
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    $response_data = json_decode($data, true);
    foreach ($response_data as $response_datum) {
      return $response_datum;
    }
  }

  public function addPayment(Request $request){
    DB::begintransaction();
    $data = $request->all();
    try {
      $member = Member::where('Constituent_ID', $data['constituent_id'])->first();
      $newUser = MemberUser::where('username', 'psgh'.$data['constituent_id'])->first();

      if ($newUser == null) {
        $newUser = MemberUser::create(
          [
            'name' => $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name,
            'email' => $member->Email_Address,
            'password' => Hash::make($data['constituent_id'].'psgh'),
            'username' => 'psgh'.$data['constituent_id'],
            'member_id' => $member->id,
          ]
        );
      }else{
        $duesPayment = DuesPayment::where('user_id', $newUser->id)->first();
        if (!$duesPayment == null) {
          return response()->json(
            [
              'type' => 'warning',
              'payload' => 'Payment already received.'
            ]
          );
          DB::rollback();
        }
      }

    } catch (\Exception $e) {
      return response()->json(
        [
          'type' => 'error',
          'payload' => 'ERROR creating user'
        ]
      );
      DB::rollback();
      throw $e;

    }

    try {
      $transactions = Transaction::where('user_id', $newUser->id)->first();
      if ($transactions == null) {
        $newTransaction = Transaction::create(
          [
            'trans_id' => $data['reference_no'],
            'user_id' => $newUser->id,
            'amount' => $data['amount'],
            'payment_date' => $data['payment_date'],
            'status' => 'paid'
          ]
        );
      }
    } catch (\Exception $e) {
      return response()->json(
        [
          'type' => 'error',
          'payload' => 'ERROR creating transaction'
        ]
      );
      DB::rollback();
      throw $e;

    }

    try {
      $paymentTransaction = PaymentTransaction::where('member_id', $member->id)->first();
      if ($paymentTransaction == null) {
        $newPaymentTransaction = PaymentTransaction::create(
          [
            'member_id' => $member->id,
            'description' => 'Being payment for annual association dues',
            'trans_type' => $data['bank'],
            'bank' => $data['bank'],
            'reference' => $data['reference_no'],
            'amount' => $data['amount'],
            'payment_date' => $data['payment_date'],
          ]
        );
      }
    } catch (\Exception $e) {
      return response()->json(
        [
          'type' => 'error',
          'payload' => 'ERROR creating payment transaction'
        ]
      );
      DB::rollback();
      throw $e;
    }

    try {
      $duesPayment = DuesPayment::where('user_id', $newUser->id)->first();
      if ($duesPayment == null) {
        $newDuesPayment = DuesPayment::create(
          [
            'amount' => $data['amount'],
            'payment_date' => $data['payment_date'],
            'user_id' => $newUser->id,
          ]
        );
      }else{
        return response()->json(
          [
            'type' => 'warning',
            'payload' => 'Payment already received.'
          ]
        );
        DB::rollback();
      }
    } catch (\Exception $e) {
      return response()->json(
        [
          'type' => 'error',
          'payload' => 'ERROR creating dues payment'
        ]
      );
      DB::rollback();
      throw $e;
    }


    DB::commit();

    return response()->json(
      [
        'type' => 'success',
        'payload' => 'Record saved successfully'
      ]
    );
  }
}
