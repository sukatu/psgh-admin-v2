<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
class Flexipay extends Controller
{
  function toFlexiipay($url,$param)
  {
    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-Type:  application/json; charset=utf-8';

    $ch = curl_init();
    // Disable SSL verification

    curl_setopt($ch, CURLOPT_URL,$url);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    //	curl_setopt($ch,CURLOPT_USERAGENT,getRandomUserAgent());
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$param);

    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }
  public function test(){


    $url = "https://myflexipay.com/ext/";

    $param = json_encode(
      array(
        "serviceCode"=> "mm",
        "provider"=>"mtn",
        "xmode"=> "debit",
        "txtDevice"=> "0243360295",
        "txtAmount"=>"0.1",
        "foreignID"=> "12345abcd",
        "userID"=> "1012",
        "userToken"=> "752ed3cfb813f69fc8d597aa4272d8e942adbffe",
        "callbackURL"=> "http://statusURL"
      ));
      return \Response::json($this->toFlexiipay($url,$param));

    }
  }
