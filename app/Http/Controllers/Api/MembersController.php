<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;
use App\ApiUsers;
use Mail;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Mail\RegistrationAcknowledged;
class MembersController extends Controller
{
  public function create($api_key, Request $request){
    try {
      $checkUser = ApiUsers::where('key', $api_key)->first();
      if ($checkUser) {
        $data = $request->all();
        $title = $data['title'];
        $first_name = $data['first_name'];
        $middle_name = $data['middle_name'];
        $last_name = $data['last_name'];
        $dob       = $data['date_of_birth'];
        $registration_number = $data['registration_number'];
        $sex = $data['sex'];
        $registration_date = $data['registration_date'];
        $maiden_name = $data['maiden_name'];
        $marital_status = $data['marital_status'];
        $nationality = $data['nationality'];
        $qualification_at_registration = $data['qualification_at_registration'];
        $training_institution = $data['training_institution'];
        $qualification_date = $data['qualification_date'];
        $email = $data['email'];
        $postal_address = $data['postal_address'];
        $residential_address = $data['residential_address'];
        $hometown = $data['hometown'];
        $phone = $data['phone'];
        $district = $data['district'];
        $region = $data['region'];

        $string = str_replace(' ', '', $registration_number);
        $constituent_id = preg_replace('/[^0-9]/', '', $string);
        $member = new Member();
        $member->First_Name = $first_name;
        $member->Last_Name = $last_name;
        $member->Middle_Name = $middle_name;
        $member->Birthdate = $dob;
        $member->Constituent_ID = preg_replace('/[^0-9]/', '', $string);
        $member->Gender = $sex;
        $member->Registration_Date = $registration_date;
        $member->Member_Name_Title = $title;
        $member->Maiden_Name = $maiden_name;
        $member->Marriage_Status = $marital_status;
        $member->Nationality = $nationality;
        $member->Qualification = $qualification_at_registration;
        $member->Pharmacy_School_or_University_Attended_with_Years = $training_institution;
        $member->Qualification_with_Years = $qualification_date;
        $member->status = 'Active';
        $member->Mobile = $phone;
        $member->Email_Address = $email;
        $member->Home_Postal_Code = $postal_address;
        $member->Home_Address_Line1 = $residential_address;
        $member->Home_City = $hometown;
        $member->region = $region;
        $member->Membership = 'NEWLY QUALIFIED DUES';
        $member->Member_Type_Code = 'NEW MEMBER';
        $member->district = $district;
        $member->save();

        $user = new User();
        $user->email = $email;
        $user->username = 'psgh'.$constituent_id;
        $user->password = Hash::make($constituent_id.'psgh');
        $user->name = $first_name.' '.$last_name;
        $user->member_id = $member->id;
        $user->save();

        Mail::to($email)->send(new RegistrationAcknowledged($constituent_id));

        return response()->json(
          [
            'success' => '0011' //successful insert
          ]
        );
      }else{
        return response()->json(
          [
            'error' => '004' // could not authenticate user
          ]
        );
      }
    } catch (\Exception $e) {
      return $e;
    }
  }
}
