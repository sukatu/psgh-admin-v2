<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Mail;
use \Illuminate\Foundation\Validation\ValidatesRequests;
use App\Mail\SendPasswordRessetLink;
class AuthController extends Controller
{

  public function confirm_email(Request $request){
    $user = User::where('email', $request->email)->first();
    try {
      if ($user == null) {
        return back()->with('error', 'The email you provided does not belong to any account. Try again');
      }else{
        $token = str_random(64);
        $mail_to_user =  Mail::to($request->email)->send(new SendPasswordRessetLink($user->name, $user->email, $token));
        PasswordReset::create(
          [
            'email' => $user->email,
            'token' => $token,
            'status' => '0',
          ]
        );
        Mail::to('sukaissa@gmail.com')->send(new SendPasswordRessetLink($user->name, $user->email, $token));
        return back()->with('success', 'Reset link has been sent to your mail, check and verify.');
      }
    } catch (\Exception $e) {
      return back()->with('error', 'Service Unavailable. Try again'. $e);
    }
  }

  public function lost_password(){
    return view('pages/admin/psgh-pages/page-lost-password');
  }

  public function verify_token($token){
    try {
      $app = PasswordReset::where('token', $token)->first();

      if ($app == null) {
        return redirect('confirm-email-view')->with('error', 'The token is invalid or has expired.');
      }else{
        $email = $app->email;
        PasswordReset::where('email', $email)->delete();
        return view('pages/admin/psgh-pages/reset-password', compact('email'));
      }
    } catch (\Exception $e) {
      return $e;
    }
  }

  public function reset_password(){
    return view('pages/admin/psgh-pages/reset-password');
  }

  public function update_password(Request $request){
    if ($request->password != $request->password_confirmation) {
      return back()->with('error', 'Password Mismatch');
    }else{
      try {
        $user = User::where('email', $request->email)
        ->update(
          [
            'password' => Hash::make($request['password'])
          ]
        );
        return redirect('confirm-reset');
      } catch (\Exception $e) {

      }

    }

  }

}
