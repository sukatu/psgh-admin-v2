<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use QrCode;
use App\Member;
use Illuminate\Support\Facades\Input;
use App\Event;
use App\EventAttendee;
use App\EventRegistrant;
use App\EventSession;
use App\SessionAttendee;
use Mail;
use App\Mail\RegistrationNitice;
use App\Jobs\SendAttendanceJob;
use App\Jobs\SendAllAttendanceJob;
use App\Jobs;
use App\Jobs\SessionAttendanceJob;
use App\Mail\SessionMail;
class EventController extends Controller
{
  public function badgeIndex(){
    $members = Member::get(['Member_Name_Title', 'First_Name', 'Last_Name', 'Middle_Name', 'Constituent_ID']);
    return view('pages/admin/psgh-pages/list',  compact('members'));
  }

  public function singleBadgeGenerator(Request $request){
    $member = Member::where('Constituent_ID', $request->input('constituent_id'))->first();
    return view('/pages/admin/psgh-pages/singleBadgeGenerator', compact('member'));
  }
  public function view_member_badge($id){
    $member = Member::where('Constituent_ID', $id)->first();
    return array('data'=>'success', 'response'=>$member);
  }

  public function report(){
    return view('pages/admin/psgh-pages/report');
  }

  public function badgeGenerator(){
    $members = Member::paginate(21);
    return view('/pages/admin/psgh-pages/badgeGenerator', compact('members'));
  }

  public function registrants(){
    $registrants = EventRegistrant::all();
    return view('pages/admin/psgh-pages/event-registrants', compact('registrants'));
  }

  public function attendees(){
    $attendees = EventAttendee::all();

    return view('pages/admin/psgh-pages/event-attendees', compact('attendees'));
  }

  public function register_attendees(){
    $members = Member::paginate(10);

    return view('pages/admin/psgh-pages/register-attendees',  compact('members'));
  }

  public function fetch_member_registration($id){
    $member = Member::where('Constituent_ID', $id)->first();
    return view('pages/admin/psgh-pages/register-member-event', compact('member'));
  }
  public function register_event_attendee(Request $request){
    try {
      $event =  Event::first();
      $register = new EventRegistrant();
      $register->event_id = $event->id;
      $register->member_id = $request->member_code;
      $register->save();
      return 'saved';

    } catch (\Exception $e) {
      require $e;
    }

  }
  public function filter_badge_reporting(Request $request){
    $members = Member::where('Member_Type_Code', $request->input('value'))->paginate(18)->appends(request()->query());;
    return view('pages/admin/psgh-pages/filter-badge-reporting', compact('members'));

  }

  public function qrCodeGenerator(){
    try {
      return QrCode::size(20)->generate('A basic example of QR code!');
    } catch (\Exception $e) {
      require $e;
    }
  }

  public function register_member(Request $request){
    try {
      $member = Member::where('Constituent_ID', $request->input('constituent_id'))->first();
      $select_member = EventRegistrant::where('member_id', $member->id)->first();
      if ($select_member) {
        $check_attendance = EventAttendee::where('member_id', $select_member->member_id)->first();
        if ($check_attendance) {
          return 'Badge already scanned!';
        }else{
          $register = new EventAttendee();
          $register->event_id = $select_member->event_id;
          $register->member_id = $select_member->member_id;
          $register->save();
          dispatch(new SendAttendanceJob($member->Constituent_ID));
          return '100';
        }
      }else{
        return 'The member has not registered for the current event';
      }

    } catch (\Exception $e) {
      return '100';
      // return 'Member Registration No. not registered in our database';
    }

  }

  public function sessions(){
    $sessions = EventSession::all();
    return view('pages/admin/psgh-pages/event-sessions', compact('sessions'));
  }

  public function show_session_form(){
    return view('pages/admin/psgh-pages/session-form');
  }

  public function validateSessionForm(Request $request){
    try {

      $event_session = new EventSession();
      $event_id_arr = $request->input('event_id');
      $session_name_arr = $request->input('session_name');
      $presenter_arr = $request->input('presenter');
      $start_date_arr = $request->input('start_date');
      $end_date_arr  = $request->input('end_date');
      $start_time_arr = $request->input('start_time');
      $end_time_arr = $request->input('end_time');
      $forms = count($request->input('form'));

      for ($i = 0; $i < $forms; $i++) {
        EventSession::insert(
          [
            [
              "name" => $session_name_arr[$i],
              "presenter" => $presenter_arr[$i],
              "start_date" => $start_date_arr[$i],
              "end_date" => $end_date_arr[$i],
              "start_time" => $start_time_arr[$i],
              "end_time" => $end_time_arr[$i],
              "event_id" => $event_id_arr[$i]
            ]
          ]
        );
      }
      return back()->with('success', 'Record saved');

    } catch (\Exception $e) {
      return back()->with('error', 'An error occured');
    }
  }
  public function session($id){
    $session = EventSession::where('id', $id)->first();
    return view('pages/admin/psgh-pages/session', compact('session'));
  }

  public function update_session(Request $request, $id){
    $data = Input::all();
    try {
      $session = EventSession::where('id', $id)
      ->update(
        [
          'name' => $data['session_name'],
          'presenter' => $data['presenter'],
          'start_date' => $data['start_date'],
          'start_time' => $data['start_time'],
          'end_time' => $data['end_time'],
          'end_date' => $data['end_date']
        ]
      );
      return back()->with('success', 'Record saved successfully');
    } catch (\Exception $e) {
      return back()->with('error', 'Server ERROR');
    }

  }

  public function delete_session($id){
    try {
      $session = EventSession::where('id', $id)->delete();
      return redirect('/sessions')->with('success', 'Record deleted');
    } catch (\Exception $e) {

    }
  }
  public function session_attendees($id){
    try {
      $session_attendees = SessionAttendee::where('session_id', $id)->get();
      $event_session = EventSession::where('id', $id)->first();
      return view('pages/admin/psgh-pages/session-attendees', compact('session_attendees', 'id', 'event_session'));
    } catch (\Exception $e) {

    }

  }

  public function register_session(Request $request, $id){
    try {
      $session = EventSession::where('id', $id)->first();
      $member = Member::where('Constituent_ID', $request->input('constituent_id'))->first();
      $mem_attended = SessionAttendee::where('member_id', $member->id)->where('session_id', $id)->first();
      if ($mem_attended == null) {
        if ($member == null) {
          return back()->with('error', 'Member does not exist in our database');
        }else{
          $event_registrant = EventRegistrant::where('member_id', $member->id)->first();
          if ($event_registrant == null) {
            return back()->with('error', 'Member has not registered for the Event');
          }else{
            $session_reg = new SessionAttendee();
            $session_reg->member_id = $member->id;
            $session_reg->event_id = $session->event_id;
            $session_reg->session_id = $id;
            $session_reg->save();

            dispatch(new SessionAttendanceJob($member->First_Name, $member->Last_Name, $session->name, $session->points, $member->Constituent_ID));

            return back()->with('success', 'Attendance Scanning Successful');
          }
        }
      }else{
        return back()->with('error', 'Member already scanned for this SESSION');
      }
    } catch (\Exception $e) {
      return back()->with('error', 'Service ERROR');
    }
  }

  public function sendAttendanceJob(){
    $registrants = EventRegistrant::all();
    foreach ($registrants as $registrant) {
      $member = Member::where('id', $registrant->member_id)->first();
      dispatch(new SendAllAttendanceJob($member->Constituent_ID));
    }
  }

  public function filter_ru($value){
    if ($value == '1') {
      $event_registrants = EventRegistrant::all();
      return view('pages/admin/psgh-pages/filter-event-registrants', compact('event_registrants', 'value'));
    }elseif ($value == '2') {
      $members = Member::all();
      $event_registrants = EventRegistrant::all();
      return view('pages/admin/psgh-pages/filter-event-registrants', compact('members', 'event_registrants', 'value'));
    }

  }

  public function updateSessionAttendance(){
    $session_attendees = SessionAttendee::all();
    foreach ($session_attendees as $session_attendee) {
      $member = Member::where('Constituent_ID', $session_attendee->constituent_id)->first();
        SessionAttendee::where('constituent_id', $member->Constituent_ID)->update(
        [
            'member_id' => $member->id
        ]
      );
    }
  }
}
