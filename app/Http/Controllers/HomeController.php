<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Mail;
use App\DuesType;
use App\MembershipTypeCodeDue;
use Illuminate\Support\Facades\Input;
use App\Mail\SendPasswordRessetLink;
use App\Membership;
use DB;
use App\Member;
class HomeController extends Controller
{
  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Contracts\Support\Renderable
  */
  public function index()
  {
    return view('pages/admin/psgh-pages/dashboard');
  }

  public function dashboard(){
    return view('pages/admin/psgh-pages/dashboard');
  }

  public function lost_password(){
    return view('pages/admin/psgh-pages/page-lost-password');
  }
  public function confirm_reset(){
    return view('pages/admin/psgh-pages/dashboard');
  }

  public function confirm_email(){
    return view('pages/admin/psgh-pages/page-lost-password');
  }

  public function fetch_dues_type($id){
    $dues_type = DuesType::where('id', $id)->first();
    return $dues_type;
  }

  public function update_dues_type(Request $request){
    $data = Input::all();
    try {
      if ($request->has('settings')) {
        if ($request->has('new')) {
          if($request->input('member_id') == 0){
            return back()->with('ERROR', 'Assign atleast one membership to dues type');
          }else{
            $forms = count($request->input('member_id'));
            $mem_id = $data['member_id'];
            $dues_type = new DuesType();

            $dues_type->display_name = $data['display_name'];
            $dues_type->description = $data['description'];
            $dues_type->amount = $data['amount'];
            $dues_type->expiry = $data['expiry'];
            $dues_type->status = $data['status'];
            $dues_type->save();
            for ($i = 0; $i < $forms; $i++) {

              $save_stype_code = new MembershipTypeCodeDue();
              $save_stype_code->dues_type_id = $dues_type->id;
              $save_stype_code->membership_id = $mem_id[$i];
              $save_stype_code->save();

            }
            return back()->with('success', 'New Dues Type created successfully');
          }
        }else{
          $exists = DuesType::where('id','!=', $data['id'])->first();
          if ($exists->display_name == $data['display_name']) {
            return back()->with('ERROR', 'Dues Type Name already assigned');
          }else{
            if($request->input('member_id') == 0){
              return back()->with('ERROR', 'Assign atleast one membership to dues type');
            }else{
              $forms = count($request->input('member_id'));
              $mem_id = $data['member_id'];


              $dues_type = DuesType::where('id', $data['id'])->update(
                [
                  'display_name' => $data['display_name'],
                  'description' => $data['description'],
                  'amount' => $data['amount'],
                  'expiry' => $data['expiry'],
                  'status' => $data['status']
                ]
              );

              for ($i = 0; $i < $forms; $i++) {
                $exists = MembershipTypeCodeDue::where('dues_type_id', $data['id'])->where('membership_id', $mem_id[$i])->first();
                if ($exists) {

                }else{

                  $save_stype_code = new MembershipTypeCodeDue();
                  $save_stype_code->dues_type_id = $data['id'];
                  $save_stype_code->membership_id = $mem_id[$i];
                  $save_stype_code->save();

                }
              }
              return back()->with('success', 'Record updated successfully!');
            }

          }
        }
      }else{
        $dues_type = DuesType::where('id', $data['id'])->
        update([
          'display_name' => $data['display_name'],
          'description' => $data['description'],
          'amount' => $data['amount'],
        ]);
        return back()->with('success', 'Record updated successfully!');
      }
    } catch (\Exception $e) {
      return back()->with('ERROR', 'Server Error: Contact the Administrator'.$e);
    }
  }

  public function setting_dues_view($id){
    if ($id == 'new') {
      $idd = 'new';
      $memberships = Membership::all();
      $dues_types = DuesType::all();
      return view('pages/admin/psgh-pages/settings-dues-type', compact('dues_types', 'memberships', 'idd'));
    }else{
      $idd = 'edit';
      $dues_type = DuesType::where('id', $id)->first();
      $memberships = Membership::all();
      $dues_code_types = MembershipTypeCodeDue::where('dues_type_id', $dues_type->id)->get();
      $dues_types = DuesType::all();
      return view('pages/admin/psgh-pages/settings-dues-type', compact('dues_types', 'dues_type', 'dues_code_types', 'memberships', 'idd'));
    }
  }

  public function unassign_membership($cid, $mid){
    MembershipTypeCodeDue::where('dues_type_id', $cid)->where('membership_id', $mid)->delete();
    return back()->with('unasigned-success', 'Membership unassigned successfully');
  }

  public function test(){
    $url = "https://api.flexibank.net/api/Notify";

     $param = json_encode(array(
      "to" => "233243360295",
      "from" => "PSGH",
      "message" => "Hi, Initial Test",
      "userID"=> "1012",
      "userToken"=> "752ed3cfb813f69fc8d597aa4272d8e942adbffe"
     ));

     return $this->toFlexiipay($url, $param);
  }

  function validateDate($date, $format = 'd-m-Y'){
    $d = \DateTime::createFromFormat($format, $date);
    return $d;
  }

  function toFlexiipay($url,$param)
  {
    $headers = array();
    $headers[] = 'Accept: application/json';
    $headers[] = 'Content-Type:  application/json; charset=utf-8';

    $ch = curl_init();
    // Disable SSL verification

    curl_setopt($ch, CURLOPT_URL,$url);

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
    //	curl_setopt($ch,CURLOPT_USERAGENT,getRandomUserAgent());
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$param);

    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }



}
