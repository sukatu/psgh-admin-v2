<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\InvoiceWithAttachment;
use Illuminate\Support\Facades\Mail;

class InvoiceAttachmentController extends Controller
{
    public function sendEmail()
    {
		$content = [
    		"CompanyName" => "Codingofcents",
    		"InvDate" => date("m/d/Y", strtotime(date("Y-m-d"))),
    		"InvNo" => "8910",
    		"CompanyAddress" => "Luwuk, Central Sulawesi, Indonesia",
    		"Details" => [
    			["Item" => "13 inch Macbook Pro", "Qty" =>  2, "Price" => 1299],
    			["Item" => "15 inch Macbook Pro", "Qty" =>  1, "Price" => 2399],
    		],
    		"Payment" => [
    			["Item" => "Total Tax", "Total" => 0],
    			["Item" => "Subtotal Price", "Total" => 4997],
    			["Item" => "Total Price", "Total" => 4997],
    		]
    	];

    	$subjects = "Invoice #8910";
    	$to = "sukaissa@gmail.com";
        // here we add attachment, attachment must be an array
        $attachment = [
          public_path("email/attachment/package.json"),
          public_path("email/attachment/package.json"),
         ];

    	Mail::to($to)->send(new InvoiceWithAttachment($subjects, $content, $attachment));
    	try {

    		return response()->json("Email Sent!");
    	} catch (\Exception $e) {
    		return response()->json($e->getMessage());
    	}
    }
}
