<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Member;
use App\Mail\SendProfileMail;
use App\EventRegistrant;
use App\Mail\SendEmailTest;
use App\Message;
use App\Jobs\SendEmailJob;
use App\Profiles;
use App\Mail\SocietyManager;
use App\MessageSent;
use DB;
use App\Http\Controllers\MembersController;
use App\Mail\InvoiceWithAttachment;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
  /**
  * Display a listing  of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
  }

  public function compose(){
    $members = Member::get(['Constituent_ID', 'Email_Address', 'First_Name', 'Last_Name', 'Middle_Name', 'Email_Address_Alternate']);
    $profiles = Profiles::all();
    return view('pages/admin/psgh-pages/mail-compose', compact('members', 'profiles'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  public function testMail(){
    Mail::to('sukaissa@gmail.com')->send(new SendProfileMail('$email'));
  }

  public function bulkMail(Request $request){
    if ($request->input('recipient') == '1') {
      $emails = Member::all();
      foreach ($emails as $email) {
        $data = Input::all();
        $body = $data['body'];
        $subject = $data['subject'];
        Mail::send('mail', compact("body"),

        function($message) use ($email, $subject) {

          $message

          ->from('admin@psgh', 'Administrator')

          ->subject($subject);

          $message->to($email->Email_Address);

        });
      }
      return 'message sent!';
    }else if ($request->input('recipient') == '2') {
      $members = Member::all();

      foreach ($members as $key => $member) {

        $check_if_registered = EventRegistrant::where('member_id', $member->id)->first();

        if ($check_if_registered) {
          echo $check_if_registered->id;
          echo "\n";
        }else{

          $not_registered_mem = Member::where('id', $member->id)->first()->Email_Address;
          $data = Input::all();
          $body = $data['body'];
          $subject = $data['subject'];
          Mail::send('mail', compact("body"),

          function($message) use ($not_registered_mem, $subject) {

            $message

            ->from('admin@psgh', 'Administrator')

            ->subject($subject);

            $message->to($not_registered_mem);

          });
        }
      }
    }
  }
  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }

  public function dispatcha(Request $request){
    $file = $request->file('file_url');
    $recipient = $request->input('recipient');
    $email = $request->input('mem_email');
    $subject = $request->input('subject');
    $body = $request->input('body');
    $profile = $request->input('profile');
    $profileCategory = $request->input('profile-categories');
    if ($file == null) {
      $attachment = [];
    }else{
      $file->move('email/attachment/', $file->getClientOriginalName());

      $attachment = [
        public_path('/email/attachment/'.$file->getClientOriginalName()),
      ];
    }

    if($recipient == 'All Members'){
      $members = DB::table('sned_mails')->where('status', 0)->limit(250)->get();
      foreach ($members as $member) {

        $data = $this->emailMacros($body, $member->email);
        $this->logMessage($data, 'All Members', $subject, 1);
        Mail::to($member->email)->queue(new SocietyManager($subject, $data, $attachment));
        DB::table('sned_mails')->where('email', $member->email)->update(['status' => 1]);

      }

    }elseif ($request->input('recipient') == 'Profile') {

      $memberController = new MembersController();
      $members = Member::where($memberController->getProfile($profile), $profileCategory)->where('send_mail', 1)->whereNotNull('Email_Address')->orderBy('id', 'desc')->get();

      if ($members->count() < 1) {
        return back()->with('error', 'Message cannot be sent to 0 contacts.');
      }else {
        foreach ($members as $member) {
          $data = $this->emailMacros($body, $member->Email_Address);
          Mail::to($member->Email_Address)->queue(new SocietyManager($subject, $data, $attachment));
          MessageSent::insert(
            [
              [
                'subject' => $subject,
                'type' => 'Profile',
                'sendAt' => date('Y-m-d h:m:s'),
                'recipients' => $member->Email_Address,
                'created_at' => date('Y-m-d'),
              ]
            ]
          );
        }
      }

    }elseif ($recipient == 'Individual') {

      $data = $this->emailMacros($body, $email);
      $this->logMessage($data, 'Individual', $subject, 1);
      Mail::to($email)->queue(new SocietyManager($subject, $data, $attachment));

    }
    return back()->with('success', 'Message Sent!');
  }

  public function logMessage($data, $type, $subject, $totalRecipient){
    MessageSent::insert(
      [
        [
          'payload' => $data,
          'subject' => $subject,
          'type' => $type,
          'sendAt' => date('Y-m-d h:m:s'),
          'recipients' => $totalRecipient,
          'created_at' => date('Y-m-d'),
        ]
      ]
    );
  }

  public function emailMacros($body, $member_email){
    $member = Member::where('Email_Address', $member_email)->first();

    $array = array(
      '{{name}}' => $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name,
      '{{reg_no}}' => $member->Constituent_ID,
      '{{email}}' => $member->Email_Address,
      '{{age}}' => $member->age,
      '{{status}}' => $member->status,
      '{{dues_type}}' => $member->Membership,
      '{{membership}}' => $member->Member_Type_Code,
      '{{first_name}}' => $member->First_Name,
      '{{middle_name}}' => $member->Middle_Name,
      '{{last_name}}' => $member->Last_Name,
      '{{qualification}}' => $member->Qualification,
      '{{gender}}' => $member->Gender,
      '{{title}}' => $member->Member_Name_Title,
      '{{dob}}' => $member->Birthdate,
      '{{marital_status}}' => $member->Marriage_Status,
      '{{address}}' => $member->Home_Address_Line1,
      '{{city}}' => $member->Home_City,
      '{{gh_post_code}}' => $member->Home_Postal_Code,
      '{{country}}' => $member->Home_Country,
      '{{phone}}' => $member->Home_Phone,
      '{{mobile}}' => $member->Mobile,
      '{{employer_name}}' => $member->Employer_Name,
    );
    return strtr($body, $array);;
  }

  public function draft(){
    return view('pages/admin/psgh-pages/mail-draft');
  }
}
