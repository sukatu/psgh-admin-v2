<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\Jobs\Sukaa;
use App\Mail\SendEmailTest;
use App\Jobs\SendEmailJob;
use App\Jobs\SendMembershipPromotionJob;
use Mail;
use App\Mail\MembershipPromotion;
class MembersController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $members = App\Member::paginate(20);
    $members_title = App\Member::get(['First_Name', 'Middle_Name', 'Last_Name', 'Constituent_ID', 'Member_Name_Title']);
    return view('pages/admin/psgh-pages/member-list', compact('members', 'members_title'));
  }

  public function validateRegistrationForm(Request $request)
  {
    if ($request->input('submission_type') == 'update') {
      $this->validate($request,[
        'title' => 'required',
        'dob' => 'required|date',
        'gender' => 'required',
        'email_address' => 'required|email',
        'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:9',

      ]);
      return $this->update($request);
    }else{
      $this->validate($request,[
        'title' => 'required',
        'first_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255|unique:users,name,',
        'last_name' => 'required|regex:/^[a-zA-Z]+$/u|max:255|unique:users,name,',
        'dob' => 'required|date',
        'gender' => 'required',
        'marital_status' => 'required',
        'email_address' => 'required|email',
        'mobile' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        'home_address' => 'required',
        'suburb' => 'required',
        'nationality' => 'required',
        'gh_post_code' => 'required',
        'professional_title' => 'required',
        'profession' => 'required',
        'educational_level' => 'required',
        'qualification' => 'required',

      ]);
      return $this->store($request);
    }
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  public function fetch_ajax(){
    $json = App\Member::get(['Constituent_ID', 'First_Name', 'Middle_Name', 'Last_Name', 'Registration_Date', 'Employer_Name', 'status', 'Mobile', 'Email_Address']);
    return $json;
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store($data)
  {
    try {
      $newmember = App\Member::insert(
        [
          [
            'Member_Name_Title' => $data['title'],
            'First_Name' => $data['first_name'],
            'Middle_Name' => $data['middle_name'],
            'Last_Name' => $data['last_name'],
            'Gender' => $data['gender'],
            'Birthdate' => $data['dob'],
            'Mobile' => $data['mobile'],
            'Home_Phone' => $data['alt_mobile'],
            'Email_Address' => $data['email'],
            'Email_Address_Alternate' => $data['alt_email'],
            'Home_Address_Line1' => $data['home_address_line1'],
            'Constituent_ID' => $data['constituent_id'],
            'Home_Country'  => $data['country'],
            'Home_City'   => $data['city'],
            'Home_Postal_Code' => $data['gh_post_code'],
            'Nationality' => $data['nationality'],
            'Place_of_Birth' => $data['place_of_birth'],
            'Marriage_Status' => $data['marital_status'],
            'Maiden_Name' => $data['maiden_name'],
            'Member_Type_Code' => $data['member_type_code'],
            'Membership' => $data['membership'],
            'Spouse_Name' => $data['spouse_name'],
            'Employer_Name' => $data['employer_name'],
            'Professional_Title' => $data['professional_title'],
            'Profession' => $data['profession'],
            'Employer_Address_Line1' => $data['employer_contact'],
            'Employer_City' => $data['employer_city'],
            'Employer_Location' => $data['employer_location'],
            'Employer_Postal_Code' => $data['employer_gh_post_code'],
            'Employer_Country' => $data['employer_country'],
            'Pharmacy_School_or_University_Attended_with_Years' => $data['university_attended'],
            'Qualification' => $data['qualification'],
            'created_by' => \Auth::user()->id,
            'status' => $data['status']

          ]
        ]
      );
      return back()->with('success', 'Record saved successfully');
    } catch (\Exception $e) {
      return back()->with('error', 'Service down, try again'.$e);
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $member = App\Member::where('Constituent_ID', $id)->first();
    return view('pages/admin/psgh-pages/member-details', compact('member'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $member = App\Member::where('Constituent_ID', $id)->first();
    return view('pages/admin/psgh-pages/member', compact('member'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $data)
  {
    try {
      $newmember = App\Member::where('Constituent_ID', $data['Constituent_ID'])
      ->update(
        [
          'Member_Name_Title' => $data['title'],
          'Mobile_Area_Code' => $data['mobile_area_code'],
          'First_Name' => $data['first_name'],
          'Middle_Name' => $data['middle_name'],
          'Last_Name' => $data['last_name'],
          'Gender' => $data['gender'],
          'Birthdate' => $data['dob'],
          'Mobile' => $data['mobile'],
          'Home_Phone' => $data['alt_mobile'],
          'Email_Address' => $data['email_address'],
          'Email_Address_Alternate' => $data['alt_email'],
          'Home_Address_Line1' => $data['home_address_line1'],
          'Home_Country'  => $data['country'],
          'Home_City'   => $data['city'],
          'Home_Postal_Code' => $data['gh_post_code'],
          'Nationality' => $data['nationality'],
          'Place_of_Birth' => $data['place_of_birth'],
          'Marriage_Status' => $data['marital_status'],
          'Member_Type_Code' => $data['member_type_code'],
          'Membership' => $data['membership'],
          'Maiden_Name' => $data['maiden_name'],
          'Spouse_Name' => $data['spouse_name'],
          'Employer_Name' => $data['employer_name'],
          'Professional_Title' => $data['professional_title'],
          'Profession' => $data['profession'],
          'Employer_Address_Line1' => $data['employer_contact'],
          'Employer_City' => $data['employer_city'],
          'Employer_Location' => $data['employer_location'],
          'Employer_Postal_Code' => $data['employer_gh_post_code'],
          'Employer_Country' => $data['employer_country'],
          'Qualification' => $data['qualification'],
          'created_by' => \Auth::user()->id,
          'status' => $data['status']
        ]
      );
      return back()->with('success', 'Record updated successfully');
    } catch (\Exception $e) {
      return back()->with('error', 'Service down, try again'.$e);
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }

  public function register(){
    return view('pages/admin/psgh-pages/membership-registration');
  }

  public function dispatcha(Request $request){


    dispatch(new App\Jobs\SendEmailJob($details, $subject));
    return 'sent';
  }

  public function update_mail(){
    try {

      foreach ($users = App\User::all() as $user) {
        $member_email = App\Member::where('id', $user->member_id)->first();
        if ($member_email) {
          App\Member::where('id', $user->member_id)
          ->update(
            [
              'Email_Address' => $user->email
            ]
          );
        }
        echo "saved";
      }

    } catch (\Exception $e) {
      return $e;
    }
  }
  public function search_member($Constituent_ID){
    $member = App\Member::where('Constituent_ID', $Constituent_ID)->first();
    return view('pages/admin/psgh-pages/fetch-member', compact('member'));
  }

  public function updateMember(){
    foreach (App\Member::all() as $value) {
      App\EventRegistrant::where('member_id', $value->id)
      ->update(
        [
          'constituent_id' => $value->Constituent_ID,
        ]
      );
    }
  }

  public function promotions(){
    $members = App\Member::paginate(15);
    $members_title = App\Member::get(['First_Name', 'Middle_Name', 'Last_Name', 'Constituent_ID', 'Member_Name_Title']);
    $memberships = App\Membership::all();
    return view('pages/admin/psgh-pages/promotions', compact('members', 'members_title', 'memberships'));
  }

  public function bulk_promotion(Request $request){
    try {
      $memberships = App\Member::where('Member_Type_Code', $request->input('from_membership'))->get();
      if ($memberships->count() == 0) {
        return back()->with('info', 'No member has been promoted');
      }else{
      foreach ($memberships as $membership) {
        App\Member::where('Constituent_ID', $membership->Constituent_ID)->
        update(
          [
              'Member_Type_Code' => $request->input('to_membership'),
          ]
        );
      }
      if ($request->input('noty') == 1) {
        dispatch(new SendMembershipPromotionJob());
      }
      $total = $memberships->count();
      return back()->with(['success'=> 'Promotion successfully', 'total'=>$total]);
    }
    } catch (\Exception $e) {
      return back()->with('error', 'Server ERROR'.$e);
    }

  }
  public function getProfileCategory($id){
    $profile_details = App\ProfileDetails::where('profile_id', $id)->get();
    return view('pages/admin/psgh-pages/profile-details', compact('profile_details'));
  }
  public function getProfileCategoryOption($id){
    $profile_details = App\ProfileDetails::where('profile_id', $id)->get();
    return $profile_details;
  }
  public function getProfile($id){
    $profile = App\Profiles::where('id', $id)->first()->name;
    return $profile;
  }
  public function getMember($constituent_id){
    $member = App\Member::where('Constituent_ID', $constituent_id)->first();
    $dues_types = App\DuesType::where('display_name', $member->Membership)->first();
      $response = response()->json(
        [
          'member' => $member,
          'dues_type' => $dues_types
        ]
      );
    return $response;
  }
}
