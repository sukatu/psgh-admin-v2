<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\Http\Requests\UpdatePermissionsRequest;
use App\Role;
class PermissionsController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {

    return view('pages/admin/psgh-pages/role-permissions');
  }

  public function fetchPermissions(Request $request, $role_name){
    $groupPermissions = Permission::all()->groupBy('group_name');
    $role = Role::where('name', $role_name)->first();
    $selectedPermissions = $role->permissions->pluck('id');
    return view('pages/admin/psgh-pages/fetch-permissions', compact('groupPermissions', 'selectedPermissions'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(UpdatePermissionsRequest $request)
  {
    $role = Role::where('name', $request->input('user_type'))->firstOrFail();
    $role->permissions()->sync($request->input('permissions'));
    \Artisan::call('cache:clear');
    \Artisan::call('view:clear');
    \Artisan::call('route:clear');
    return back()->with('success', 'Record updated');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
