<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\SmsTable;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\InforbipController;
use App\SmsSubscriptions;
use App\Profiles;
use App\PrifleDetails;
use App\MessageSent;
use App\Helper\PSGHHelper;
class SmsController extends Controller
{
  public function index(){
    $members = Member::get(['Constituent_ID', 'Email_Address', 'First_Name', 'Last_Name', 'Middle_Name', 'Email_Address_Alternate']);
    $profiles = Profiles::all();
    return view('pages/admin/psgh-pages/sms', compact('members', 'profiles'));
  }

  public function sendSms(Request $request){
    $validatedData = $request->validate([
      'recipient' => 'required',
      'message' => 'required'
    ]);
  }

  public function getSummary(Request $request){
    $data = Input::all();

    if (self::infobipBalance() < 10) {
      return response()->json(
        [
          'success' => 'server-error',
          'data' => '505'

        ]
      );
    }elseif (self::accountBalance() == null) {
      return response()->json(
        [
          'success' => null,
          'data' => null
        ]
      );
    }elseif (self::accountBalance() < 2) {

      return response()->json(
        [
          'success' => 'low-balance',
          'data' => '501'
        ]
      );
    }else{

      if ($data['group-type'] == 'sendToAllMemmbers') {

        return $this->calRate('sendToAllMemmbers', $this->countMessage($data['message']), null, null);

      }else if($request->has('profile-categories')){
        return $this->calRate('profile-categories', $this->countMessage($data['message']), $data['profile'], $data['profile-categories']);
      }
    }
  }

  public function countMessage($message){
        $length = strlen($message);
        $total = 0;
    switch ($length) {
      case  $length < 60:
            $total = 1;
        break;
      case $length >= 60 && $length <= 120;
            $total = 2;
        break;
      default:
            $total = 3;
        break;
    }
      return $total;

  }


  public function sendConfirmSms(Request $request){
    $input = $request->all();

    if ($input['type'] == 'sendToAllMemmbers') {
      if ($input['schedule'] == 'true'){
        $this::sendToAllMemmbers($input['message'], 'true', $input['scheduleDateData'], $input['scheduleTimeData']);
        return response()->json(
          [
            'success' => '001'
          ]
        );
      }else{

        $this::sendToAllMemmbers($input['message'], 'false', '', '');
        return response()->json(
          [
            'success' => '001'
          ]
        );

      }
    }else if($input['type'] == 'profile-categories'){

      $this::sendToProfile($input['message'], 'false', '', '');
      return response()->json(
        [
          'success' => '001'
        ]
      );
    }
  }

  public static function sendToProfile($message, $schedule, $scheduleDateData, $scheduleTimeData){
    $sessionData = PSGHHelper::getSession();
    $members = SmsTable::where($sessionData['value'], $sessionData['data'])->get();
    foreach ($members as $member) {
      InforbipController::sendSms($member->phone, $message);
      self::logSms($member->id, $message, $schedule, 'profile-categories', $scheduleDateData.' '.$scheduleTimeData);
    }
  }

  public static function logSms($recipient, $message, $schedule, $type, $sendAt){

    MessageSent::insert(
      [
        [
          'recipient_id' => $recipient,
          'payload' => $message,
          'scheduled' => $schedule,
          'type' => $type,
          'sendAt' => $sendAt,
          'created_at' => now(),
        ]
      ]
    );
  }

  public static function sendToAllMemmbers($message, $schedule, $scheduleDateData, $scheduleTimeData){
    $users = SmsTable::all();
    if ($schedule == 'false') {
      foreach ($users as $user) {
        InforbipController::sendSms($user->phone, $message);
        self::logSms($user->id, $message, $schedule, 'sendToAllMemmbers', $scheduleDateData.' '.$scheduleTimeData);
      }
    }else{
      foreach ($users as $user) {
        InforbipController::sendScheduledSms($user->phone, $message, $scheduleDateData, $scheduleTimeData);
        self::logSms($user->id, $message, $schedule, 'sendToAllMemmbers', $scheduleDateData.' '.$scheduleTimeData);
      }
    }
  }


  public static function infobipBalance(){
    // $data = json_decode(InforbipController::checkBalance());
    return 300;
  }

  public static function accountBalance(){
    $accountBalance = SmsSubscriptions::where('master_id', \Auth::user()->id)->first();
    if ($accountBalance == null) {
      return null;
    }else {
      return $accountBalance->total_remaining;
    }
  }

  public static function checkBalance(){
    if (self::infobipBalance() < 10) {
      return response()->json(
        [
          'success' => 'server-error',
          'data' => '505'
        ]
      );
    }elseif (self::accountBalance() == null) {
      return response()->json(
        [
          'success' => null,
          'data' => null
        ]
      );
    }elseif (self::accountBalance() < 2) {
      return response()->json(
        [
          'success' => 'low-balance',
          'data' => '501'
        ]
      );
    }else{
      return response()->json(
        [
          'success' => 'good-to-go',
          'data' => '001'
        ]
      );
    }
  }

  public static function calRate($type, $noSms, $value, $data){

    $subscription_data = SmsSubscriptions::where('master_id', \Auth::user()->id)->first();
    if ($subscription_data == null) {
      return response()->json(
        [
          'data' => 'null'
        ]
      );
    }else{
      $count_members = self::countSmsMembers($type, $value, $data);
      $total_remaining = $subscription_data->total_remaining;
      $available_amount   = $subscription_data->amount;

      $rate = round($subscription_data->amount / $subscription_data->quantity, 3);
      $cost = $rate * $noSms;
      $total_sms_used = $noSms * $count_members;
      $total_amount_used = $cost  * $count_members;
      $balance_sms = $total_remaining - $total_sms_used;
      $balance_amount = $available_amount - $total_amount_used;
      return response()->json(
        [
          'type' => $type,
          'count_members' => $count_members,
          'rate' => $rate,
          'no_of_sms' => $noSms,
          'cost' => $cost,
          'total_sms_used' => $total_sms_used,
          'total_amount_used' => $total_amount_used,
          'total_remaining' => $total_remaining,
          'available_amount' => $available_amount,
          'balance_sms' => $balance_sms,
          'balance_amount' => $balance_amount
        ]
      );
    }
  }

  public static function countSmsMembers($type, $value, $data){
    switch ($type) {
      case 'sendToAllMemmbers':
      return SmsTable::count();
      break;
      case 'profile-categories':
      PSGHHelper::setSession($value, $data);
      return Member::where(PSGHHelper::memberColumn($value), $data)->count();
      default:
      // code...
      break;
    }
  }


  public function testSms(){
    return InforbipController::sendScheduledSms('233243360295', 'hmm');
  }


}
