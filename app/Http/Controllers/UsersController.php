<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\RoleUser;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\AddUserRequest;
use Illuminate\Support\Facades\Validator;
class UsersController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {

    $roles = Role::all();
    return view('pages/admin/psgh-pages/add-users', compact('roles'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    return view('add-user');
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(AddUserRequest $request)
  {
  $validatedData = $request->validate(
    ['email' => 'unique:admin_users']
  );
    $user = new User();
    $user->fill($request->validated());
    $user->password = Hash::make($request->input('password'));
    $user->type = 'boss';
    $user->save();

    $user->attachRole($request->input('user_type'));

    return back()->with('success', 'User added successfully!');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    $roles =  Role::all();
    $user = User::find($id);
    $user_role = RoleUser::where('user_id', $id)->first();
    $role = Role::find($user_role->role_id);
    return view('pages/admin/psgh-pages/user-detail', compact('user', 'roles', 'role'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try {
      $user = User::where('id', $id)->
      update(
        [
          'name' => $request->name,
          'email' => $request->email,
          'password' => Hash::make($request->input('password')),
          'type' => 'boss'
        ]
      );
      $role = RoleUser::where('user_id', $id)->
      update(
        [
          'role_id' => $request->input('user_type')
        ]
      );
      return back()->with('success', 'Record updated successfully');
    } catch (\Exception $e) {
      return $e;
    }

  }

  public function list(){
    $users = User::all();
    return view('pages/admin/psgh-pages/user-lists', compact('users'));
  }
  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
