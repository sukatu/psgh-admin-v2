<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Auth;
use Redirect;
class UserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Auth::user()->type != 'boss'){
        Auth::logout();
        return Redirect::to('login');
      }
      return $next($request);
    }
}
