<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Mail\AGM2019;
use App\Member;
class SendAttendanceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

     protected $constituent_id;
    public function __construct($constituent_id)
    {
      $this->constituent_id = $constituent_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $constituent_id = $this->constituent_id;
      $member = Member::where('Constituent_ID', $this->constituent_id)->first();
      if ($member->Email_Address == null) {
        $email = $member->Email_Address_Alternate;
      }else{
        $email = $member->Email_Address;
      }
      $first_name = $member->First_Name;
      $middle_name = $member->Middle_Name;
      $last_name = $member->Last_Name;

      Mail::to($email)->send(new AGM2019($constituent_id, $first_name, $middle_name, $last_name));
    }
}
