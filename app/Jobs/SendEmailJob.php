<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\SendEmailTest;
use App\Mail\SocietyManager;
use Mail;
use App\RegionalGroupMember;
use App\RegionalGroupRule;
use App\Member;
use App\PracticeGroupRule;
use App\PracticeGroupMember;
use App\EventRegistrant;
use App\InterestGroupRule;
use DB;
use App\InterestGroupMember;
use App\Message;
use App\MessageSent;
use App\User;
use App\MemberUser;
use App\Http\Controllers\MembersController;


class SendEmailJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  protected $data;
  /**
  * Create a new job instance.
  *
  * @return void
  */
  protected $recipient, $email, $mailSubject, $body, $profile, $profileCategory;
  public function __construct($recipient, $email, $mailSubject, $body, $profile, $profileCategory)
  {
    $this->recipient = $recipient;
    $this->email = $email;
    $this->mailSubject =  $mailSubject;
    $this->body = $body;
    $this->profile = $profile;
    $this->profileCategory = $profileCategory;
  }

  /**
  * Execute the job.
  *
  * @return void
  */
  public function handle()
  {
    $recipient = $this->recipient;
    $email =  $this->email;
    $subject = $this->mailSubject;
    $body = $this->body;
    $profile = $this->profile;
    $profileCategory = $this->profileCategory;

    if ($recipient == 'All Members') {
      $members = Member::where('send_mail', 1)->get();
      foreach ($members as $member) {
        $data = $this->emailMacros($body, $member->Email_Address);
        Mail::to($member->Email_Address)->send(new SocietyManager($subject, $data));
      }
      $this->logMessage($data, 'All Members', $subject, $members->count());
    }elseif ($recipient == 'Profile') {
      $memberController = new MembersController();
      $members = Member::where($memberController->getProfile($profile), $profileCategory)->where('send_mail', 1)->get();
      foreach ($members as $member) {
        $data = $this->emailMacros($body, $member->Email_Address);
        Mail::to($member->Email_Address)->send(new SocietyManager($subject, $data));
      }
      $this->logMessage($body, 'Profile'.'['.$memberController->getProfile($profile).']['.$profileCategory.']', $subject, $members->count());
    }elseif ($recipient == 'Individual') {
      $user = MemberUser::find(4509);
      $request = 'suka';
      Mail::send('emails.welcome', ['user' => $user], function ($m) use ($user, $request) {

        $m->to('sukaissa@gmail.com');
        $m->attach('/email/attachment/attachment');
      });
    }
  }

  public function logMessage($data, $type, $subject, $totalRecipient){
    MessageSent::insert(
      [
        [
          'payload' => $data,
          'subject' => $subject,
          'type' => $type,
          'sendAt' => date('Y-m-d h:m:s'),
          'recipients' => $totalRecipient,
          'created_at' => date('Y-m-d'),
        ]
      ]
    );
  }
  public function emailMacros($body, $member_email){
    $member = Member::where('Email_Address', $member_email)->first();

    $array = array(
      '{{name}}' => $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name,
      '{{reg_no}}' => $member->Constituent_ID,
      '{{email}}' => $member->Email_Address,
      '{{age}}' => $member->age,
      '{{status}}' => $member->status,
      '{{dues_type}}' => $member->Membership,
      '{{membership}}' => $member->Member_Type_Code,
      '{{first_name}}' => $member->First_Name,
      '{{middle_name}}' => $member->Middle_Name,
      '{{last_name}}' => $member->Last_Name,
      '{{qualification}}' => $member->Qualification,
      '{{gender}}' => $member->Gender,
      '{{title}}' => $member->Member_Name_Title,
      '{{dob}}' => $member->Birthdate,
      '{{marital_status}}' => $member->Marriage_Status,
      '{{address}}' => $member->Home_Address_Line1,
      '{{city}}' => $member->Home_City,
      '{{gh_post_code}}' => $member->Home_Postal_Code,
      '{{country}}' => $member->Home_Country,
      '{{phone}}' => $member->Home_Phone,
      '{{mobile}}' => $member->Mobile,
      '{{employer_name}}' => $member->Employer_Name,
    );
    return strtr($body, $array);;
  }

}
