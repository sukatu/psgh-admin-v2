<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Member;
use App\Mail\SessionMail;
class SessionAttendanceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
     protected $first_name, $last_name, $session_name, $session_point, $constituent_id;
    public function __construct($first_name, $last_name, $session_name, $session_point, $constituent_id)
    {
      $this->first_name = $first_name;
      $this->last_name = $last_name;
      $this->session_name = $session_name;
      $this->session_point = $session_point;
      $this->constituent_id = $constituent_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      $first_name = $this->first_name;
      $last_name = $this->last_name;
      $session_name = $this->session_name;
      $session_point = $this->session_point;
      $constituent_id = $this->constituent_id;
      $member = Member::where('Constituent_ID', $constituent_id)->first();
      if ($member->Email_Address == null) {
        $email = $member->Email_Address_Alternate;
      }else {
        $email = $member->Email_Address;
      }
      Mail::to($email)->send(new SessionMail($first_name, $last_name, $session_name, $session_point, $constituent_id));
    }
}
