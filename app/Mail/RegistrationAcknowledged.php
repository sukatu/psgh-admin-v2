<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Member;
class RegistrationAcknowledged extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     protected $registration_number;
    public function __construct($registration_number)
    {
        $this->registration_number = $registration_number;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $registration_number = $this->registration_number;
      $member = Member::where('Constituent_ID', $registration_number)->first();
      $first_name = $member->First_Name;
      $last_name  = $member->Last_Name;
        return $this->view('emails.api-member-registration', compact('registration_number', 'first_name', 'last_name'));
    }
}
