<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationNotice extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

protected $constituent_id;
protected $first_name;
protected $middle_name;
protected $last_name;
    public function __construct($constituent_id, $first_name, $middle_name, $last_name)
    {
        $this->constituent_id = $constituent_id;
        $this->first_name = $first_name;
        $this->middle_name = $middle_name;
        $this->last_name = $last_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $constituent_id = $this->constituent_id;
      $first_name = $this->first_name;
      $middle_name = $this->middle_name;
      $last_name = $this->last_name;
        return $this->view('attendance', compact('first_name', 'middle_name', 'last_name', 'constituent_id'));
    }
}
