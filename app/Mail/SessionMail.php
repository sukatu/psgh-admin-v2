<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SessionMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
     protected $first_name, $last_name, $session_name, $session_point, $constituent_id;
    public function __construct($first_name, $last_name, $session_name, $session_point, $constituent_id)
    {
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->session_name = $session_name;
        $this->session_point = $session_point;
        $this->constituent_id = $constituent_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $first_name = $this->first_name;
      $last_name = $this->last_name;
      $session_name = $this->session_name;
      $session_point = $this->session_point;
      $constituent_id = $this->constituent_id;
      return $this->view('session_attendance_mail', compact('first_name', 'last_name', 'session_name', 'session_point', 'constituent_id'));
    }
}
