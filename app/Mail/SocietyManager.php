<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SocietyManager extends Mailable
{
  use Queueable, SerializesModels;

  /**
  * Create a new message instance.
  *
  * @return void
  */
  public $subject;
  public $content;
  public $attachment;

  public function __construct($subject, $content, $attachment)
  {
    $this->subject = $subject;
    $this->content = $content;
    $this->attachment = $attachment;
  }

  /**
  * Build the message.
  *
  * @return $this
  */
  public function build()
  {
    $email = $this->markdown('emails.society-manager')
    ->subject($this->subject)
    ->with('content', $this->content);
    foreach ($this->attachment as $item) {
      $email->attach($item);
    }
    return $email;
  }
}
