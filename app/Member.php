<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'members';

    public function goodStandingMember(){
       return $this->hasOne('App\GoodStandingMember', 'Constituent_ID', 'registration_no');
    }

}
