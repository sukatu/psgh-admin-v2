<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberId extends Model
{
    //

    public static function new_id(){
      return MemberId::where('status', '0')->first()->id;
    }
}
