<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberUser extends Model
{
      protected $table = 'users';

      protected $fillable = ['name', 'email', 'password', 'username', 'member_id'];
}
