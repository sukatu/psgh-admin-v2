<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  protected $table = 'messagess';
    protected $fillable = ['body', 'type', 'created_at', 'updated_at'];
}
