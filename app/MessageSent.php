<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageSent extends Model
{
    protected $fillable = ['payload', 'type', 'scheduled', 'sendAt', 'recipients'];
}
