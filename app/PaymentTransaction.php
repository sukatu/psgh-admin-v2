<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    protected $fillable = ['member_id', 'description', 'trans_type', 'bank', 'reference', 'amount', 'payment_date'];
}
