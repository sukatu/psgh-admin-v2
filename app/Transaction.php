<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['trans_id', 'user_id', 'amount', 'payment_date', 'status'];
}
