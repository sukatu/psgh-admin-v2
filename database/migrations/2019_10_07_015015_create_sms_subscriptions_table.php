<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('sms_subscriptions', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedBigInteger('master_id');
        //     $table->unsignedBigInteger('created_by');
        //     $table->unsignedBigInteger('updated_by');
        //     $table->string('balance');
        //     $table->string('expiry');
        //     $table->string('amount');
        //     $table->string('total_sms');
        //     $table->timestamps();
        //
        //     $table->foreign('created_by')->references('id')->on('users');
        //     $table->foreign('updated_by')->references('id')->on('users');
        //     $table->foreign('master_id')->references('id')->on('users');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_subscriptions');
    }
}
