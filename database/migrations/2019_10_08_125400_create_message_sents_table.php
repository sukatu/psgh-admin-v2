<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageSentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('message_sents', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedInteger('recipient_id');
        //     $table->string('payload');
        //     $table->string('scheduled');
        //     $table->string('type');
        //     $table->timestamps();
        //
        //     $table->foreign('recipient_id')->references('id')->on('members');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_sents');
    }
}
