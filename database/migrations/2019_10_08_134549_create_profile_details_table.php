<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileDetailsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('profile_details', function (Blueprint $table) {
      $table->bigIncrements('id');
      $table->unsignedBigInteger('profile_id');
      $table->string('name');
      $table->string('display_name');
      $table->timestamps();

      $table->foreign('profile_id')->references('id')->on('profiles');
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('profile_details');
  }
}
