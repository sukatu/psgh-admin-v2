<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\ApiUsers;
class ApiUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data[] = self::getApiUsers();
        foreach ($data as $apiuser) {
          ApiUsers::insert(
            [
              'name' => $apiuser['name'],
              'key' => $apiuser['key']
            ]
          );
        }
    }

    public function getApiUsers(){
      return [
        'name' => 'PSGH',
        'key' => Hash::make('iamthatiam')
      ];
    }
}
