<?php

use Illuminate\Database\Seeder;
use App\Permission;
class PermissionSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $data[] = self::getProfilePermissions();
    $data[] = self::getEventPermissions();
    $data[] = self::getCommunicationPermissions();
    $data[] = self::getSessionPermissions();
    $data[] = self::getAccountingPermissions();
    $data[] = self::getUserPermissions();
    $data[] = self::getSettingsPermissions();

    foreach ($data as $permissionGroup) {
      foreach ($permissionGroup['permissions'] as $permissionArray) {
        $permission = Permission::where('name', $permissionArray['name']) ->first();
        if (!$permission) {
          $permission = new Permission;
        }
        $permission->fill($permissionArray);
        $permission->group_name = $permissionGroup['group_name'];
        $permission->save();

      }
    }
  }


public static function getUserPermissions(){
  return [
    'group_name' => 'Users',
    'permissions' => [
      [
        'name' => 'add-users',
        'display_name' => 'Add Users',
        'description' => 'User add or create users',
      ],
      [
        'name' => 'view-users',
        'display_name' => 'View Users',
        'description' => 'User can view users'
      ],
      [
        'name' => 'edit-users',
        'display_name' => 'Edit/Update Users',
        'description' => 'User edit or update users'
      ],
      [
        'name' => 'delete-users',
        'display_name' => 'Delete Users',
        'description' => 'User can delete users',
      ],
    ],
  ];
}
  public static function getProfilePermissions(){
    return [
      'group_name' => 'Profile',
      'permissions' => [
        [
          'name' => 'add-members',
          'display_name' => 'Add Members',
          'description' => 'User can add members'
        ],
        [
          'name' => 'view-members',
          'display_name' => 'View Members',
          'description' => 'User can view members'
        ],
        [
          'name' => 'edit-members',
          'display_name' => 'Edit/Update Members',
          'description' => 'User can edit and update members'
        ],
        [
          'name' => 'delete-members',
          'display_name' => 'Delete Members',
          'description' => 'User can delete members'
        ],
        [
          'name' => 'perform-promotions',
          'display_name' => 'Perform Promotions',
          'description' => 'User can promote members'
        ]
      ],
    ];
  }

  public static function getEventPermissions(){
    return [
      'group_name' => 'Event',
      'permissions' => [
        [
          'name' => 'add-events',
          'display_name' => 'Add Event',
          'description' => 'User can create events',
        ],
        [
          'name' => 'view-events',
          'display_name' => 'View Events',
          'description' => 'User can view events'
        ],
        [
          'name' => 'edit-events',
          'display_name' => 'Edit/Update Events',
          'description' => 'User can edit and update event',
        ],
        [
          'name' => 'delete-events',
          'display_name' => 'Delete Events',
          'description' => 'User can delete events',
        ],
      ],
    ];
  }

  public static function getSessionPermissions(){
    return [
      'group_name' => 'Session',
      'permissions' => [
        [
          'name' => 'add-sessions',
          'display_name' => 'Add Sessions',
          'description' => 'User can add sessions'
        ],
        [
          'name' => 'view-sessions',
          'display_name' => 'View Sessions',
          'description' => 'User can view session'
        ],
        [
          'name' => 'edit-sessions',
          'display_name' => 'Edit/Update Sessions',
          'description' => 'User can edit and update sessions'
        ],
        [
        'name' => 'delete-sessions',
        'display_name' => 'Delete Sessions',
        'description' => 'User can delete sessions'
      ]
      ],
    ];
  }
  public static function getCommunicationPermissions(){
    return [
      'group_name' => 'Communication',
      'permissions' => [
        [
          'name' => 'send-emails',
          'display_name' => 'Send Emails',
          'description' => 'User can send emails',
        ],
        [
          'name' => 'read-emails',
          'display_name' => 'Read Emails',
          'description' => 'User can read emails',
        ],
        [
          'name' => 'delete-emails',
          'display_name' => 'Delete Emails',
          'description' => 'User can delete emails',
        ],
        [
          'name' => 'send-sms',
          'display_name' => 'Send SMS',
          'description' => 'User can send SMS',
        ],
        [
          'name' => 'view-sms',
          'display_name' => 'View SMS',
          'description' => 'User can view SMS',
        ],
        [
          'name' => 'delete-sms',
          'display_name' => 'Delete SMS',
          'description' => 'User can delete SMS',
        ],
      ],
    ];
  }
  public static function getAccountingPermissions(){
    return [
        'group_name' => 'Accounting',
        'permissions' => [
          [
            'name' => 'perform-actions',
            'display_name' => 'Perform Actions',
            'description' => 'User can perform accounting actions'
          ],
        ],
    ];
  }

  public static function getSettingsPermissions(){
    return [
      'group_name' => 'Settings',
      'permissions' => [
        [
          'name' => 'update-roles-permissions',
          'display_name' => 'Update Roles and Permissions',
          'description' => 'User can update roles and permissions',
        ],
      ]
    ];
  }
}
