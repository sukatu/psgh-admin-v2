<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $roles = self::getRoles();
      foreach ($roles as $roleArray) {
        $permissions =  $roleArray['permissions'];
        unset($roleArray['permissions']);
        $role = Role::where('name', $roleArray['name'])->first();
        if(!$role){
           $role = new Role;
        }
        $role->fill($roleArray);
        $role->save();
        foreach ($permissions as $permission) {
          if(!$role->hasPermission($permission)){
            $role->attachPermission($permission);
          }
        }
      }
    }

    public static function getRoles(){
      return [
        [
          'name' => 'super_admin',
          'display_name' => 'Super Admin',
          'description' => 'Has All Privileges',
          'permissions' => [
            'add-users', 'delete-users', 'edit-users', 'view-users',
            'add-members', 'view-members', 'edit-members', 'delete-members',
            'add-events', 'view-events', 'edit-events', 'delete-events',
            'add-sessions', 'view-sessions', 'edit-sessions', 'delete-sessions',
            'send-emails','read-emails','delete-emails','send-sms','view-sms','delete-sms',
            'perform-actions',
            'update-roles-permissions'
          ]
        ],
        [
          'name' => 'admin',
          'display_name' => 'Admin',
          'description' => 'Has all previleges but cannot delete users',
          'permissions' => [
            'add-users','edit-users', 'view-users',
            'add-members', 'view-members', 'edit-members', 'delete-members',
            'add-events', 'view-events', 'edit-events', 'delete-events',
            'add-sessions', 'view-sessions', 'edit-sessions', 'delete-sessions',
            'send-emails','read-emails','delete-emails','send-sms','view-sms','delete-sms',
            'perform-actions',
            'update-roles-permissions'
          ]
        ],
        [
          'name' => 'guest',
          'display_name' => 'Guests',
          'description' => 'Perform Data Entries',
          'permissions' => [
            'add-sessions', 'view-sessions', 'edit-sessions', 'delete-sessions',
          ]
        ],
      ];
    }
}
