var BlankonUiFeatureNotifications = function () {

  return {


    init: function () {
      BlankonUiFeatureNotifications.initNoty();
    },


    initNoty: function () {
      window.anim = {};
      window.anim.open = 'flipInX';
      window.anim.close = 'flipOutY';

      $('#payment-form').on('submit', function (e) {
        var $text;
        var $type;
        e.preventDefault();


        $.ajax({
          url: 'add-payment-form',
          type: 'post',
          async: false,
          data: new FormData(this),
          processData: false,
          cache: false,
          contentType: false,
          success: function(response){
            $text = response.payload;
            $type = response.type;
            $('#payment-form').trigger('reset');
            
          },

        });

        var self = $(this);

        noty({
          text        : $text,
          type        : $type,
          theme       : 'relax',
          dismissQueue: true,
          layout      : self.data('layout'),
          animation   : {
            open  : 'animated ' + window.anim.open,
            close : 'animated ' + window.anim.close
          },



        });

        return false;
      });
    }

  };

}();

// Call main app init
BlankonUiFeatureNotifications.init();
