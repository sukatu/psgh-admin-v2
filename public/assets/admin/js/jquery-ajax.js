$(document).ready(function(e){
  $('#members-title').on('change', function(e){
    $('.replace-content').fadeOut();
    $.ajax({
      url: 'search-member/'+$(this).val(),
      type: 'get',
      success: function(response){
        $('.replace-content').fadeIn('slow').html(response);
      }
    });
  });
});
