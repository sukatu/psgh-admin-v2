# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.25)
# Database: GLOBTROT
# Generation Time: 2020-02-27 15:41:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table app_routes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `app_routes`;

CREATE TABLE `app_routes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) DEFAULT NULL,
  `display_name` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `app_routes` WRITE;
/*!40000 ALTER TABLE `app_routes` DISABLE KEYS */;

INSERT INTO `app_routes` (`id`, `name`, `display_name`)
VALUES
	(1,'home','Dashboard'),
	(2,'hotels','Hotels'),
	(3,'create-hotel','Add Hotel'),
	(4,'rooms','Room Types'),
	(5,'add-room','Add New Room'),
	(6,'hotel-bookings','Hotel Bookings'),
	(7,'add-hotel-booking','Add Hotel Booking'),
	(8,'users','Users'),
	(9,'add-user','Add User'),
	(10,'roles','Roles'),
	(11,'roles-and-permissions','Roles & Permissions'),
	(12,'add-role','Add Role'),
	(13,'edit-hotel','Edit Hotel'),
	(14,'edit-hotel-room','Edit Room'),
	(15,'edit-user','Edit User'),
	(16,'view-users-with-role','Users'),
	(17,'edit-role','Roles');

/*!40000 ALTER TABLE `app_routes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
