$(document).ready(function(e){

  $("form").submit(function(e){
    if ($('#recipient').val() === '') {
      e.preventDefault();
      $('.recipient-error').show();
    }
    if ($('#ccbcc').is(':visible')) {
      if ($('#profile').val() === '') {
        e.preventDefault(e);
        $('.profile-category-error').show();
      }
    }
    if ($('#profile-categories').is(':visible')) {
      if ($('#profile-category-details').val() === '') {
        e.preventDefault();
        $('.profile-category-details-error').show();
      }else if ($('#subject').val() === '') {
        e.preventDefault();
        $('.subject-error').show();
      }

    }

    if($('#email-member').is(':visible')){
      if ($('#mem_email').val() === '') {
        $('.member-email-error').show();
      }
    }

    if ($('#subject').val() === '') {
      e.preventDefault();
      $('.subject-error').show();
    }
    if($('textarea').val() === ''){
      e.preventDefault();
      $('.body-error').show();
    }
  });

  $('#recipient').on('change', function(e){
    $('.recipient-error').hide();
    var $value = $(this).val();
    if ($value === 'Profile') {
      $('#ccbcc').fadeIn();
      $('#email-member').fadeOut();
    }else if ($value === 'Individual') {
      $('#ccbcc').fadeOut();
      $('#email-member').fadeIn();
    }
    else {
      $('#ccbcc').fadeOut();
    }
  });

  $('.sms-form').on('submit', function(e){
    var $schedule;
    var $scheduleDateData;
    var $scheduleTimeData;
    if ($('#schedule').is(':checked')) {
      $schedule = true;
      $scheduleDateData = $('.scheduleDateData').html();
      $scheduleTimeData = $('.scheduleTimeData').html();
    }else{
      $schedule = false
    }

    e.preventDefault();
    $('.loading').modal('show');
    $.ajax({
      url: 'get-sms-summary',
      type: 'post',
      processData: false,
      contentType: false,
      cache: false,
      data: new FormData(this),
      success: function(response){
        console.log(response);
        $('.loading').modal('hide');
        $('.bs-example-modal-sm').modal('show');
        var $message = $('#compose-editor').val();
        var $type = response.type;
        var $available_amount = response.available_amount;
        var $balance_amount = response.balance_amount;
        var $balance_sms = response.balance_sms;
        var $cost = response.cost;
        var $count_members = response.count_members;
        var $no_of_sms = response.no_of_sms;
        var $rate = response.rate;
        var $total_amount_used = response.total_amount_used;
        var $total_remaining = response.total_remaining;
        var $total_sms_used = response.total_sms_used;

        $('#recipients').html("<u><strong>"+$count_members+"</strong></u>");
        // $('#rate').html("<u><strong>"+$rate+"</strong></u>");
        $('#noSms').html("<u><strong>"+$no_of_sms+"</strong></u>");
        // $('#cost').html("<u><strong>"+$cost+"</strong></u>");
        // $('#totalAmountUsed').html("<u><strong>"+$total_amount_used+"</strong></u>");
        $('#totalRemaining').html("<u><strong>"+$total_remaining+"</strong></u>");
        $('#totalSmsUsed').html("<u><strong>"+$total_sms_used+"</strong></u>");

        $('.scheduleDateData').val
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $('.confirm-sms').on('click', function(e){
          e.preventDefault();
          $('.bs-example-modal-sm').modal('hide');
          $('#loading-title').html('Sending...')
          $('.loading').modal('show');
          $.ajax({
            url: 'send-confirm-sms',
            type: 'post',
            data:{'available_amount':$available_amount, 'balance_amount':$balance_amount,'balance_sms':$balance_sms,'cost':$cost,'count_members':$count_members,'no_of_sms':$no_of_sms,'rate':$rate,'total_amount_used':$total_amount_used,'total_remaining':$total_remaining,'total_sms_used':$total_sms_used, 'type':$type, 'message':$message, 'schedule':$schedule, 'scheduleDateData':$scheduleDateData, 'scheduleTimeData':$scheduleTimeData},
            success: function(response){
              console.log(response);
              if (response.success === '001') {
                $('.loading').modal('hide');
                swal("Sent!", "Your messages have been sent!", "success");
              }
            }
          })
        });
      }
    })
  });
  $('#schedule').on('click', function(e){
    $('.schedule').modal('show');
  });
  $('#btnOnSchedule').on('click', function(e){
    e.preventDefault();
    $('.scheduleDateData ').html($('#date').val());
    $('.scheduleDateData').css('color','green');
    $('.scheduleDateData').css('font-weight', 'bold');
    $('.scheduleTimeData').html($('#time').val());
    $('.schedule-input').val(true);
    $('.scheduleDateData-input').val($('#date').val());
    $('.scheduleTimeData-input').val($('#time').val());
    $('.schedule').modal('hide').fadeOut();
  });
  $('#subject').on('keyup', function(){
    $('.subject-error').fadeOut();
  });

  $('#mem_email').on('change', function(e){
    $('.member-email-error').fadeOut();
  });
  $('#summernote').on('keyup', function(e){
    $('.body-error').hide();
  });
  $('#profile').on('change', function(e){
    e.preventDefault();
    $('.profile-category-error').hide();
    $('.loading').modal('show');
    $.ajax({
      url: 'get-profile-categories/'+$(this).val(),
      type:'get',
      success: function(response){
        $('.loading').modal('hide');
        $('.profile-categories-section').html(response);
      }
    })
  })
});


function myFunction1() {
  var copyText = document.getElementById("myInput1");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction2() {
  var copyText = document.getElementById("myInput2");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction3() {
  var copyText = document.getElementById("myInput3");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction4() {
  var copyText = document.getElementById("myInput4");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction5() {
  var copyText = document.getElementById("myInput5");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction6() {
  var copyText = document.getElementById("myInput6");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction7() {
  var copyText = document.getElementById("myInput7");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction8() {
  var copyText = document.getElementById("myInput8");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction9() {
  var copyText = document.getElementById("myInput9");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction10() {
  var copyText = document.getElementById("myInput10");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction11() {
  var copyText = document.getElementById("myInput11");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction12() {
  var copyText = document.getElementById("myInput12");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction13() {
  var copyText = document.getElementById("myInput13");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction14() {
  var copyText = document.getElementById("myInput14");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction15() {
  var copyText = document.getElementById("myInput15");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction16() {
  var copyText = document.getElementById("myInput16");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction17() {
  var copyText = document.getElementById("myInput17");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}

function myFunction18() {
  var copyText = document.getElementById("myInput18");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  document.execCommand("copy");
  alert("Copied the text: " + copyText.value);
}
