Dear {{ $first_name }} {{ $middle_name }} {{ $constituent_id }}

Congratulations,

You have earned “1” CPD Credits for attending the Event “PHARMACEUTICAL SOCIETY OF GHANA, 2019 AGM”

We appreciate your registration, attendance and participation.

Continue to patronize all PSGH activities.

Thanks
Admin
