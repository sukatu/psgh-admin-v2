<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Society Manager is a fullpack administration software powered by SIMSOFT front-end framework. Included are multiple evetns management, session, members etc. Just request for a demo to get your society started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>SIGN IN | Society Manager :: SimSoft</title>
  <link href="{{ asset('../../../img/ico/html/apple-touch-icon-144x144-precomposed.png') }}" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="{{ asset('../../../img/ico/html/apple-touch-icon-114x114-precomposed.png') }}" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="{{ asset('../../../img/ico/html/apple-touch-icon-72x72-precomposed.png') }}" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="{{ asset('../../../img/ico/html/apple-touch-icon-57x57-precomposed.png') }}" rel="apple-touch-icon-precomposed">
  <link href="{{ asset('../../../img/ico/html/apple-touch-icon.png') }}" rel="shortcut icon">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="{{ asset('../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('../../../assets/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('../../../assets/admin/css/reset.css') }}" rel="stylesheet">
  <link href="{{ asset('../../../assets/admin/css/layout.css') }}" rel="stylesheet">
  <link href="{{ asset('../../../assets/admin/css/components.css') }}" rel="stylesheet">
  <link href="{{ asset('../../../assets/admin/css/plugins.css') }}" rel="stylesheet">
  <link href="{{ asset('../../../assets/admin/css/themes/default.theme.css') }}" rel="stylesheet" id="theme">
  <link href="{{ asset('../../../assets/admin/css/pages/sign.css') }}" rel="stylesheet">
  <link href="{{ asset('../../../assets/admin/css/custom.css') }}" rel="stylesheet">
</head>
<body class="page-sound">
  <div id="sign-wrapper">
    <div class="brand">
      <img src="{{ asset('../../../img/society-logo-small.png') }}" alt="brand logo"/>
    </div>
    <form class="sign-in form-horizontal shadow rounded no-overflow" action="{{ route('login') }}" method="post">
      @csrf
      <div class="sign-header">
        <div class="form-group">
          <div class="sign-text">
            <span>Member Area</span>
          </div>
        </div>
      </div>
      <div class="sign-body">
        <div class="form-group">
          <div class="input-group input-group-lg rounded no-overflow">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            <span class="input-group-addon"><i class="fa fa-user"></i></span>
          </div>

          @error('email')
          <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
        <div class="form-group">
          <div class="input-group input-group-lg rounded no-overflow">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            @error('password')
            <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
            </span>
            @enderror
          </div>
        </div>
      </div>
      <div class="sign-footer">
        <div class="form-group">
          <div class="row">
            <div class="col-xs-6">
              <div class="ckbox ckbox-theme">
                <input id="rememberme" type="checkbox">
                <label for="rememberme" class="rounded">Remember me</label>
              </div>
            </div>
            <div class="col-xs-6 text-right">
              <a href="{{ route('lost-password') }}" title="lost password">Lost password?</a>
            </div>
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-theme btn-lg btn-block no-margin rounded" id="login-btn">Sign In</button>
        </div>
      </div>
    </form>
    <p class="text-muted text-center sign-link">Are you fascinated? <a href="page-signup.html"> Request for a demo</a></p>
  </div>
  <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js') }}"></script>
  <script src="{{ asset('../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js') }}"></script>
  <script src="{{ asset('../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js') }}"></script>
  <script src="{{ asset('../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js') }}"></script>
  <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('../../../assets/admin/js/pages/blankon.sign.js"></script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55892530-1', 'auto');
  ga('send', 'pageview');

  </script>
</body>
</html>
