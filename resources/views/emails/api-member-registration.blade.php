<p>Dear {{ $first_name}} {{ $last_name }},</p>
Welcome to the <strong>Pharmaceutical Society of Ghana (PSGH)</strong>.
Your registration with Pharmacy Council is acknowledged as a prerequisite for registration with the PSGH.
<br>
<p>Kindly use the following link to complete your registration with the PSGH. 'https://psgh.thesocietymanager.com/'
With respect to the Pharmaceutical Society of Ghana: Your Registration Number is {{ $registration_number }}.</p>
<p>You are required to update/change your password the first time you access the online portal.</p>
<p>Hence your username is psghxxxx and your default password is xxxxpsgh. (where xxxx is the registration number) E.g. psgh9876 and 9876psgh.</p>
<br>
<p>Note: Please change the default password to your own preferred password, after you have logged in, by clicking on the profile icon and name and then change password.
You are required to confirm the profile details, update and provide additional information where necessary.</p>
Also upload your head-shot photo.
<br>
<p>Thanks</p>
<p>Admin</p>
