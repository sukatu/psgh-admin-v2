<aside id="sidebar-left" class="sidebar-circle">
  <div id="tour-8" class="sidebar-content">
    <div class="media">
      <a class="pull-left has-notif avatar" href="home">
        <img src="{{ asset('images/avatar1.png') }}" alt="admin">
        <i class="online"></i>
      </a>
      <div class="media-body">
        <h4 class="media-heading">Hello, <span>User</span></h4>
      </div>
    </div>
  </div>
  <ul id="tour-9" class="sidebar-menu">
    <li class="submenu active">
      <a href="{{ URL::route('dashboard') }}">
        <span class="icon"><i class="fa fa-home"></i></span>
        <span class="text">Dashboard</span>
        <span class="selected"></span>
      </a>
    </li>
    <li class="submenu">
      <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-users"></i></span>
        <span class="text">Profile</span>
        <span class="arrow"></span>
      </a>
      <ul>
        <li><a href="{{ URL::route('member-list') }}">Members Lists</a></li>
        <li><a href="{{ route('member-registration') }}">New Member Registration</a></li>
        <li> <a href="{{ route('member-promotions') }}">Promotions</a> </li>
      </ul>
    </li>
    @if(Laratrust::can('add-events'))
    <li class="submenu">
      <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-list"></i></span>
        <span class="text">Event</span>
        <span class="arrow"></span>
      </a>
      <ul>
        <li><a href="#"> Setup</a></li>
        <!-- <li><a href="{{ route('registrants') }}">Event Registration</a></li> -->
        <li><a href="{{ route('attendees') }}">Event Attendance</a></li>
        <li><a href="{{ route('register-attendee') }}">Event Registration</a></li>
        <li class="submenu">
          <a href="javascript:void(0);">
            <span class="text">Session</span>
            <span class="arrow"></span>
          </a>
          <ul>
            <li><a href="{{ route('add-session') }}">Setup</a></li>
            <li><a href="{{ route('sessions') }}">Attendance</a></li>
          </ul>
        </li>
      </ul>
    </li>
    @endif
    @if(Laratrust::can('read-emails'))
    <li class="submenu">
      <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-paper-plane"></i></span>
        <span class="text">Communication</span>
        <span class="arrow"></span>
      </a>
      <ul>
        <li><a href="{{ route('sms') }}">SmS</a></li>
        <li><a href="{{ route('mail') }}">Mail</a></li>
      </ul>
    </li>
    @endif
    <li class="submenu">
      <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-money"></i></span>
        <span class="text">Member Dues</span>
        <span class="arrow"></span>
      </a>
      <ul>
        <li><a href="{{ route('accounting') }}">Member Standings</a></li>
        <li><a href="{{ route('payment') }}">Member Dues</a></li>
        <li><a href="{{ route('membership') }}">Dues Type</a>
        </li>
      </ul>
    </li>
    <li class="sidebar-category">
      <span>TASKS</span>
      <span class="pull-right"><i class="fa fa-settings"></i></span>
    </li>
    <li class="submenu">
      <a href="#" data-toggle="modal" data-target="#myModal">
        <span class="icon"><i class="fa fa-globe"></i></span>
        <span class="text">Print Badge</span>

      </a>
    </li>

    <li class="sidebar-category">
      <span>SETTINGS</span>
      <span class="pull-right"><i class="fa fa-cogs"></i></span>
    </li>
    @if(Laratrust::can('update-roles-permissions'))
    <li class="submenu">
      <a href="{{ route('roles-permissions') }}">
        <span class="icon"><i class="fa fa-globe"></i></span>
        <span class="text">Roles & Permissions</span>
      </a>
    </li>
    @endif
    @if(Laratrust::can('add-users'))
    <li class="submenu">
      <a href="javascript:void(0);">
        <span class="icon"><i class="fa fa-user"></i></span>
        <span class="text">Users</span>
        <span class="arrow"></span>
      </a>
      <ul>
        <li class="submenu">
          <ul>
            <li><a href="blog-grid.html">Explore Users</a></li>
            <li><a href="blog-grid-type-2.html">Add User</a></li>
          </ul>
        </li>
        <li><a href="{{ route('explore-users') }}">Explore Users</a></li>
        <li><a href="{{ route('add-user') }}">Add User</a></li>
      </ul>
    </li>
    @endif
  </ul>
  <div id="tour-10" class="sidebar-footer hidden-xs hidden-sm hidden-md">
    <a id="fullscreen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Fullscreen"><i class="fa fa-desktop"></i></a>
    <a class="dropdown-item" href="{{ route('logout') }}"
    onclick="event.preventDefault();
    document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
    {{ __('Logout') }}
  </a>

  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
  </form>
</a>
</div>
</aside>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form class="" action="{{ route('print-single-badge')}}" method="post">
      @csrf
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Registration ID</h4>
        </div>
        <div class="modal-body">
          <input type="text" class="form-control" name="constituent_id" value="">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </form>
  </div>
</div>
