<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
        <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
        <meta name="author" content="Djava UI">
  <title>PSGH | Admin</title>
          <link href="{{ asset('../../../img/ico/html/apple-touch-icon-144x144-precomposed.png') }}" rel="apple-touch-icon-precomposed" sizes="144x144">
        <link href="{{ asset('../../../img/ico/html/apple-touch-icon-114x114-precomposed.png') }}" rel="apple-touch-icon-precomposed" sizes="114x114">
        <link href="{{ asset('../../../img/ico/html/apple-touch-icon-72x72-precomposed.png') }}" rel="apple-touch-icon-precomposed" sizes="72x72">
        <link href="{{ asset('../../../img/ico/html/apple-touch-icon-57x57-precomposed.png') }}" rel="apple-touch-icon-precomposed">
        <link href="{{ asset('../../../img/ico/html/apple-touch-icon.png') }}" rel="shortcut icon">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/dropzone/downloads/css/dropzone.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/jquery.gritter/css/jquery.gritter.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/admin/css/reset.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/admin/css/layout.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/admin/css/components.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/admin/css/plugins.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/admin/css/themes/default.theme.css') }}" rel="stylesheet" id="theme">
        <link href="{{ asset('../../../assets/admin/css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css')}}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/datatables/css/datatables.responsive.css')}}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/fuelux/dist/css/fuelux.min.css')}}" rel="stylesheet">
        <link href="{{ asset('../../../assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css') }}" rel="stylesheet">

    </head>
    <body class="page-session page-sound page-header-fixed page-sidebar-fixed demo-dashboard-session">
        <section id="wrapper">
            @include('layouts.header')
            @include('layouts.aside')
            <section id="page-content">
              @yield('content')
            </section>
        </section>
        @include('layouts.footer')

        <div id="back-top" class="animated pulse circle">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- @include('layouts.tutorials') -->
        <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/bootbox/bootbox.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/flot/jquery.flot.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/flot/jquery.flot.spline.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/flot/jquery.flot.categories.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/flot/jquery.flot.resize.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/flot/jquery.flot.pie.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/dropzone/downloads/dropzone.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/jquery.gritter/js/jquery.gritter.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/skycons-html5/skycons.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/waypoints/lib/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/counter-up/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js') }}"></script>
        <script src="{{ asset('../../../assets/admin/js/apps.js') }}"></script>
        <script src="{{ asset('../../../assets/admin/js/pages/blankon.dashboard.js') }}"></script>
        <script src="{{ asset('../../../assets/admin/js/demo.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js')}}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/datatables/js/datatables.responsive.js')}}"></script>
        <script src="{{ asset('../../../assets/global/plugins/bower_components/fuelux/dist/js/fuelux.min.js')}}"></script>
        <script type="text/javascript" src="js/jquery.qrcode.min.js"></script>

        <script type="text/javascript">
        $(document).ready(function(e){
              $('#qrcode').qrcode({width: 45,height: 45,text: "91000"});
                $('#qrcode2').qrcode({width: 65,height: 65,text: "2333, Edward, Amponsah, Mr, Member, Western, Community, Industry, EDDYAMPS PHARMACY LTD, 024444444"});
        });

        </script>

    </body>
</html>
