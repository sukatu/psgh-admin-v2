<header id="header">
    <div class="header-left">
        <div class="navbar-minimize-mobile left">
            <i class="fa fa-bars"></i>
        </div>
        <div class="navbar-header">
            <a id="tour-1" class="navbar-brand" href="{{ route('home') }}">
                <img class="logo" src="{{ asset('../../../img/logo-white-new.png') }}" alt="brand logo"/>
            </a>
        </div>
        <div class="navbar-minimize-mobile right">
            <i class="fa fa-cog"></i>
        </div>

        <div class="clearfix"></div>
    </div>
    <div class="header-right">
        <div class="navbar navbar-toolbar">
            <ul class="nav navbar-nav navbar-left">
                <li id="tour-2" class="navbar-minimize">
                    <a href="javascript:void(0);" title="Minimize sidebar">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
                <li class="navbar-search">
                    <a href="dashboard.html#" class="trigger-search"><i class="fa fa-search"></i></a>
                    <form id="" action="../search-result-page" method="get" class="navbar-form">
                        <div class="form-group has-feedback">
                            <input type="text" required class="form-control typeahead rounded" placeholder="Search for people, places and things">
                            <button type="submit" class="btn btn-theme fa fa-search form-control-feedback rounded"></button>
                        </div>
                    </form>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
            <!-- Start notifications -->
            <li id="tour-5" class="dropdown navbar-notification">

                <a href="dashboard.html#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o"></i><span class="count label label-danger rounded">6</span></a>

                <!-- Start dropdown menu -->
                <div class="dropdown-menu animated flipInX">
                    <div class="dropdown-header">
                        <span class="title">Notifications <strong>(10)</strong></span>
                        <span class="option text-right"><a href="dashboard.html#"><i class="fa fa-cog"></i> Setting</a></span>
                    </div>
                    <div class="dropdown-body niceScroll">

                        <!-- Start notification list -->
                        <div class="media-list small">

                            <a href="dashboard.html#" class="media">
                                <div class="media-object pull-left"><i class="fa fa-share-alt fg-info"></i></div>
                                <div class="media-body">
                                    <span class="media-text"><strong>Dolanan Remi : </strong><strong>Chris Job,</strong><strong>Denny Puk</strong> and <strong>Joko Fernandes</strong> sent you <strong>5 free energy boosts and other request</strong></span>
                                    <!-- Start meta icon -->
                                    <span class="media-meta">3 minutes</span>
                                    <!--/ End meta icon -->
                                </div><!-- /.media-body -->
                            </a><!-- /.media -->

                            <a href="dashboard.html#" class="media">
                                <div class="media-object pull-left"><i class="fa fa-cogs fg-success"></i></div>
                                <div class="media-body">
                                    <span class="media-text">Your sistem is updated</span>
                                    <!-- Start meta icon -->
                                    <span class="media-meta">5 minutes</span>
                                    <!--/ End meta icon -->
                                </div><!-- /.media-body -->
                            </a><!-- /.media -->

                            <a href="dashboard.html#" class="media">
                                <div class="media-object pull-left"><i class="fa fa-ban fg-danger"></i></div>
                                <div class="media-body">
                                    <span class="media-text">3 Member is BANNED</span>
                                    <!-- Start meta icon -->
                                    <span class="media-meta">5 minutes</span>
                                </div>
                            </a>
                            <a href="dashboard.html#" class="media">
                                <div class="media-object pull-left"><img class="img-circle" src="../../../img/avatar/50/10.png" alt=""/></div>
                                <div class="media-body">
                                    <span class="media-text">daddy pushed to project Blankon version 1.0.0</span>
                                    <span class="media-meta">45 minutes</span>
                                </div>
                            </a>
                            <a href="dashboard.html#" class="media">
                                <div class="media-object pull-left"><i class="fa fa-user fg-info"></i></div>
                                <div class="media-body">
                                    <span class="media-text">Change your user profile</span>
                                    <span class="media-meta">1 days</span>
                                </div>
                            </a>
                            <a href="dashboard.html#" class="media">
                                <div class="media-object pull-left"><i class="fa fa-book fg-info"></i></div>
                                <div class="media-body">
                                    <span class="media-text">Added new article</span>
                                    <span class="media-meta">1 days</span>
                                </div>
                            </a>
                            <a href="dashboard.html#" class="media indicator inline">
                                <span class="spinner">Load more notifications...</span>
                            </a>
                        </div>
                    </div>
                    <div class="dropdown-footer">
                        <a href="dashboard.html#">See All</a>
                    </div>
                </div>

            </li>
            <li id="tour-6" class="dropdown navbar-profile">
                <a href="dashboard.html#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="meta">
                        <span class="avatar"><img src="{{ asset('images/avatar1.png') }}" class="img-circle" alt="admin"></span>
                        <span class="text hidden-xs hidden-sm text-muted">{{ Auth::user()->name }}</span>
                        <span class="caret"></span>
                    </span>
                </a>
                <ul class="dropdown-menu animated flipInX">
                    <li class="divider"></li>
                    <li>  <a class="dropdown-item" href="{{ URL::route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                          {{ __('Logout') }}
                      </a>

                      <form id="logout-form" action="{{ URL::route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form></li>
                </ul>
            </li>
            </ul>
        </div>
    </div>
</header>
