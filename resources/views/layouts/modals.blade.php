<!-- Modal for displaying sms Summary -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">SMS Summary</h4>
      </div>
      <div class="modal-body">
        <table width="100%">
          <thead>
            <tr>
              <th>Description</th>
              <th>Value</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Total Recipient(s):</td>
              <td class="text-success" id="recipients"></td>
            </tr>
            <!-- <tr>
            <td>Rate:</td>
            <td class="text-success" id="rate"></td>
          </tr> -->
          <tr>
            <td>No. of SMS:</td>
            <td class="text-success" id="noSms"></td>
          </tr>
          <!-- <tr>
          <td>Cost:</td>
          <td class="text-success" id="cost"></td>
        </tr> -->
        <!-- <tr>
        <td>Total Amount Used:</td>
        <td class="text-success" id="totalAmountUsed"></td>
      </tr> -->
      <tr>
        <td>Total Remaining</td>
        <td class="text-success" id="totalRemaining"></td>
      </tr>
      <tr>
        <td>Total SmS Used</td>
        <td class="text-success" id="totalSmsUsed"></td>
      </tr>
    </tbody>
  </table>

</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-theme confirm-sms"> <i class="fa fa-paper-plane"></i> Confirm</button>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<div class="modal fade loading" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <p id="loading-title">Please wait&hellip;</p>
        <div class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade schedule" tabindex="-1" data-backdrop="static" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <p id="">Scheduling SMS</p>
        <input type="date" id="date" name="" value=""> <input type="time" id="time" name="" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-theme confirm-sms" id="btnOnSchedule"> <i class="fa fa-paper-plane"></i> Ok</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Macr (pre-defined information)</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- The text field -->
        @foreach(App\Macro::all() as $macro)
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-5">
              <input type="text" value="{{ $macro->format }}" style="display: none;" id="myInput{{ $macro->id }}">
              <strong>{{ $macro->format }} </strong>
            </div>
            <div class="col-md-3">
              <button onclick="myFunction{{ $macro->id }}()" class="btn btn-primary">Copy</button>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>

</div>
