<!DOCTYPE html><html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>{{ config('app.name', 'Society Manager') }}</title>
  <link href="../../../img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="../../../img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="../../../img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="../../../img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
  <link href="../../../img/ico/html/apple-touch-icon.png" rel="shortcut icon">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/bootstrap-fileupload/css/bootstrap-fileupload.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css" rel="stylesheet">
  <link href="../../../assets/commercial/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css" rel="stylesheet">
  <link href="../../../assets/admin/css/reset.css" rel="stylesheet">
  <link href="../../../assets/admin/css/layout.css" rel="stylesheet">
  <link href="../../../assets/admin/css/components.css" rel="stylesheet">
  <link href="../../../assets/admin/css/plugins.css" rel="stylesheet">
  <link href="../../../assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
  <link href="../../../assets/admin/css/custom.css" rel="stylesheet">
  <link href="../../../assets/admin/css/pages/search.css" rel="stylesheet">
  <link href="../../../assets/admin/css/pages/search-basic.css" rel="stylesheet">
  <link rel="stylesheet" href="swal/bower_components/qunit/qunit/qunit.css">
  <link rel="stylesheet" href="swal/dist/sweetalert.css">
  <script src="{{ asset('js/downloaded/jquery.js') }}"></script>
  <script src="{{ asset('js/downloaded/bootstrap.js') }}"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">
  <script src="{{ asset('js/downloaded/summernote.js') }}"></script>

  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <style media="screen">
  .tooltip {
position: relative;
display: inline-block;
}

.tooltip .tooltiptext {
visibility: hidden;
width: 140px;
background-color: #555;
color: #fff;
text-align: center;
border-radius: 6px;
padding: 5px;
position: absolute;
z-index: 1;
bottom: 150%;
left: 50%;
margin-left: -75px;
opacity: 0;
transition: opacity 0.3s;
}

.tooltip .tooltiptext::after {
content: "";
position: absolute;
top: 100%;
left: 50%;
margin-left: -5px;
border-width: 5px;
border-style: solid;
border-color: #555 transparent transparent transparent;
}

.tooltip:hover .tooltiptext {
visibility: visible;
opacity: 1;
}
  </style>
</head>
<body class="page-header-fixed page-sidebar-fixed">
  <section id="wrapper">
    @include('layouts.header')
    @include('layouts.aside')
    <section id="page-content">
      @yield('content')
    </section>
  </section>
  @include('layouts.footer')
  @include('layouts.modals')
  <div id="back-top" class="animated pulse circle">
    <i class="fa fa-angle-up"></i>
  </div>
  
  <script src="../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootstrap-fileupload/js/bootstrap-fileupload.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js"></script>
  <script src="../../../assets/admin/js/apps.js"></script>
  <script src="../../../assets/admin/js/pages/blankon.form.layout.js"></script>
  <script src="{{ asset('assets/admin/js/jquery-ajax.js')}}" charset="utf-8"></script>
  <script src="../../../assets/commercial/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
  <script src="../../../assets/admin/js/pages/blankon.search.basic.js"></script>
  <script src="js/custom-ajax.js" charset="utf-8"></script>

  <script src="swal/dist/sweetalert.min.js"></script>


  <script src="{{ asset('js/downloaded/qunit-1.1.18.0.js') }}"></script>

  <script>
  $(document).ready(function() {
    $('#summernote').summernote();
  });
  </script>

</body>
</html>
