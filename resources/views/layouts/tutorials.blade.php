<div class="modal modal-success fade" id="modal-bootstrap-tour" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Welcome to Blankon</h4>
            </div>
            <div class="modal-body">
                <div class="media">
                    <div class="media-left" style="padding-right: 15px;">
                        <a href="dashboard.html#">
                            <img data-no-retina class="media-object" src="../../../img/media/mascot/1.jpg" alt="..." style="width: 100px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Hello, my name is Mr.Blankon</h4>
                        <b>Introduction</b> - Blankon fullpack admin theme is a theme full pack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="BlankonDashboard.callModal2()" data-dismiss="modal">What's New <i class="fa fa-rocket"></i></button>
                <button type="button" class="btn btn-primary" onclick="BlankonDashboard.handleTour()" data-dismiss="modal">Let's tour <i class="fa fa-arrow-circle-right"></i></button>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-success fade" id="modal-bootstrap-tour-new-features" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="margin: 50px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="BlankonDashboard.callModal1()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New Features</h4>
            </div>
            <div class="modal-body" style="height: 400px; overflow: scroll;">
                <img src="../../../img/admin-special/marketing-campaign/new-feature.jpg" alt="..." class="img-responsive" style="width: 500px; margin: 0 auto 25px;"/>
                <div class="table-responsive">
                    <table class="table table-default table-bordered table-condensed mb-20">
                        <thead>
                        <tr>
                            <th>Page</th>
                            <th>Description</th>
                            <th style="width: 90px" class="text-center">Live preview</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr class="border-success">
                                <td style="min-width: 150px">
                                    Blankon Marketing Campaign
                                </td>
                                <td>
                                    It is a design speciality for marketing campaign dashboard designs, there are many features for support all system marketing. Such as Dashboard design with overview count target media social, progress chart and widget marketing and amazing others design do you need!
                                </td>
                                <td class="text-center">
                                    <a href="../../admin-special/marketing-campaign/index.html" target="_blank" class="btn btn-block btn-primary">View</a>
                                </td>
                            </tr>
                            <tr class="border-danger">
                                <td style="min-width: 150px">
                                    Blankon HTML Static SeedProject
                                </td>
                                <td>
                                    HTML or HyperText Markup Language is the standard markup language used to create web pages.
                                </td>
                                <td class="text-center">
                                    <a href="../../admin-seed-project/html/dashboard.html" target="_blank" class="btn btn-block btn-primary">View</a>
                                </td>
                            </tr>
                            <tr class="border-primary">
                                <td style="min-width: 150px">
                                    Blankon AngularJS SeedProject
                                </td>
                                <td>
                                    AngularJS is an open-source web application framework, maintained by Google and community, that assists with creating single-page applications, which consist of one HTML page with CSS, and JavaScript on the client side.
                                </td>
                                <td class="text-center">
                                    <a href="../../admin-seed-project/angularjs/index.html#/dashboard" target="_blank" class="btn btn-block btn-primary">View</a>
                                </td>
                            </tr>
                            <tr class="border-danger">
                                <td style="min-width: 150px">
                                    Blankon AJAX SeedProject
                                </td>
                                <td>
                                    Ajax or short for asynchronous JavaScript + XML. is a group of interrelated Web development techniques used on the client-side to create asynchronous Web applications.
                                </td>
                                <td class="text-center">
                                    <a href="../../admin-seed-project/ajax/index.html#dashboard" target="_blank" class="btn btn-block btn-primary">View</a>
                                </td>
                            </tr>
                            <tr class="border-info">
                                <td style="min-width: 150px">
                                    Blankon PHP SeedProject
                                </td>
                                <td>
                                    PHP is a server-side scripting language designed for web development but also used as a general-purpose programming language.
                                </td>
                                <td class="text-center">
                                    <a href="../../admin-seed-project/php/index.php.html" target="_blank" class="btn btn-block btn-primary">View</a>
                                </td>
                            </tr>
                            <tr class="border-warning">
                                <td style="min-width: 150px">
                                    Blankon Material Design SeedProject
                                </td>
                                <td>
                                    It is a design language developed by Google. Material Design makes more liberal use of grid-based layouts, responsive animations and transitions, padding, and depth effects such as lighting and shadows.
                                </td>
                                <td class="text-center">
                                    <a href="../../admin-seed-project/material-design/dashboard.html" target="_blank" class="btn btn-block btn-primary">View</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-danger fade" id="modal-bootstrap-tour-end" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="margin: 150px auto;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">End Blankon Tour</h4>
            </div>
            <div class="modal-body">
                <div class="media">
                    <div class="media-left">
                        <a href="dashboard.html#">
                            <img data-no-retina class="media-object" src="../../../img/media/mascot/1.jpg" alt="..." style="width: 100px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Thanks for watching!</h4>
                        <p>Thank you for view our blankon tour. If you already purchased Blankon and then you have any questions that are beyond the scope of this help file. You can visit us on below :</p>
                        <ul class="list-inline">
                            <li>
                                <a href="https://wrapbootstrap.com/user/djavaui" class="btn btn-inverse tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Wrapbootstrap">W</a>
                            </li>
                            <li>
                                <a href="http://djavaui.com" class="btn btn-lilac tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Our Website"><i class="fa fa-globe"></i></a>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/djavaui/" class="btn btn-facebook tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Facebook"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="https://twitter.com/djavaui" class="btn btn-twitter tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Twitter"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/102744122511959250698" class="btn btn-googleplus tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Google+"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="https://github.com/djavaui" class="btn btn-default tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Github"><i class="fa fa-github"></i></a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCt_dudJF4_0bOkQkwYN2qQQ" class="btn btn-youtube tooltips" target="_blank" data-toggle="tooltip" data-placement="top" data-title="Youtube"><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>
                        <b>Thanks so much!</b>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="BlankonDashboard.handleTour()" data-dismiss="modal">Let's tour again <i class="fa fa-arrow-circle-right"></i></button>
            </div>
        </div>
    </div>
</div>
