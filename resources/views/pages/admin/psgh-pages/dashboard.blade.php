@extends('layouts.gen')
@section('content')
<div class="body-content animated fadeIn">
  <div class="row">
    <div class="col-md-7">
      <div class="row">
        <div class="col-lg-4 col-md-4">
          <div class="mini-stat-type-4 bg-success shadow">
            <h3>Members</h3>
            <h2 class="count">{{ App\Member::count() }}</h2>
            <a href="{{ route('member-list') }}" target="_blank" class="btn btn-success">VIEW</a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="mini-stat-type-4 bg-danger shadow">
            <h3>Deceased</h3>
            <h2 class="count">{{ App\Member::where('status', 'Deceased')->count() }}</h2>
            <a href="{{ route('member-list') }}" target="_blank" class="btn btn-danger">VIEW</a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="mini-stat-type-4 bg-primary shadow">
            <h3>Newly Qualified</h3>
            <h2 class="count">{{ App\Member::where('Member_Type_Code', 'NEW MEMBER')->count() }}</h2>
            <a href="{{ route('member-list') }}" target="_blank" class="btn btn-primary">VIEW</a>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="mini-stat-type-4 bg-primary shadow">
            <h5>Registered for Upcoming Event</h5>
            <h2 class="count">{{ App\EventRegistrant::count() }}</h2>
          </div>
        </div>
        <div class="col-lg-4 col-md-4">
          <div class="mini-stat-type-4 bg-primary shadow">
            <h5>Total Sessions this year</h5>
            <h2 class="count">{{ App\EventSession::count() }}</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
