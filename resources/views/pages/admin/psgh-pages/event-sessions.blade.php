
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>PSGH | Admin</title>  <link href="../../../img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="../../../img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="../../../img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="../../../img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
  <link href="../../../img/ico/html/apple-touch-icon.png" rel="shortcut icon">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/datatables/css/datatables.responsive.css" rel="stylesheet">
  <link href="../../../assets/admin/css/reset.css" rel="stylesheet">
  <link href="../../../assets/admin/css/layout.css" rel="stylesheet">
  <link href="../../../assets/admin/css/components.css" rel="stylesheet">
  <link href="../../../assets/admin/css/plugins.css" rel="stylesheet">
  <link href="../../../assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
  <link href="../../../assets/admin/css/custom.css" rel="stylesheet">
  <link rel="stylesheet" href="node_modules/sweetalert/dist/sweetalert.css">
  <style media="screen">
  @media all {

    .portrait{
      border: solid;
      border-color: black;
      border-width: thin;
      padding-left: 10px;
      padding-right: 25px!important;
      background-color: #ffff;
      width: 350px;
    }
    .event-title{
      font-size: 22px;
      text-align: center;
      margin-left: 13px;
    }
    #qrcode{
      margin-left: 10px;
    }
    #qrcode2{

    }
    .top{
      margin-top: 10px;
    }
    .sponsor{
      background: yellow;
      width: 70px;
      border: solid;
      border-color: black;
      padding-left: 10px;
      text-align: center;
      margin-left: -10px;
    }
  }
  </style>
</head>
<body class="page-sound page-header-fixed page-sidebar-fixed page-footer-fixed">
  <section id="wrapper">
    @include('layouts.header')
  @include('layouts.aside')
<!-- START @PAGE CONTENT -->
<section id="page-content">

  <!-- Start page header -->
  <div class="header-content">
    <h2>Event Sessions <span>all members who attended this event. ({{ $sessions->count() }})</span></h2>

  </div><!-- /.header-content -->
  <!--/ End page header -->

  <!-- Start body content -->
  <div class="body-content animated fadeIn">

    <div class="row">
      <div class="col-md-12">

        <div class="panel panel-tab panel-tab-double shadow">
          <!-- Start tabs heading -->
          <div class="panel-heading no-padding">
            <ul class="nav nav-tabs">
              <li class="active nav-border nav-border-top-success">
                <a href="additional-option-system-manage-metadata.html#tab-setting-countries" data-toggle="tab">
                  <i class="icon-globe icons fg-success"></i>
                  <div>
                    <span class="text-strong">Members</span>
                    <span>List members default</span>
                  </div>
                </a>
              </li>

            </ul>
            <a href="{{ route('add-session') }}" class="btn btn-success btn-add-setting-countries" style="position: absolute;right: 12px;top: 12px;">Add Session &nbsp;<i class="fa fa-plus"></i></a>
          </div><!-- /.panel-heading -->

          <div class="panel-body no-padding">
            <div class="panel panel-default shadow no-margin">
              <div class="panel-body">
                <div class="tab-content">
                  <div class="tab-pane fade in active" id="tab-setting-countries">
                    <table id="datatable-setting-countries" class="table table-default table-bordered">
                      <thead>
                        <tr>
                          <th data-class="expand" class="text-center" style="width: 90px;"> ID</th>
                          <th data-hide="phone">Name</th>
                          <th>Points</th>
                          <th style="width: 1px; min-width: 1px;">Total Attendees</th>
                          <th class="text-center" style="width: 200px; min-width: 200px;">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($sessions as $session)
                        <tr>
                          <td class="text-center">{{ $session->id }}</td>
                          <td>{{ $session->name }}</td>
                          <td>{{ $session->points }}</td>
                          <td>{{ App\SessionAttendee::where('session_id', $session->id)->count() }}</td>
                          <td class="text-center">

                            <!-- <a href="#" class="btn btn-primary" data-toggle="tooltip" data-placement="top" data-title="Scan for attendance"><i class="fa fa-qrcode"></i></a> -->
                            <a href="{{ route('session', $session->id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" data-title="View Session"><i class="fa fa-eye"></i></a>
                            <a href="{{ route('session-attendees', $session->id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" data-title="View Attendees"><i class="fa fa-user"></i></a>

                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                          <th data-class="expand" class="text-center" style="width: 90px;"> ID</th>
                          <th data-hide="phone">Name</th>
                          <th>Points</th>
                          <th style="width: 1px; min-width: 1px;">Total Attendees</th>
                          <th class="text-center" style="width: 200px; min-width: 200px;">Action</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div><!-- /.panel-body -->
            </div><!-- /.panel -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel -->
      </div>
    </div>
  </div>
  <footer class="footer-content">
    2014 - <span id="copyright-year"></span> &copy; Blankon Admin. Created by <a href="http://djavaui.com/" target="_blank">Djava UI</a>, Yogyakarta ID
    <span class="pull-right">0.01 GB(0%) of 15 GB used</span>
  </footer>
</section>
</section>

<!-- START @BACK TOP -->
<div id="back-top" class="animated pulse circle">
  <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Session attendance scanning for<i id="modal-header"></i> </h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="col-md-6">
            <div id="portrait" class="portrait">
              <div class="row top">
                <div class="col-md-2">
                  <img src="img/logo (2).png" width="60" alt="">
                </div>
                <div class="col-md-8 text-center">
                  <strong>  <h4 class="event-title">PSGH<i class="text-danger">AGM2019</i><sub>ACCRA</sub></h4></strong>
                </div>
                <div class="col-md-2">
                  <div id="qrcode"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <strong><h5 class="text-center">13th - 18th AUG</h5></strong>
                </div>
              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h2 class="text-center" id="first_name">DORA</h2>
                    <h2 class="text-center" id="middle_name">ERBYNN</h2>
                    <h2 class="text-center" id="last_name"></h2>
                    <h3 class="text-center" id="title">(MRS)</h3>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">2333</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">MEMBER</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">WESTERN</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h3 class="text-center">Community, Industry</h3>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">EDDYAMPS PHARMACY LTD.</h4>
                  </strong>
                </div>

              </div>

              <div class="row">

                <div class="col-md-12 pull-left">
                  <div class="col-md-2">
                    <div id="qrcode2"></div>
                  </div>
                  <div class="col-md-2 pull-right">
                    <div class="sponsor">
                      <strong>
                        <p>Denk
                          Pharma</p>
                        </strong>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <button type="button" id="btnPrint" name="button">Print</button>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-theme"> <i class="fa fa-print"></i> Print</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-filder-badge" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Scanning for Attendance </h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <form class="register-member-for-event-form" action="#" method="get">
              @csrf
              <input type="text" class="form-control" name="attendee_no" placeholder="Scan QR code" value="" required>
            </div>
            <br><br>
            <div class="modal-footer">
              <button type="button" tabindex="-1" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" tabindex="0" class="btn btn-theme"> <i class="fa fa-print"></i> Register</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
  <script src="../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/datatables.responsive.js"></script>
  <script src="../../../assets/admin/js/apps.js"></script>
  <script src="../../../assets/admin/js/pages/investor/blankon.investor.additional.option.system.metadata.js"></script>
  <script src="../../../assets/admin/js/demo.js"></script>
  <script type="text/javascript" src="js/jquery.qrcode.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('.btnClickFilter').on('click', function(){
      $('.modal-filder-badge').modal('show');
    });
    $('#datatable-setting-countries').on('click', '.btnPrintBadge', function(e){
      e.preventDefault();
      var $data = $(this).data('class');
      console.log($data);
      $.ajax(
        {
          url: 'view-member-badge/'+$data,
          type: 'get',
          success: function(response){
            if (response['data'] === 'success') {
              console.log(response['response']['Constituent_ID']);
              $('#modal-header').html(response['response']['First_Name']+ ' ' +response['response']['Middle_Name']+ ' ' +response['response']['Last_Name']);
              $('#mem-code').html(response['response']['Constituent_ID']);
              $('#first_name').html(response['response']['First_Name']);
              $('.bs-example-modal-lg').modal('show');
            }else {
              console.log('error');
              console.log(response);
            }
          }
        }
      )
    });
  });
</script>

<!-- START GOOGLE ANALYTICS -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55892530-1', 'auto');
ga('send', 'pageview');

</script>
<!--/ END GOOGLE ANALYTICS -->
<script type="text/javascript">
$(document).ready(function(e){
  $('.register-member-for-event-form').on('submit', function(e){
    e.preventDefault();
    $.ajax({
      url: 'register-member-for-event',
      type: 'POST',
      data: new FormData(this),
      cache: false,
      contentType: false,
      processData: false,
      success: function(response){
        console.log(response);
        if (response === '100') {
          alert('Badge scanned');
          location.reload();
        }else {
          alert(response);
        }
      }
    })
  });
});

</script>

</body>
<!--/ END BODY -->

</html>
