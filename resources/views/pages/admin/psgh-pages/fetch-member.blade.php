<table id="membership-table" class="table table-striped table-lilac">
  <thead>
    <tr>
      <th data-class="expand" class="text-center">Image</th>
      <th data-hide="phone">Title</th>
      <th data-hide="phone">Description</th>
      <th data-hide="phone,tablet" class="text-center">Contact</th>
      <th data-hide="phone,tablet">Email</th>
      <th data-hide="phone,tablet" style="min-width: 200px" class="text-center">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td class="text-center" style="width: 2px;"><img src="{{ asset('images/avatar1.png') }}" alt="..."  style="border-radius: 100px;" width="50" class="mt-5 mb-5"/></td>
      <td>{{ $member->Member_Name_Title}}</td>
      <td>
        <p>{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name}}</p>
        <p>Employer: {{ $member->Employer_Name}}</p>
        <p>Employer Address: {{ $member->Employer_Address_Line1 }}</p>
      </td>
      <td class="text-center">{{ $member->Mobile }}</td>
      <td>{{ $member->Email_Address }}</td>
      <td class="text-center">
        <a href="member-details/{{$member->Constituent_ID}}" class="btn btn-sm btn-success btn-xs btn-push"><i class="fa fa-eye"></i> Detail</a>
        <a href="edit-details/{{$member->Constituent_ID}}" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Edit</a>
        <!-- <a href="delete-details/{{$member->Constituent_ID" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Delete</a> -->
      </td>
    </tr>

  </tbody>
  <tfoot>
    <tr>
      <th data-class="expand" class="text-center">Image</th>
      <th data-hide="phone">Title</th>
      <th data-hide="phone">Name</th>
      <th data-hide="phone,tablet" class="text-center">Comments</th>
      <th data-hide="phone,tablet">Date</th>
      <th data-hide="phone,tablet" class="text-center">Action</th>
    </tr>
  </tfoot>
</table>
