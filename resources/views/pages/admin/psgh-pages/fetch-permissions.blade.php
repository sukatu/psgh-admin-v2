<h2>Permissions</h2>
<div class="panel-group" id="accordion">
  @foreach($groupPermissions as $groupName => $permissionGroup)
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $loop->index }}">{{ $groupName }}</a>
      </h4>
    </div>
    <div id="collapse{{ $loop->index }}" class="panel-collapse collapse {{ $loop->first?'in':'' }}">
      <div class="panel-body">
        @foreach($permissionGroup as $permission)
        <label for="" class="form-control">
          <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" {{ $selectedPermissions->contains($permission->id)?'checked':''}}> {{ $permission->display_name }}</label>
          @endforeach
        </div>
      </div>
    </div>
    @endforeach
  </div>

  <div class="row">
    <input type="submit" name="" value="Submit" class="form-control">
  </div>
