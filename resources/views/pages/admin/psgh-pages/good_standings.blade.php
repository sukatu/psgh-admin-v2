
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>PSGH | *</title>  <!--/ END META SECTION -->

  <!-- START @FAVICONS -->
  <link href="../../../img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="../../../img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="../../../img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="../../../img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
  <link href="../../../img/ico/html/apple-touch-icon.png" rel="shortcut icon">
  <!--/ END FAVICONS -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

  <!-- START @FONT STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
  <!--/ END FONT STYLES -->

  <!-- START @GLOBAL MANDATORY STYLES -->
  <link href="../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!--/ END GLOBAL MANDATORY STYLES -->

  <!-- START @PAGE LEVEL STYLES -->
  <link href="../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/datatables/css/datatables.responsive.css" rel="stylesheet">
  <!--/ END PAGE LEVEL STYLES -->

  <!-- START @THEME STYLES -->
  <link href="../../../assets/admin/css/reset.css" rel="stylesheet">
  <link href="../../../assets/admin/css/layout.css" rel="stylesheet">
  <link href="../../../assets/admin/css/components.css" rel="stylesheet">
  <link href="../../../assets/admin/css/plugins.css" rel="stylesheet">
  <link href="../../../assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
  <link href="../../../assets/admin/css/custom.css" rel="stylesheet">
  <style media="screen">
  @media all {

    .portrait{
      border: solid;
      border-color: black;
      border-width: thin;
      padding-left: 10px;
      padding-right: 25px!important;
      background-color: #ffff;
      width: 350px;
    }
    .event-title{
      font-size: 22px;
      text-align: center;
      margin-left: 13px;
    }
    #qrcode{
      margin-left: 10px;
    }
    #qrcode2{

    }
    .top{
      margin-top: 10px;
    }
    .sponsor{
      background: yellow;
      width: 70px;
      border: solid;
      border-color: black;
      padding-left: 10px;
      text-align: center;
      margin-left: -10px;
    }
  }
  </style>
</head>
<body class="page-sound page-header-fixed page-sidebar-fixed page-footer-fixed">

  <section id="wrapper">
    @include('layouts.header')
    @include('layouts.aside')

    <!-- START @PAGE CONTENT -->
    <section id="page-content">

      <!-- Start page header -->
      <div class="header-content">
        <h2>Default Members</h2>

      </div>
      @if(session('success'))
      <br>
      <div class="alert alert-success">
        <p>{{ session('success') }}</p>
      </div>
      @endif
      <div class="body-content animated fadeIn">

        <div class="row">
          <div class="col-md-12">

            <div class="panel panel-tab panel-tab-double shadow">
              <!-- Start tabs heading -->
              <div class="panel-heading no-padding">
                <ul class="nav nav-tabs">
                  <li class="active nav-border nav-border-top-success">
                    <a href="additional-option-system-manage-metadata.html#tab-setting-countries" data-toggle="tab">
                      <i class="icon-globe icons fg-success"></i>
                      <div>
                        <span class="text-strong">Members</span>
                        <span>List members default</span>
                      </div>
                    </a>
                  </li>

                </ul>
                <!-- <div class="col-md-8 pull-right">
                <label for="">Paid</label>  <input type="checkbox" name="" value="Paid">
                <input type="checkbox" name="" value="">
                <input type="checkbox" name="" value="">
                <input type="checkbox" name="" value="">
              </div> -->
              <a href="#" class="btn btn-success btn-add-setting-countries btnClickFilter pull-right">Filter Members &nbsp;<i class="fa fa-list"></i></a>
            </div><!-- /.panel-heading -->

            <div class="panel-body no-padding">
              <div class="panel panel-default shadow no-margin">
                <div class="panel-body">
                  <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-setting-countries">
                      <table id="example" class="table table-default display table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th data-class="expand" class="text-center" style="width: 90px;">Registration No.</th>
                            <th data-hide="phone">Name</th>
                            <th>Reg. Event</th>
                            <th>Payment Date</th>
                            <th>Amount</th>
                            <th class="text-center" style="width: 100px; min-width: 100px;">Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($members as $member)
                          <tr>
                            <td>{{ $member->Constituent_ID }}</td>
                            <td>{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name }}</td>
                            <?php $event_registrant = App\EventRegistrant::where('member_id', $member->id)->first(); ?>
                            <td>@if($event_registrant)<span class="text-success">{{ 'Registered' }}</span>@else<span class="text-danger">{{ 'Not Registered' }}</span>@endif</td>
                            <?php $paid_member = App\GoodStandingMember::where('registration_no', $member->Constituent_ID)->first(); ?>
                            <td>@if($paid_member != null){{ $paid_member->date }} @endif</td>
                            <td>@if($paid_member != null){{ $paid_member->amount }}@endif</td>
                            <td>@if($paid_member == null) <span class="text-danger"> {{ 'Not Paid' }} </span> @else<span class="text-success">{{ 'Good Standing' }}</span>@endif </td>
                            <td>@if($paid_member == null)  <a href="#" data-class="{{ $member->Constituent_ID }}" class="btn btn-success mkap">Mark as paid</a>@endif </td>
                          </tr>
                          @endforeach
                        </tbody>
                        <tfoot>
                          <tr>
                            <th data-class="expand" class="text-center" style="width: 90px;">Registration No.</th>
                            <th data-hide="phone">Name</th>
                            <th>Reg. Event</th>
                            <th>Payment Date</th>
                            <th>Amount</th>
                            <th class="text-center" style="width: 100px; min-width: 100px;">Status</th>
                            <th>Action</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div><!-- /.panel-body -->
              </div><!-- /.panel -->
            </div><!-- /.panel-body -->
          </div><!-- /.panel -->
        </div>
      </div>
    </div>
    <footer class="footer-content">
      2014 - <span id="copyright-year"></span> &copy; Blankon Admin. Created by <a href="http://djavaui.com/" target="_blank">Djava UI</a>, Yogyakarta ID
      <span class="pull-right">0.01 GB(0%) of 15 GB used</span>
    </footer>
  </section>
</section>

<!-- START @BACK TOP -->
<div id="back-top" class="animated pulse circle">
  <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Badge Reporting for <i id="modal-header"></i> </h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="col-md-6">
            <div id="portrait" class="portrait">
              <div class="row top">
                <div class="col-md-2">
                  <img src="img/logo (2).png" width="60" alt="">
                </div>
                <div class="col-md-8 text-center">
                  <strong>  <h4 class="event-title">PSGH<i class="text-danger">AGM2019</i><sub>ACCRA</sub></h4></strong>
                </div>
                <div class="col-md-2">
                  <div id="qrcode"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <strong><h5 class="text-center">13th - 18th AUG</h5></strong>
                </div>
              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h2 class="text-center" id="first_name">DORA</h2>
                    <h2 class="text-center" id="middle_name">ERBYNN</h2>
                    <h2 class="text-center" id="last_name"></h2>
                    <h3 class="text-center" id="title">(MRS)</h3>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">2333</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">MEMBER</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">WESTERN</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h3 class="text-center">Community, Industry</h3>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">EDDYAMPS PHARMACY LTD.</h4>
                  </strong>
                </div>

              </div>

              <div class="row">

                <div class="col-md-12 pull-left">
                  <div class="col-md-2">
                    <div id="qrcode2"></div>
                  </div>
                  <div class="col-md-2 pull-right">
                    <div class="sponsor">
                      <strong>
                        <p>Denk
                          Pharma</p>
                        </strong>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <button type="button" id="btnPrint" name="button">Print</button>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-theme"> <i class="fa fa-print"></i> Print</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal mkapp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        <style>
        .switch {
          position: relative;
          display: inline-block;
          width: 60px;
          height: 34px;
        }

        .switch input {
          opacity: 0;
          width: 0;
          height: 0;
        }

        .slider {
          position: absolute;
          cursor: pointer;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: #ccc;
          -webkit-transition: .4s;
          transition: .4s;
        }

        .slider:before {
          position: absolute;
          content: "";
          height: 26px;
          width: 26px;
          left: 4px;
          bottom: 4px;
          background-color: white;
          -webkit-transition: .4s;
          transition: .4s;
        }

        input:checked + .slider {
          background-color: #2196F3;
        }

        input:focus + .slider {
          box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
          -webkit-transform: translateX(26px);
          -ms-transform: translateX(26px);
          transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
          border-radius: 34px;
        }

        .slider.round:before {
          border-radius: 50%;
        }
        </style>

        <div class="modal-body">
          <div class="col-md-6 text-center" style="margin-left: 130px;">
            <form class="" action="register-as-paid" method="post">
              @csrf
              <input type="hidden" name="constituent_id" id="constituent_id" value="">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <select class="form-control" name="amount">
                      <option value="850">GH850</option>
                      <option value="750">GH750</option>
                      <option value="175">GH175</option>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <label class="switch text-center">
                      <input type="checkbox" name="checkbox" checked value="">
                      <span class="slider"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <br><br>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-theme"> <i class="fa fa-arrow-right"></i> Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade modal-filder-badge" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Filter Members</h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <form class="" action="filter-members" method="get">
              <select class="form-control" name="value" required>
                <option value="">Select</option>
                <option value="1">Paid</option>
                <option value="2">Not Paid</option>
                <option value="3">Registered & Paid</option>
                <option value="4">Registered Not Paid</option>
                <option value="5">Not Registered but Paid</option>
                <option value="6">Not Registered & Not Paid</option>
              </select>
            </div>
            <br><br>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-theme"> <i class="fa fa-arrow-right"></i> Go</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
  <script src="../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>

  <script src="../../../assets/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/datatables.responsive.js"></script>
  <script src="../../../assets/admin/js/apps.js"></script>
  <script src="../../../assets/admin/js/pages/investor/blankon.investor.additional.option.system.metadata.js"></script>
  <script src="../../../assets/admin/js/demo.js"></script>
  <script type="text/javascript" src="js/jquery.qrcode.min.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" charset="utf-8"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" charset="utf-8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" charset="utf-8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" charset="utf-8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" charset="utf-8"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" charset="utf-8"></script>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
      dom: 'Bfrtip',
      buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
      ]
    } );
  } );
  $(document).ready(function(){
    $('.btnClickFilter').on('click', function(){
      $('.modal-filder-badge').modal('show');
    });
    $('#datatable-setting-countries').on('click', '.btnPrintBadge', function(e){
      e.preventDefault();
      var $data = $(this).data('class');
      console.log($data);
      $.ajax(
        {
          url: 'view-member-badge/'+$data,
          type: 'get',
          success: function(response){
            if (response['data'] === 'success') {
              console.log(response['response']['Constituent_ID']);
              $('#modal-header').html(response['response']['First_Name']+ ' ' +response['response']['Middle_Name']+ ' ' +response['response']['Last_Name']);
              $('#mem-code').html(response['response']['Constituent_ID']);
              $('#first_name').html(response['response']['First_Name']);
              $('.bs-example-modal-lg').modal('show');
            }else {
              console.log('error');
              console.log(response);
            }
          }
        }
      )
    });
  });
</script>

<!-- START GOOGLE ANALYTICS -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55892530-1', 'auto');
ga('send', 'pageview');

</script>
<!--/ END GOOGLE ANALYTICS -->
<script type="text/javascript">
$(document).ready(function(e){
  $('#qrcode').qrcode({width: 45,height: 45,text: "91000"});
  $('#qrcode2').qrcode({width: 65,height: 65,text: "2333, Edward, Amponsah, Mr, Member, Western, Community, Industry, EDDYAMPS PHARMACY LTD, 024444444"});
});

</script>
<script type="text/javascript">
$('#btnPrint').on('click', function(e){
  print($('.portrait'));
});
</script>
<script type="text/javascript">
$(document).ready(function(e){
  $('#example').on('click', '.mkap', function(){
    $id = $(this).data('class');
    $('.mkapp').modal('show');
    $('#constituent_id').val($id);
  });
});
</script>
</body>
<!--/ END BODY -->

</html>
