@extends('layouts.tables')
@section('content')
<div class="header-content">
  <h2><i class="fa fa-pencil"></i> Compose <span>mail compose sample</span></h2>
  <div class="breadcrumb-wrapper hidden-xs">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{ url('/home') }}">Dashboard</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{ url()->previous() }}">Mail</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li class="active">Compose</li>
    </ol>
  </div>
</div>
<div class="body-content animated fadeIn">
  <div class="row compose-mail-wrapper">
    <div class="col-sm-3">
      <ul class="nav nav-pills nav-stacked nav-email mb-20 rounded shadow">
        <!-- <li class="">
        <a href="mail-inbox.html">
        <i class="fa fa-inbox"></i> Inbox  <span class="label pull-right">7</span>
      </a>
    </li> -->
    <li class="active">
      <a href="{{ route('mail') }}"><i class="fa fa-envelope-o"></i> Send Mail
      </a>
    </li>
    <!-- <li>
    <a href="{{ route('draft') }}">
    <i class="fa fa-file-text-o"></i> Drafts <span class="label label-info pull-right inbox-notification">35</span>
  </a>
</li>
<li>
<a href="{{ route('draft') }}">
<i class="fa fa-paper-plane"></i> Drafts <span class="label label-info pull-right inbox-notification">35</span>
</a>
</li>
<li><a href="mail-compose.html#"> <i class="fa fa-trash-o"></i> Sent</a></li> -->
</ul>
<!-- <h5 class="nav-email-subtitle">More</h5> -->
<!-- <ul class="nav nav-pills nav-stacked nav-email mb-20 rounded shadow">
<li>
<a href="#">
<i class="fa fa-folder-open"></i> Job list
</a>
</li>
<li>
<a href="#">
<i class="fa fa-folder-open"></i> Backup
</a>
</li>
</ul> -->
</div>
<div class="col-sm-9">
  <form class="form-horizontal" action="{{ route('bulkMail') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="panel rounded shadow panel-danger">
      <div class="panel-heading">
        <div class="pull-left">
          <h3 class="panel-title">View message</h3>
        </div>
        <div class="pull-right">
          <form action="#" class="form-horizontal mr-5 mt-3">
            <div class="form-group no-margin no-padding has-feedback search-mail">
              <input type="text" class="form-control no-border" placeholder="Search mail">
              <button type="submit" class="btn btn-theme fa fa-search form-control-feedback"></button>
            </div>
          </form>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-sub-heading inner-all">
        <div class="pull-left">
          <ul class="list-inline no-margin">
            <li>
              <a href="{{ url()->previous() }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
            </li>
            <li class="hidden-xs">
              <a href="mail-compose.html#" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save Draft</a>
            </li>
          </ul>
        </div>
        <div class="pull-right">
          <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> Send Email</button>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <label for="to" class="col-sm-2 control-label">To:</label>
          <div class="col-sm-10">
            <div class="input-group">
              <select class="form-control @error('recipient') is-invalid @enderror" id="recipient" name="recipient" >
                <option value="{{ old('recipient') }}">{{ old('recipient') }}</option>
                <option value="All Members">All Members</option>
                <option value="Profile">Profile</option>
                <option value="Members who didn't register for event">Members who didn't register for event</option>
                <option value="Members who registered for the event">Members who registered for the event</option>
                <option value="Individual" >Individual</option>
              </select>
              <span class="invalid-feedback text-danger recipient-error" style="display: none;" role="alert">
                <strong>{{ __('This field is required') }}</strong>
              </span>
            </div>
          </div>
        </div>
        <div id="ccbcc" class="collapse">
          <div class="form-group">
            <label for="Cc" class="col-sm-2 control-label">Profile Category:</label>
            <div class="col-sm-10">
              <select class="form-control" id="profile" name="profile">
                <option value=""></option>
                @foreach($profiles as $profile)
                <option value="{{ $profile->id }}">{{ $profile->display_name }}</option>
                @endforeach
              </select>
              <span class="invalid-feedback text-danger profile-category-error" style="display: none;" role="alert">
                <strong>{{ __('This field is required') }}</strong>
              </span>
            </div>
          </div>
          <div class="form-group profile-categories-section">

          </div>
        </div>
        <div class="form-group" style="display: none;" id="email-member">
          <label for="Cc" class="col-sm-2 control-label">Member Email:</label>
          <div class="col-sm-10">
            <select id="mem_email" name="mem_email" class="chosen-select" style="display: none;">
              @foreach($members as $member)
              <option value=""></option>
              <option value="{{ $member->Email_Address }}">{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name.' ('.$member->Constituent_ID.')' }} @if($member->Email_Address == '') {{ $member->Email_Address_Alternate }}@else {{ $member->Email_Address }}@endif</option>
              @endforeach
            </select>
            <span class="invalid-feedback text-danger member-email-error" style="display: none;" role="alert">
              <strong>{{ __('This field is required') }}</strong>
            </span>
          </div>
        </div>
        <div class="form-group">
          <div class="form-group">
            <label for="subject" class="col-sm-2 control-label">Subject:</label>
            <div class="col-sm-10">
              <input type="text" value="{{ old('subject') }}" name="subject" id="subject" class="form-control input-sm">
              <span class="invalid-feedback text-danger subject-error" style="display: none;" role="alert">
                <strong>{{ __('This field is required') }}</strong>
              </span>
            </div>

          </div>
          <div class="form-group">
            <label for="subject" class="col-sm-2 control-label">Macro:</label>
            <div class="col-sm-10">
              <button type="button" title="Insert macro" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                {}
              </button>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <textarea id="summernote" name="body" ></textarea>
              <span class="invalid-feedback text-danger body-error" style="display: none;" role="alert">
                <strong>{{ __('This field is required') }}</strong>
              </span>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="file" class="col-sm-2 control-label">Attach file:</label>
          <div class="col-sm-10">
            <div class="fileupload fileupload-new" data-provides="fileupload">
              <div class="input-append">
                <div class="uneditable-input">
                  <i class="glyphicon glyphicon-file fileupload-exists"></i>
                  <span class="fileupload-preview"></span>
                </div>
                <span class="btn btn-theme btn-sm btn-success btn-file">
                  <span class="fileupload-new">Select file</span>
                  <span class="fileupload-exists">Change</span>
                  <input type="file" name="file_url" id="file" />
                </span>
                <a href="mail-compose.html#" class="btn btn-danger btn-sm fileupload-exists" data-dismiss="fileupload">Remove</a>
              </div>
            </div>
          </div>
        </div><!-- /.form-group -->
      </div>
      <div class="panel-footer">
        <div class="pull-right">
          <button class="btn btn-danger btn-sm">Cancel</button>
          <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> Send Email</button>
        </div>
        <div class="clearfix"></div>
      </div>
      @if(session('success'))
      <div class="alert alert-success">
        {{ session('success') }}
      </div>
      @elseif(session('error'))
      <div class="alert alert-danger">
        {{ session('error') }}
      </div>
      @endif
    </div>
  </form>
</div>
</div>
</div>
@endsection
