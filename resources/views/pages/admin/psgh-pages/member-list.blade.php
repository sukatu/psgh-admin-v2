@extends('layouts.tables')
@section('content')
<div class="body-content animated fadeIn">
  <div class="row">
    <div class="col-md-12">
      <div class="panel rounded shadow">
        <div class="panel-heading">
          <div class="pull-left">
            <h3 class="panel-title">Members List</h3>
          </div>
          <div class="pull-right">
            <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
            <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
          <!-- <div class="col-sm-3 pull-left">
          <a href="{{ route('export-view') }}"><button type="button" class="btn btn-primary" name="button">Export View</button></a>
          </div> -->
          <div class="col-sm-7  pull-right">
            <select id="members-title" class="chosen-select" style="display: none;">
              @foreach($members_title as $title)
              <option value=""></option>
              <option value="{{ $title->Constituent_ID }}">{{ $title->title.' '.$title->First_Name.' '.$title->Middle_Name.' '.$title->Last_Name }} <div class="pull-right">
                ({{ $title->Constituent_ID }})
              </div> </option>
              @endforeach
            </select>
           </div>
           <div class="replace-content">
            <table id="membership-table" class="table table-striped table-lilac">
              <thead>
                <tr>
                  <th data-class="expand" class="text-center">Image</th>
                  <th data-hide="phone">Title</th>
                  <th data-hide="phone">Description</th>
                  <th data-hide="phone,tablet" class="text-center">Contact</th>
                  <th data-hide="phone,tablet">Email</th>
                  <th data-hide="phone,tablet" style="min-width: 200px" class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($members as $member)
                <tr>
                  <td class="text-center" style="width: 2px;"><img src="{{ asset('images/avatar1.png') }}" alt="..."  style="border-radius: 100px;" width="50" class="mt-5 mb-5"/></td>
                  <td>{{ $member->Member_Name_Title}}</td>
                  <td>
                    <p>{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name}}</p>
                    <p>Employer: {{ $member->Employer_Name}}</p>
                    <p>Employer Address: {{ $member->Employer_Address_Line1 }}</p>
                  </td>
                  <td class="text-center">{{ $member->Mobile }}</td>
                  <td>{{ $member->Email_Address }}</td>
                  <td class="text-center">
                    <a href="member-details/{{$member->Constituent_ID}}" class="btn btn-sm btn-success btn-xs btn-push"><i class="fa fa-eye"></i> Detail</a>
                    <a href="edit-details/{{$member->Constituent_ID}}" class="btn btn-sm btn-primary btn-xs btn-push"><i class="fa fa-pencil"></i> Edit</a>
                    <!-- <a href="delete-details/{{$member->Constituent_ID" class="btn btn-sm btn-danger btn-xs btn-push"><i class="fa fa-trash"></i> Delete</a> -->
                  </td>
                </tr>

                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th data-class="expand" class="text-center">Image</th>
                  <th data-hide="phone">Title</th>
                  <th data-hide="phone">Name</th>
                  <th data-hide="phone,tablet" class="text-center">Comments</th>
                  <th data-hide="phone,tablet">Date</th>
                  <th data-hide="phone,tablet" class="text-center">Action</th>
                </tr>
              </tfoot>
            </table>
            </div>
            <div class="text-center">
              <?php echo $members->links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
