@extends('layouts.tables')
@section('content')
      <div class="body-content animated fadeIn">

        @if(session('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div>
        @elseif(session('error'))
        <div class="alert alert-danger">
          {{ session('error') }}
        </div>
        @endif
        <div class="row">
          <div class="col-md-12">
            <div class="panel rounded shadow">
              <div class="panel-heading">
                <div class="pull-left">
                  <h3 class="panel-title">Basic Information</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                  <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                  <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
              </div>
              <div class="panel-body no-padding">
                <form class="form-horizontal form-bordered" method="POST" action="{{ route('validateRegistrationForm') }}" role="form">
                  @if(count($errors))
                  <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.
                    <br/>
                    <ul>
                      @foreach($errors->all() as $error)
                      <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                  </div>
                  @endif
                  @csrf
                  <input type=hidden name="submission_type" value="update">
                    <div class="form-body">
                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Title <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select class="form-control" name="title">
                          <option  value="{{ old('title') }}">{{ old('title') }}</option>
                          <option value="{{ $member->Member_Name_Title }}" selected>{{ $member->Member_Name_Title }}</option>
                          <option value="MR.">MR.</option>
                          <option value="MS.">MS.</option>
                          <option value="MRS.">MRS.</option>
                          <option value="MISS">MISS</option>
                          <option value="REV.">REV.</option>
                          <option value="DOC.">DOC.</option>
                          <option value="PROF.">PROF.</option>
                          <option value="CAPT">CAPT.</option>
                          <option value="ALHJ">ALHJ.</option>
                          <option value="ACP.">ACP.</option>
                          <option value="APST.">APST.</option>
                          <option value="COL.">COL.</option>
                          <option value="MAJ.">MAJ.</option>
                          <option value="SUPT.">SUPT.</option>
                          <option value="PS.">PS.</option>
                          <option value="REV. SIS">REV. SIS.</option>
                          <option value="LT. COL.">LT. COL.</option>
                          <option value="REV. DOC.">REV. DOC.</option>
                          <option value="REV. PROF.">REV. PROF.</option>
                          <option value="CAPT. RTD.">CAPT. RTD.</option>
                          <option value="DR.(MRS)">DR.(MRS.)</option>
                          <option value="PROF. DOC.">PROF. DOC.</option>
                        </select>
                      </div>
                      <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">First Name <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" value="{{ $member->First_Name }}" name="first_name" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('first_name') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('middle_name') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Middle Name</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm"  name="middle_name" value="{{ $member->Middle_Name }}" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('middle_name') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Last Name <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" value="{{ $member->Last_Name }}" name="last_name" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('last_name') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Date of Birth</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" value="{{ str_replace("00:00:00", "",$member->Birthdate) }}" name="dob"  placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('dob') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Gender<span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select class="form-control" name="gender" value="">
                          <option value="{{ $member->Gender }}">{{ $member->Gender }}</option>
                          <option value="Female">Female</option>
                          <option value="Male">Male</option>
                        </select>
                        <label for="sv_skill_programming" class="error"></label>
                        <input type="text" class="hide" id="sv_skill_programming"/>
                      </div>
                      <span class="text-danger">{{ $errors->first('gender') }}</span>
                    </div>
                    <div class="form-group {{ $errors->has('marital_status') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Marital Status<span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select class="form-control"  name="marital_status">
                          <option value="{{ old('marital_status') }}">{{ old('marital_status') }}</option>
                          <option value="{{ $member->Marriage_Status }}" selected>{{ $member->Marriage_Status }}</option>
                          <option value="Married">Married</option>
                          <option value="Single">Single</option>
                          <option value="Devorced">Devorced</option>
                          <option value="Widow">Widow</option>
                        </select>
                        <label for="sv_skill_programming" class="error"></label>
                        <input type="text" class="hide" id="sv_skill_programming"/>
                      </div>
                      <span class="text-danger">{{ $errors->first('marital_status') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('maiden_name') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Maiden Name</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" value="{{ $member->Maiden_Name }}" name="maiden_name" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('maiden_name') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('spouse_name') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Spouse Name</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" value="{{ $member->Spouse_Name }}" name="spouse_name" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('spouse_name') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('email_address') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Email Address</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" value="{{ $member->Email_Address }}" name="email_address" placeholder="">
                      </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Alt. Email Address</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" name="alt_email" value="{{ $member->Email_Address_Alternate }}" placeholder="">
                      </div>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Mobile</label>
                      <div class="col-sm-1">
                        <input type="text" name="mobile_area_code" class="form-control" value="{{ $member->Mobile_Area_Code }}">
                      </div>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Mobile }}" class="form-control input-sm" name="mobile" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('mobile') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Alt. Mobile</label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control input-sm" value="{{ $member->Home_Phone }}" name="alt_mobile" placeholder="">
                      </div>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('home_address') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Home Address</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Home_Address_Line1 }}"  class="form-control input-sm" name="home_address" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('home_address') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('suburb') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Suburb</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Home_Address_Line2 }}" class="form-control input-sm" name="suburb" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('suburb') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">City</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Home_City }}" class="form-control input-sm" name="city" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('city') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Country</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ old('country') }}" class="form-control input-sm" name="country" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('country') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('place_of_birth') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Place of Birth</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Place_of_Birth }}" class="form-control input-sm" name="place_of_birth" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('place_of_birth') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('nationality') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Nationality</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Nationality }}" class="form-control input-sm" name="nationality" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('nationality') }}</span>
                    </div><!-- /.form-group -->
                    <div class="form-group {{ $errors->has('gh_post_code') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">GH Post Code</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Home_Postal_Code }}" class="form-control input-sm" name="gh_post_code" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('gh_post_code') }}</span>
                    </div><!-- /.form-group -->
                  </div>
                </div><!-- /.panel-body -->
              </div><!-- /.panel -->
              <!--/ End basic validation -->

            </div>
          </div><!-- /.row -->
          <div class="row">
            <div class="col-md-12">

              <!-- Start number validation -->
              <div class="panel rounded shadow">
                <div class="panel-heading">
                  <div class="pull-left">
                    <h3 class="panel-title">Work Information</h3>
                  </div>
                  <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                  <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="panel-body no-padding">

                  <div class="form-body">
                    <div class="form-group {{ $errors->has('employer_name') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Company</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Employer_Name }}" class="form-control input-sm" name="employer_name" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('employer_name') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('employer_address') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Work Address</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Employer_Address_Line1 }}" class="form-control input-sm" name="employer_address" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('employer_address') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('employer_contact') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Work Contact</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Employer_Address_Line2 }}" class="form-control input-sm" name="employer_contact" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('employer_contact') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('employer_suburb') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Work Suburb</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Employer_City }}" class="form-control input-sm" name="employer_suburb" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('employer_suburb') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('employer_city') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Work City</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Employer_City }}" class="form-control input-sm" name="employer_city" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('employer_city') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('employer_location') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Work Location</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{$member->Employer_Location }}" class="form-control input-sm" name="employer_location" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('employer_location') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('employer_country') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Work Country</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Employer_Country }}" class="form-control input-sm" name="employer_country" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('employer_country') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('employer_gh_post_code') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Work Gh. Post Code</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Employer_Postal_Code }}" class="form-control input-sm" name="employer_gh_post_code" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('employer_gh_post_code') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('professional_title') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Position</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Professional_Title }}" class="form-control input-sm" name="professional_title" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('professional_title') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('profession') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Employment Status</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Profession }}" class="form-control input-sm" name="profession" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('profession') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('qualification') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Qualification</label>
                      <div class="col-sm-7">
                        <input type="text" value="{{ $member->Qualification }}" class="form-control input-sm" name="qualification" placeholder="">
                      </div>
                      <span class="text-danger">{{ $errors->first('qualification') }}</span>
                    </div>
                    <br><br>
                    <div class="form-group {{ $errors->has('area_of_pharmacy_practice') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Area of Pharmacy Practice <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select  class="form-control" value="{{ $member->Area_of_Pharmacy_Practice }}" name="area_of_pharmacy_practice">
                          <option value="{{ $member->Area_of_Pharmacy_Practice }}" selected>{{ $member->Area_of_Pharmacy_Practice }}</option>
                          <option value="Academia, Regulatory & Social">Academia, Regulatory & Social</option>
                          <option value="Academia, Regulatory & Social|Community">Academia, Regulatory & Social|Community</option>
                          <option value="Academia, Regulatory & Social|Community|Industry">Academia, Regulatory & Social|Community|Industry</option>
                          <option value="Academia, Regulatory & Social|Medical Representation & Marketing">Academia, Regulatory & Social|Medical Representation & Marketing</option>
                          <option value="Academic">Academic</option>
                          <option value="Academic|Community">Academic|Community</option>
                          <option value="Academic|Community|Clinical">Academic|Community|Clinical</option>
                          <option value="Academic|Community|Hospital|Clinical">Academic|Community|Hospital|Clinical</option>
                          <option value="Academic|Community|Industrial|Informatics|Internet|Medical Representation">Academic|Community|Industrial|Informatics|Internet|Medical Representation</option>
                          <option value="">Academic|Consulting|Community|Hospital|Clinical|Administrative|Emergency|Other</option>
                          <option value="Academic|Community">Academic|Community</option>
                          <option value="Academic|Community|Clinical">Academic|Community|Clinical</option>
                          <option value="Academic|Community|Hospital|Clinical">Academic|Community|Hospital|Clinical</option>
                          <option value="Academic|Consulting">Academic|Consulting</option>
                          <option value="Academic|Consulting|Regulatory|Clinical|Industrial">Academic|Consulting|Regulatory|Clinical|Industrial</option>
                          <option value="Academic|Hospital">Academic|Hospital</option>
                          <option value="Academic|Hospital|Clinical">Academic|Hospital|Clinical</option>
                          <option value="Community">Community</option>
                        </select>
                        <label for="sv_position" class="error"></label>
                        <input  type="text" class="hide" id="sv_position"/>
                      </div>
                      <span class="text-danger">{{ $errors->first('area_of_pharmacy_practice') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('practice_group_association') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Practice Group Association <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select  class="form-control" value="{{ old('practice_group_association') }}" name="practice_group_association" multiple="multiple">
                          <option value="{{ $member->Practice_Group_Association }}">{{ $member->Practice_Group_Association }}</option>
                          <option value="AREPI">AREPI</option>
                          <option value="CPPA">CPPA</option>
                          <option value="CPPA|AREPI">CPPA|AREPI</option>
                          <option value="CPPA|GHOSPA">CPPA|GHOSPA</option>
                          <option value="CPPA|GHOSPA|AREPI">CPPA|GHOSPA|AREPI</option>
                          <option value="CPPA|GHOSPA|IPA">CPPA|GHOSPA|IPA</option>
                          <option value="CPPA|IPA">CPPA|IPA</option>
                          <option value="CPPA|IPA|AREPI">CPPA|IPA|AREPI</option>
                          <option value="CPPA|RELPA|AREPI">CPPA|RELPA|AREPI</option>
                          <option value="CPPA|RELPA|GHOSPA">CPPA|RELPA|GHOSPA</option>
                          <option value="CPPA|RELPA|GHOSPA|IPA|AREPI">CPPA|RELPA|GHOSPA|IPA|AREPI</option>
                          <option value="CPPA|RELPA|IPA">CPPA|RELPA|IPA</option>
                          <option value="GHOSPA">GHOSPA</option>
                          <option value="GHOSPA|AREPI">GHOSPA|AREPI</option>
                          <option value="GHOSPA|IPA">GHOSPA|IPA</option>
                          <option value="IPA">IPA</option>
                          <option value="RELPA">RELPA</option>
                          <option value="RELPA|IPA">RELPA|IPA</option>
                          <option value="SARPA">SARPA</option>
                          <option value="SARPA|CPPA">SARPA|CPPA</option>
                          <option value="SARPA|CPPA|GHOSPA">SARPA|CPPA|GHOSPA</option>
                          <option value="SARPA|CPPA|IPA|AREPI">SARPA|CPPA|IPA|AREPI</option>
                          <option value="SARPA|CPPA|RELPA|GHOSPA">SARPA|CPPA|RELPA|GHOSPA</option>
                          <option value="SARPA|CPPA|RELPA|GHOSPA|AREPI">SARPA|CPPA|RELPA|GHOSPA|AREPI</option>
                          <option value="SARPA|CPPA|RELPA|GHOSPA|IPA|AREPI">SARPA|CPPA|RELPA|GHOSPA|IPA|AREPI</option>
                          <option value="SARPA|GHOSPA">SARPA|GHOSPA</option>
                          <option value="SARPA|GHOSPA|IPA">SARPA|GHOSPA|IPA</option>
                          <option value="SARPA|IPA">SARPA|IPA</option>
                          <option value="SARPA|RELPA">SARPA|RELPA</option>
                        </select>
                        <label for="sv_position" class="error"></label>
                        <input  type="text" class="hide" id="sv_position"/>
                      </div>
                      <span class="text-danger">{{ $errors->first('practice_group_association') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('interest_group_association') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Interest Group Association <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select  class="form-control" value="" name="interest_group_association" multiple="multiple">
                          <option value="{{ $member->Interest_Group_Association }}">{{ $member->Interest_Group_Association }}</option>
                          <option value="CPPA|IPA">CPPA|IPA</option>
                          <option value="LAPAG">LAPAG</option>
                          <option value="LAPAG|Other">LAPAG|Other</option>
                          <option value="LAPAG|YPG">LAPAG|YPG</option>
                          <option value="LAPAG|YPG|Other">LAPAG|YPG|Other</option>
                          <option value="YPG">YPG</option>
                          <option value="YPG|Other">YPG|Other</option>
                          <option value="Other">Other</option>
                        </select>
                        <label for="sv_position" class="error"></label>
                        <input  type="text" class="hide" id="sv_position"/>
                      </div>
                      <span class="text-danger">{{ $errors->first('interest_group_association') }}</span>
                    </div><!-- /.form-group -->
                    <br><br>
                    <div class="form-group {{ $errors->has('university_attended') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Pharmacy School or University Attended <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select  class="form-control" value="{{ old('university_attended') }}" name="university_attended">
                          <option value="{{ $member->Pharmacy_School_or_University_Attended_with_Years }}">{{ $member->Pharmacy_School_or_University_Attended_with_Years }}</option>
                          <option value="PYATIGORSK PHARMACEUTICAL INSTITUTE
                          USSR(RUSSIA)
                          1987-1992">PYATIGORSK PHARMACEUTICAL INSTITUTE
                          USSR(RUSSIA)
                          1987-1992</option>
                          <option value="PYATIGOPOK STATE PHARM ACADEMY">PYATIGOPOK STATE PHARM ACADEMY</option>
                          <option value="NATIONAL UNIVERSITY OF PHARMACY, UKRAINE
                          5YEARS">NATIONAL UNIVERSITY OF PHARMACY, UKRAINE
                          5YEARS</option>
                          <option value="NATIONAL PHARMACEUTICAL UNIVERSITY">NATIONAL PHARMACEUTICAL UNIVERSITY</option>
                          <option value="MORGAN STATE UNIVERSITY">MORGAN STATE UNIVERSITY</option>
                          <option value="KWAME UNIVERSITY OF SCIENCE AND TECHNOLOGY">KWAME UNIVERSITY OF SCIENCE AND TECHNOLOGY</option>
                        </select>
                        <label for="sv_position" class="error"></label>
                        <input  type="text" class="hide" id="sv_position"/>
                      </div>
                      <span class="text-danger">{{ $errors->first('university_attended') }}</span>
                    </div>
                    <br><br>
                    <div class="form-group wrapper">
                      <label class="col-sm-3 control-label">School Attended with Years <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <input type="text" class="form-control" name="school_attended_with_years[]" placeholder="" value="{{ $member->Pharmacy_School_or_University_Attended_with_Years }}">
                        <label for="sv_position" class="error"></label>
                        <input type="text" class="hide" id="sv_position"/>
                      </div>
                    </div>
                    <button class="btn btn-primary add_fields pull-right"> <i class="fa fa-plus"></i>  Add More Fields</button>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row">
            <div class="col-md-12">
              <div class="panel rounded shadow">
                <div class="panel-heading">
                  <div class="pull-left">
                    <h3 class="panel-title">Account Information</h3>
                  </div>
                  <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                  <div class="clearfix"></div>
                </div><!-- /.panel-heading -->

              </div>
            </div>
          </div><!-- /.row -->

          <div class="row">
            <div class="col-md-12">

              <div class="panel rounded shadow">
                <div class="panel-heading">
                  <div class="pull-left">
                    <h3 class="panel-title">Membership Information</h3>
                  </div>
                  <div class="pull-right">
                    <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                    <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                  </div>
                  <div class="clearfix"></div>
                </div>
                <div class="panel-body no-padding">

                  <div class="form-body">
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Registration No.</label>
                      <div class="col-sm-7">
                        <input type="text"  value="{{ $member->Constituent_ID }}" class="form-control input-sm" name="Constituent_ID">
                      </div>
                    </div>
                    <br><br>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Dues Type <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select  class="form-control" name="membership">
                          <option value="{{ $member->Membership }}" selected>{{ $member->Membership }}</option>
                          <option value="Elderly Dues">Elderly Dues</option>
                          <option value="Fellow Dues">Fellow Dues</option>
                          <option value="Member Dues">Member Dues</option>
                          <option value="Newly Qualified Dues">Newly Qualified Dues</option>
                          <option value="Retention Exempted Dues">Retention Exempted Dues</option>
                            <option value="Fully Exempted Dues">Fully Exempted Dues</option>
                        </select>
                        <label for="sv_position" class="error"></label>
                        <input  type="text" class="hide" id="sv_position"/>
                      </div>
                    </div>
                    <br><br>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Member Type <span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select  class="form-control" name="member_type_code">
                          <option value="{{ $member->Member_Type_Code }}" selected>{{ $member->Member_Type_Code }}</option>
                          <option value="Fellow">Fellow</option>
                          <option value="Member">Member</option>
                          <option value="New Member">New Member</option>
                        </select>
                        <label for="sv_position" class="error"></label>
                        <input  type="text" class="hide" id="sv_position"/>
                      </div>
                    </div>
                    <br><br>
                    <div class="form-group {{ $errors->has('regional_branch_chapter') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Regional Branch Chapter<span class="asterisk">*</span></label>
                      <div class="col-sm-7">
                        <select class="form-control" value="{{ old('regional_branch_chapter') }}" name="regional_branch_chapter">
                          <option value="{{ $member->Regional_Branch_Chapter }}" selected>{{ $member->Regional_Branch_Chapter }}</option>
                          <option value="Greater Accra">Greater Accra</option>
                          <option value="Ashanti">Ashanti</option>
                          <option value="Western South">Western</option>
                          <option value="Western North">Western North</option>
                          <option value="Eastern">Eastern</option>
                          <option value="Central">Central</option>
                          <option value="Volta">Volta</option>
                          <option value="Oti">Oti</option>
                          <option value="Brong East">Bono East</option>
                          <option value="Brong West">Bono</option>
                          <option value="Ahafo">Ahafo</option>
                          <option value="Nothern">Nothern</option>
                          <option value="Savannah">Savannah</option>
                          <option value="North East">North East</option>
                          <option value="Upper East">Upper East</option>
                          <option value="Upper West">Upper West</option>
                          <option value="Abroad">Abroad</option>
                        </select>
                        <label for="sv_skill_programming" class="error"></label>
                        <input type="text" class="hide" id="sv_skill_programming"/>
                      </div>
                      <span class="text-danger">{{ $errors->first('regional_branch_chapter') }}</span>
                    </div>
                    <br><br>
                    <div class="form-group {{ $errors->has('registration_date') ? 'has-error' : '' }}">
                      <label class="col-sm-3 control-label">Registration Date</label>
                      <div class="col-sm-7">
                        <input type="text"  value="{{ $member->Registration_Date }}" class="form-control input-sm" name="registration_date">
                      </div>
                    </div>
                    <span class="text-danger">{{ $errors->first('registration_date') }}</span>
                  </div>
                  <br><br>
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Member Status <span class="asterisk">*</span></label>
                    <div class="col-sm-7">
                      <select class="form-control" name="status">
                        <option value="{{ $member->status }}" selected>{{ $member->status }}</option>
                        <option value="Active">Active</option>
                        <option value="Alive-Inactive">Alive-Inactive</option>
                        <option value="Alive-Active">Alive-Active</option>
                        <option value="Alive-Incapacitated">Alive-Incapacitated</option>
                        <option value="Deceased">Deceased</option>
                      </select>
                      <label for="sv_position" class="error"></label>
                      <input  type="text" class="hide" id="sv_position"/>
                    </div>
                  </div>
                  <br><br>
                  <br><br>
                  <div class="form-footer">
                    <div class="col-sm-offset-3">
                      <button type="submit" class="btn btn-theme">Update</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55892530-1', 'auto');
  ga('send', 'pageview');

</script>

@endsection
