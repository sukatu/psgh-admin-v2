<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>PSGH | Admin</title>  <link href="../../../img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="../../../img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="../../../img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="../../../img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
  <link href="../../../img/ico/html/apple-touch-icon.png" rel="shortcut icon">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../../assets/admin/css/reset.css" rel="stylesheet">
  <link href="../../../assets/admin/css/layout.css" rel="stylesheet">
  <link href="../../../assets/admin/css/components.css" rel="stylesheet">
  <link href="../../../assets/admin/css/plugins.css" rel="stylesheet">
  <link href="../../../assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
  <link href="../../../assets/admin/css/custom.css" rel="stylesheet">
</head>
<body class="page-header-fixed page-sidebar-fixed">
  <section id="wrapper">
    @include('layouts.header')
    @include('layouts.aside')
    <section id="page-content">
      <div class="body-content animated fadeIn">

          @if(session('success'))
          <div class="alert alert-success">
            {{ session('success') }}
          </div>
          @elseif(session('error'))
          <div class="alert alert-danger">
            {{ session('success') }}
          </div>
          @endif
        <div class="row">
          <div class="col-md-12">
            <div class="panel rounded shadow">
              <div class="panel-heading">
                <div class="pull-left">
                  <h3 class="panel-title">Basic Information</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                  <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                  <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
              </div>
              <div class="panel-body no-padding">
          <form class="form-horizontal form-bordered" method="POST" action="validateRegistrationForm" role="form" id="">
            @if(count($errors))
              <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
              @csrf
              <div class="form-body">
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Title <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" name="title">
                      <option  value="{{ old('title') }}">{{ old('title') }}</option>
                      <option value="MR.">MR.</option>
                      <option value="MS.">MS.</option>
                      <option value="MRS.">MRS.</option>
                      <option value="MISS">MISS</option>
                      <option value="REV.">REV.</option>
                      <option value="DOC.">DOC.</option>
                      <option value="PROF.">PROF.</option>
                      <option value="CAPT">CAPT.</option>
                      <option value="ALHJ">ALHJ.</option>
                      <option value="ACP.">ACP.</option>
                      <option value="APST.">APST.</option>
                      <option value="COL.">COL.</option>
                      <option value="MAJ.">MAJ.</option>
                      <option value="SUPT.">SUPT.</option>
                      <option value="PS.">PS.</option>
                      <option value="REV. SIS">REV. SIS.</option>
                      <option value="LT. COL.">LT. COL.</option>
                      <option value="REV. DOC.">REV. DOC.</option>
                      <option value="REV. PROF.">REV. PROF.</option>
                      <option value="CAPT. RTD.">CAPT. RTD.</option>
                      <option value="DR.(MRS)">DR.(MRS.)</option>
                      <option value="PROF. DOC.">PROF. DOC.</option>
                    </select>
                  </div>
                  <span class="text-danger">{{ $errors->first('title') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">First Name <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control input-sm" value="{{ old('first_name') }}" name="first_name" placeholder="First Name">
                  </div>
                  <span class="text-danger">{{ $errors->first('first_name') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('middle_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Middle Name</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control input-sm"  name="middle_name" value="{{ old('middle_name') }}" placeholder="Middle Name">
                  </div>
                  <span class="text-danger">{{ $errors->first('middle_name') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Last Name <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control input-sm" value="{{ old('last_name') }}" name="last_name" placeholder="Last Name">
                  </div>
                  <span class="text-danger">{{ $errors->first('last_name') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Date of Birth</label>
                  <div class="col-sm-7">
                    <input type="date" class="form-control input-sm" value="{{ old('dob') }}" name="dob"  placeholder="Date of Birth">
                  </div>
                  <span class="text-danger">{{ $errors->first('dob') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Gender<span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" name="gender" value="">
                      <option value="{{ old('gender') }}">{{ old('gender') }}</option>
                      <option value="Female">Female</option>
                      <option value="Male">Male</option>
                    </select>
                    <label for="sv_skill_programming" class="error"></label>
                    <input type="text" class="hide" id="sv_skill_programming"/>
                  </div>
                  <span class="text-danger">{{ $errors->first('gender') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('marital_status') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Marital Status<span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control"  name="marital_status">
                      <option value="{{ old('marital_status') }}">{{ old('marital_status') }}</option>
                      <option value="Married">Married</option>
                      <option value="Single">Single</option>
                      <option value="Devorced">Devorced</option>
                      <option value="Widow">Widow</option>
                    </select>
                    <label for="sv_skill_programming" class="error"></label>
                    <input type="text" class="hide" id="sv_skill_programming"/>
                  </div>
                  <span class="text-danger">{{ $errors->first('marital_status') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('maiden_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Maiden Name</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control input-sm" value="{{ old('maiden_name') }}" name="maiden_name" placeholder="Maiden Name">
                  </div>
                  <span class="text-danger">{{ $errors->first('maiden_name') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('spouse_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Spouse Name</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control input-sm" value="{{ old('spouse_name') }}" name="spouse_name" placeholder="Spouse Name">
                  </div>
                  <span class="text-danger">{{ $errors->first('spouse_name') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('email_address') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Email Address</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control input-sm" value="{{ old('email_address') }}" name="email_address" placeholder="Email Address">
                  </div>
                  <span class="text-danger">{{ $errors->first('email_address') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group">
                  <label class="col-sm-3 control-label">Alt. Email Address</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control input-sm" name="alt_email" placeholder="Alt. Email Address">
                  </div>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Mobile</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('mobile') }}" class="form-control input-sm" name="mobile" placeholder="Mobile">
                  </div>
                  <span class="text-danger">{{ $errors->first('mobile') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group">
                  <label class="col-sm-3 control-label">Alt. Mobile</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control input-sm" name="alt_mobile" placeholder="Alt. Mobile">
                  </div>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('home_address') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Home Address</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('home_address') }}" class="form-control input-sm" name="home_address" placeholder="Home Address">
                  </div>
                  <span class="text-danger">{{ $errors->first('home_address') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('suburb') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Suburb</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('suburb') }}" class="form-control input-sm" name="suburb" placeholder="Suburb">
                  </div>
                  <span class="text-danger">{{ $errors->first('suburb') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">City</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('city') }}" class="form-control input-sm" name="city" placeholder="City">
                  </div>
                  <span class="text-danger">{{ $errors->first('city') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Country</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('country') }}" class="form-control input-sm" name="country" placeholder="Country">
                  </div>
                  <span class="text-danger">{{ $errors->first('country') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('place_of_birth') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Place of Birth</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('place_of_birth') }}" class="form-control input-sm" name="place_of_birth" placeholder="Place of Birth">
                  </div>
                  <span class="text-danger">{{ $errors->first('place_of_birth') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('nationality') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Nationality</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('nationality') }}" class="form-control input-sm" name="nationality" placeholder="Nationality">
                  </div>
                  <span class="text-danger">{{ $errors->first('nationality') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group {{ $errors->has('gh_post_code') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">GH Post Code</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('gh_post_code') }}" class="form-control input-sm" name="gh_post_code" placeholder="Gh Post Code">
                  </div>
                  <span class="text-danger">{{ $errors->first('gh_post_code') }}</span>
                </div><!-- /.form-group -->
              </div>
            </div><!-- /.panel-body -->
          </div><!-- /.panel -->
          <!--/ End basic validation -->

        </div>
      </div><!-- /.row -->
      <div class="row">
        <div class="col-md-12">

          <!-- Start number validation -->
          <div class="panel rounded shadow">
            <div class="panel-heading">
              <div class="pull-left">
                <h3 class="panel-title">Professional Information</h3>
              </div>
              <div class="pull-right">
                <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
              </div>
              <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body no-padding">

              <div class="form-body">
                <div class="form-group {{ $errors->has('employer_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Employer Name</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('employer_name') }}" class="form-control input-sm" name="employer_name" placeholder="Employer Name">
                  </div>
                  <span class="text-danger">{{ $errors->first('employer_name') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('employer_address') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Employer Address</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('employer_address') }}" class="form-control input-sm" name="employer_address" placeholder="Employer Address">
                  </div>
                  <span class="text-danger">{{ $errors->first('employer_address') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('employer_contact') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Employer Contact</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('employer_contact') }}" class="form-control input-sm" name="employer_contact" placeholder="Employer Contact">
                  </div>
                  <span class="text-danger">{{ $errors->first('employer_contact') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('employer_suburb') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Employer Suburb</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('employer_suburb') }}" class="form-control input-sm" name="employer_suburb" placeholder="Employer Suburb">
                  </div>
                  <span class="text-danger">{{ $errors->first('employer_suburb') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('employer_city') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Employer City</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('employer_city') }}" class="form-control input-sm" name="employer_city" placeholder="Employer City">
                  </div>
                  <span class="text-danger">{{ $errors->first('employer_city') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('employer_location') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Employer Location</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('employer_location') }}" class="form-control input-sm" name="employer_location" placeholder="Employer City">
                  </div>
                  <span class="text-danger">{{ $errors->first('employer_location') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('employer_country') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Employer Country</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('employer_country') }}" class="form-control input-sm" name="employer_country" placeholder="Employer Country">
                  </div>
                  <span class="text-danger">{{ $errors->first('employer_country') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('employer_gh_post_code') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Employer Gh. Post Code</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('employer_gh_post_code') }}" class="form-control input-sm" name="employer_gh_post_code" placeholder="Employer Gh. Post Code">
                  </div>
                  <span class="text-danger">{{ $errors->first('employer_gh_post_code') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('professional_title') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Professional Title</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('professional_title') }}" class="form-control input-sm" name="professional_title" placeholder="Professional Title">
                  </div>
                  <span class="text-danger">{{ $errors->first('professional_title') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('profession') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Profession</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('profession')}}" class="form-control input-sm" name="profession" placeholder="Profession">
                  </div>
                  <span class="text-danger">{{ $errors->first('profession') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('educational_level') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Educational Level</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('educational_level') }}" class="form-control input-sm" name="educational_level" placeholder="educational_level">
                  </div>
                  <span class="text-danger">{{ $errors->first('educational_level') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('area_of_practice') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Area of Practice</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('area_of_practice') }}" class="form-control input-sm" name="area_of_practice" placeholder="Area of Practice">
                  </div>
                  <span class="text-danger">{{ $errors->first('area_of_practice') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('qualification') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Qualification</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('qualification') }}" class="form-control input-sm" name="qualification" placeholder="Qualification">
                  </div>
                  <span class="text-danger">{{ $errors->first('qualification') }}</span>
                </div>
                <br><br>
                <div class="form-group {{ $errors->has('area_of_pharmacy_practice') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Area of Pharmacy Practice <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" value="{{ old('area_of_pharmacy_practice') }}" name="area_of_pharmacy_practice" multiple="multiple">
                      <option value="">Choose position</option>
                      <option value="accountant">Accountant</option>
                      <option value="software engineer">Software Engineer</option>
                      <option value="sales assistant">Sales Assistant</option>
                    </select>
                    <label for="sv_position" class="error"></label>
                    <input type="text" class="hide" id="sv_position"/>
                  </div>
                  <span class="text-danger">{{ $errors->first('area_of_pharmacy_practice') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('practice_group_association') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Practice Group Association <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" value="{{ old('practice_group_association') }}" name="practice_group_association" multiple="multiple">
                      <option value="">Choose position</option>
                      <option value="accountant">Accountant</option>
                      <option value="software engineer">Software Engineer</option>
                      <option value="sales assistant">Sales Assistant</option>
                    </select>
                    <label for="sv_position" class="error"></label>
                    <input type="text" class="hide" id="sv_position"/>
                  </div>
                  <span class="text-danger">{{ $errors->first('practice_group_association') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('interest_group_association') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Interest Group Association <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" value="" name="interest_group_association" multiple="multiple">
                      <option value="">Choose position</option>
                      <option value="accountant">Accountant</option>
                      <option value="software engineer">Software Engineer</option>
                      <option value="sales assistant">Sales Assistant</option>
                    </select>
                    <label for="sv_position" class="error"></label>
                    <input type="text" class="hide" id="sv_position"/>
                  </div>
                  <span class="text-danger">{{ $errors->first('interest_group_association') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('university_attended') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Pharmacy School or University Attended <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" value="{{ old('university_attended') }}" name="university_attended" multiple="multiple">
                      <option value="">Choose position</option>
                      <option value="accountant">Accountant</option>
                      <option value="software engineer">Software Engineer</option>
                      <option value="sales assistant">Sales Assistant</option>
                    </select>
                    <label for="sv_position" class="error"></label>
                    <input type="text" class="hide" id="sv_position"/>
                  </div>
                  <span class="text-danger">{{ $errors->first('university_attended') }}</span>
                </div>
                <br><br>
                <div class="form-group wrapper">
                  <label class="col-sm-3 control-label">School Attended with Years <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="school_attended_with_years[]" placeholder="School Attended with Years" value="">
                    <label for="sv_position" class="error"></label>
                    <input type="text" class="hide" id="sv_position"/>
                  </div>
                </div>
                <button class="btn btn-primary add_fields pull-right"> <i class="fa fa-plus"></i>  Add More Fields</button>

              </div>
            </div>
          </div>
        </div>
      </div><!-- /.row -->
      <div class="row">
        <div class="col-md-12">

          <!-- Start password validation -->
          <div class="panel rounded shadow">
            <div class="panel-heading">
              <div class="pull-left">
                <h3 class="panel-title">Account Information</h3>
              </div>
              <div class="pull-right">
                <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
              </div>
              <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body no-padding">

              <div class="form-body">
                <br>
                <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Username</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('username') }}" class="form-control input-sm" name="username">
                  </div>
                  <span class="text-danger">{{ $errors->first('username') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Default Password</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('password') }}" class="form-control input-sm" name="password">
                  </div>
                </div>
                <br><br>
              </div>
            </div>
          </div>
        </div>
      </div><!-- /.row -->

      <div class="row">
        <div class="col-md-12">

          <!-- Start select boxes validation -->
          <div class="panel rounded shadow">
            <div class="panel-heading">
              <div class="pull-left">
                <h3 class="panel-title">Membership Information</h3>
              </div>
              <div class="pull-right">
                <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
              </div>
              <div class="clearfix"></div>
            </div><!-- /.panel-heading -->
            <div class="panel-body no-padding">

              <div class="form-body">
                <div class="form-group">
                  <label class="col-sm-3 control-label">Constituent ID</label>
                  <div class="col-sm-7">
                    <input type="text" readonly value="{{ App\MemberId::new_id() }}" class="form-control input-sm" name="constituent_id">
                  </div>
                </div>
                <br><br>
                <div class="form-group">
                  <label class="col-sm-3 control-label">Membership <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" name="membership" multiple="multiple">
                      <option value="">Choose position</option>
                      <option value="accountant">Accountant</option>
                      <option value="software engineer">Software Engineer</option>
                      <option value="sales assistant">Sales Assistant</option>
                    </select>
                    <label for="sv_position" class="error"></label>
                    <input type="text" class="hide" id="sv_position"/>
                  </div>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('regional_branch_chapter') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Regional Branch Chapter<span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" value="{{ old('regional_branch_chapter') }}" name="regional_branch_chapter">
                      <option value=""></option>
                      <option value="Greater Accra">Greater Accra</option>
                      <option value="Ashanti">Ashanti</option>
                      <option value="Western South">Western</option>
                      <option value="Western North">Western North</option>
                      <option value="Eastern">Eastern</option>
                      <option value="Central">Central</option>
                      <option value="Volta">Volta</option>
                      <option value="Oti">Oti</option>
                      <option value="Brong East">Bono East</option>
                      <option value="Brong West">Bono</option>
                      <option value="Ahafo">Ahafo</option>
                      <option value="Nothern">Nothern</option>
                      <option value="Savannah">Savannah</option>
                      <option value="North East">North East</option>
                      <option value="Upper East">Upper East</option>
                      <option value="Upper West">Upper West</option>
                      <option value="Abroad">Abroad</option>
                    </select>
                    <label for="sv_skill_programming" class="error"></label>
                    <input type="text" class="hide" id="sv_skill_programming"/>
                  </div>
                  <span class="text-danger">{{ $errors->first('regional_branch_chapter') }}</span>
                </div><!-- /.form-group -->
                <br><br>
                <div class="form-group {{ $errors->has('registration_date') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Registration Date</label>
                  <div class="col-sm-7">
                    <input type="date" value="{{ old('registration_date') }}" class="form-control input-sm" name="registration_date">
                  </div>
                </div>
                <span class="text-danger">{{ $errors->first('registration_date') }}</span>
              </div><!-- /.form-body -->
              <br><br><br><br>
              <div class="form-footer">
                <div class="col-sm-offset-3">
                  <button type="submit" class="btn btn-theme">Save</button>
                </div>
              </div><!-- /.form-footer -->
            </form>
          </div><!-- /.panel-body -->
        </div>
      </div>
    </div><!-- /.row -->


  </div><!-- /.body-content -->
  <!--/ End body content -->

  <!-- Start footer content -->
  <footer class="footer-content">
    2014 - <span id="copyright-year"></span> &copy; Blankon Admin. Created by <a href="http://djavaui.com/" target="_blank">Djava UI</a>, Yogyakarta ID
    <span class="pull-right">0.01 GB(0%) of 15 GB used</span>
  </footer><!-- /.footer-content -->
  <!--/ End footer content -->

</section><!-- /#page-content -->
<!--/ END PAGE CONTENT -->

<!-- START @SIDEBAR RIGHT -->
<aside id="sidebar-right">

  <div class="panel panel-tab">
    <div class="panel-heading no-padding">
      <div class="pull-right">
        <ul class="nav nav-tabs">
          <li>
            <a href="form-validation.html#sidebar-profile" data-toggle="tab">
              <i class="fa fa-user"></i>
            </a>
          </li>
          <li>
            <a href="form-validation.html#sidebar-layout" data-toggle="tab">
              <i class="fa fa-cogs"></i>
            </a>
          </li>
          <li class="active">
            <a href="form-validation.html#sidebar-setting" data-toggle="tab">
              <i class="fa fa-paint-brush"></i>
            </a>
          </li>
          <li>
            <a href="form-validation.html#sidebar-chat" data-toggle="tab">
              <i class="fa fa-comments"></i>
            </a>
          </li>
        </ul>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body no-padding">
      <div class="tab-content">
        <div class="tab-pane" id="sidebar-profile">
          <div class="sidebar-profile">

            <!-- Start right navigation - menu -->
            <ul class="sidebar-menu niceScroll">

              <!-- Start category about me -->
              <li class="sidebar-category">
                <span>ABOUT ME</span>
                <span class="pull-right"><i class="fa fa-newspaper-o"></i></span>
              </li>
              <!--/ End category about me -->

              <!--/ Start navigation - about me -->
              <li>
                <ul class="list-unstyled">
                  <li>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  </li>
                  <li>
                    <i class="fa fa-refresh"></i> Last update about 2 hours ago
                  </li>
                  <li>
                    <i class="fa fa-clock-o"></i> Total time spent 250 hrs
                  </li>
                  <li>
                    <i class="fa fa-envelope"></i> Tol.lee@djavaui.com
                  </li>
                  <li>
                    <i class="fa fa-mobile"></i> 1 405 777 1212
                  </li>
                </ul>
              </li>
              <!--/ End navigation - about me -->

              <!-- Start category recent activity -->
              <li class="sidebar-category">
                <span>RECENT ACTIVITY</span>
                <span class="pull-right"><i class="fa fa-line-chart"></i></span>
              </li>
              <!--/ End category recent activity -->

              <!--/ Start navigation - activity -->
              <li>
                <div class="media-list activity">
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <i class="fa fa-flash"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Added a note to Ticket #947</span>
                      <span class="media-meta time">about 2 hours ago</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <i class="fa fa-flash"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Added a product to Ticket #948</span>
                      <span class="media-meta time">about 3 hours ago</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <i class="fa fa-flash"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Added a service to Ticket #949</span>
                      <span class="media-meta time">about 15 hours ago</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                </div><!-- /.media-list -->
              </li>
              <!--/ End navigation - activity -->

              <!-- Start category current working -->
              <li class="sidebar-category">
                <span>CURRENTLY WORKING</span>
                <span class="pull-right"><i class="fa fa-group"></i></span>
              </li>
              <!--/ End category current working -->

              <!-- Start left navigation - current working -->
              <li>
                <div class="media-list working">
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/10.png" class="img-circle" alt="Daddy Botak">
                      <i class="online"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Daddy Botak</span>
                      <span class="media-meta status">online</span>
                      <span class="media-meta device"><i class="fa fa-globe"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/11.png" class="img-circle" alt="Sarah Tingting">
                      <i class="offline"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Sarah Tingting</span>
                      <span class="media-meta status">offline</span>
                      <span class="media-meta device"><i class="fa fa-globe"></i></span>
                      <span class="media-meta time">7 m</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/26.png" class="img-circle" alt="">
                      <i class="busy"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Nicolas Olivier</span>
                      <span class="media-meta status">busy</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/13.png" class="img-circle" alt="Claudia Cinta">
                      <i class="online"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Claudia Cinta</span>
                      <span class="media-meta status">online</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/26.png" class="img-circle" alt="">
                      <i class="busy"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Catherine Parker</span>
                      <span class="media-meta status">busy</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                </div><!-- /.media-list -->
              </li>
              <!--/ End left navigation - current working -->

            </ul>
            <!-- Start right navigation - menu -->
          </div>
        </div><!-- /#sidebar-profile -->
        <div class="tab-pane" id="sidebar-layout">
          <div class="sidebar-layout">

            <!-- Start right navigation - menu -->
            <ul class="sidebar-menu niceScroll">

              <!--/ Start navigation - reset settings -->
              <li>
                <a id="reset-setting" href="javascript:void(0);" class="btn btn-inverse btn-block"><i class="fa fa-refresh fa-spin"></i> RESET SETTINGS</a>
              </li>
              <!--/ End navigation - reset settings -->

              <!-- Start category layout -->
              <li class="sidebar-category">
                <span>LAYOUT</span>
                <span class="pull-right"><i class="fa fa-toggle-on"></i></span>
              </li>
              <!--/ End category layout -->

              <!--/ Start navigation - layout -->
              <li>
                <ul class="list-unstyled layout-setting">
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="layout-fluid" type="radio" name="layout" value="">
                      <label for="layout-fluid">Fluid</label>
                    </div>
                  </li>
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="layout-boxed" type="radio" name="layout" value="page-boxed">
                      <label for="layout-boxed">Boxed</label>
                    </div>
                  </li>
                </ul>
              </li>
              <!--/ End navigation - layout -->

              <!-- Start category header -->
              <li class="sidebar-category">
                <span>HEADER</span>
                <span class="pull-right"><i class="fa fa-toggle-on"></i></span>
              </li>
              <!--/ End category header -->

              <!--/ Start navigation - header -->
              <li>
                <ul class="list-unstyled header-layout-setting">
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="header-default" type="radio" name="header" value="">
                      <label for="header-default">Default</label>
                    </div>
                  </li>
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="header-fixed" type="radio" name="header" value="page-header-fixed">
                      <label for="header-fixed">Fixed</label>
                    </div>
                  </li>
                </ul>
              </li>
              <!--/ End navigation - header -->

              <!-- Start category sidebar -->
              <li class="sidebar-category">
                <span>SIDEBAR</span>
                <span class="pull-right"><i class="fa fa-toggle-on"></i></span>
              </li>
              <!--/ End category sidebar -->

              <!--/ Start navigation - sidebar -->
              <li>
                <ul class="list-unstyled sidebar-layout-setting">
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="sidebar-default" type="radio" name="sidebar" value="">
                      <label for="sidebar-default">Default</label>
                    </div>
                  </li>
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="sidebar-fixed" type="radio" name="sidebar" value="page-sidebar-fixed">
                      <label for="sidebar-fixed">Fixed</label>
                    </div>
                  </li>
                </ul>
              </li>
              <!--/ End navigation - sidebar -->

              <!-- Start category sidebar type -->
              <li class="sidebar-category">
                <span>SIDEBAR TYPE</span>
                <span class="pull-right"><i class="fa fa-toggle-on"></i></span>
              </li>
              <!--/ End category sidebar type -->

              <!--/ Start navigation - sidebar -->
              <li>
                <ul class="list-unstyled sidebar-type-setting">
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="sidebar-type-default" type="radio" name="sidebarType" value="">
                      <label for="sidebar-type-default">Default</label>
                    </div>
                  </li>
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="sidebar-type-box" type="radio" name="sidebarType" value="sidebar-box">
                      <label for="sidebar-type-box">Box</label>
                    </div>
                  </li>
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="sidebar-type-rounded" type="radio" name="sidebarType" value="sidebar-rounded">
                      <label for="sidebar-type-rounded">Rounded</label>
                    </div>
                  </li>
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="sidebar-type-circle" type="radio" name="sidebarType" value="sidebar-circle">
                      <label for="sidebar-type-circle">Circle</label>
                    </div>
                  </li>
                </ul>
              </li>
              <!--/ End navigation - sidebar -->

              <!-- Start category footer -->
              <li class="sidebar-category">
                <span>FOOTER</span>
                <span class="pull-right"><i class="fa fa-toggle-on"></i></span>
              </li>
              <!--/ End category footer -->

              <!--/ Start navigation - footer -->
              <li>
                <ul class="list-unstyled footer-layout-setting">
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="footer-default" type="radio" name="footer" value="">
                      <label for="footer-default">Default</label>
                    </div>
                  </li>
                  <li>
                    <div class="rdio rdio-theme">
                      <input id="footer-fixed" type="radio" name="footer" value="page-footer-fixed">
                      <label for="footer-fixed">Fixed</label>
                    </div>
                  </li>
                </ul>
              </li>
              <!--/ End navigation - footer -->

            </ul>
            <!-- Start right navigation - menu -->
          </div>
        </div><!-- /#sidebar-layout -->
        <div class="tab-pane in active" id="sidebar-setting">
          <div class="sidebar-setting">
            <!-- Start right navigation - menu -->
            <ul class="sidebar-menu">

              <!-- Start category color schemes -->
              <li class="sidebar-category">
                <span>COLOR SCHEMES</span>
                <span class="pull-right"><i class="fa fa-tint"></i></span>
              </li>
              <!--/ End category color schemes -->

              <!-- Start navigation - themes -->
              <li>
                <div class="sidebar-themes color-schemes">

                  <a class="theme" href="javascript:void(0);" style="background-color: #81b71a" data-toggle="tooltip" data-placement="right" data-original-title="Default"><span class="hide">default</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #00B1E1" data-toggle="tooltip" data-placement="top" data-original-title="Blue"><span class="hide">blue</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #37BC9B" data-toggle="tooltip" data-placement="top" data-original-title="Cyan"><span class="hide">cyan</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #8CC152" data-toggle="tooltip" data-placement="top" data-original-title="Green"><span class="hide">green</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #E9573F" data-toggle="tooltip" data-placement="top" data-original-title="Red"><span class="hide">red</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #F6BB42" data-toggle="tooltip" data-placement="top" data-original-title="Yellow"><span class="hide">yellow</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #906094" data-toggle="tooltip" data-placement="top" data-original-title="Purple"><span class="hide">purple</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #D39174" data-toggle="tooltip" data-placement="top" data-original-title="Brown"><span class="hide">brown</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #9FB478" data-toggle="tooltip" data-placement="left" data-original-title="Green Army"><span class="hide">green-army</span></a>

                  <a class="theme" href="javascript:void(0);" style="background-color: #63D3E9" data-toggle="tooltip" data-placement="right" data-original-title="Blue Sea"><span class="hide">blue-sea</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #5577B4" data-toggle="tooltip" data-placement="top" data-original-title="Blue Gray"><span class="hide">blue-gray</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #AF86B9" data-toggle="tooltip" data-placement="top" data-original-title="Purple Gray"><span class="hide">purple-gray</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #CC6788" data-toggle="tooltip" data-placement="top" data-original-title="Purple Wine"><span class="hide">purple-wine</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #F06F6F" data-toggle="tooltip" data-placement="top" data-original-title="Alizarin Crimson"><span class="hide">alizarin-crimson</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #979797" data-toggle="tooltip" data-placement="top" data-original-title="Black And White"><span class="hide">black-and-white</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #8BC4B9" data-toggle="tooltip" data-placement="top" data-original-title="Amazon"><span class="hide">amazon</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #B0B069" data-toggle="tooltip" data-placement="top" data-original-title="Amber"><span class="hide">amber</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #A9C784" data-toggle="tooltip" data-placement="left" data-original-title="Android green"><span class="hide">android-green</span></a>

                  <a class="theme" href="javascript:void(0);" style="background-color: #B3998A" data-toggle="tooltip" data-placement="right" data-original-title="Antique brass"><span class="hide">antique-brass</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #8D8D6E" data-toggle="tooltip" data-placement="top" data-original-title="Antique Bronze"><span class="hide">antique-bronze</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #B0B69D" data-toggle="tooltip" data-placement="top" data-original-title="Artichoke"><span class="hide">artichoke</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #F19B69" data-toggle="tooltip" data-placement="top" data-original-title="Atomic Tangerine"><span class="hide">atomic-tangerine</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #98777B" data-toggle="tooltip" data-placement="top" data-original-title="Bazaar"><span class="hide">bazaar</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #C3A961" data-toggle="tooltip" data-placement="top" data-original-title="Bistre Brown"><span class="hide">bistre-brown</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #D6725E" data-toggle="tooltip" data-placement="top" data-original-title="Bittersweet"><span class="hide">bittersweet</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #7789D1" data-toggle="tooltip" data-placement="top" data-original-title="Blueberry"><span class="hide">blueberry</span></a>
                  <a class="theme" href="javascript:void(0);" style="background-color: #6FA362" data-toggle="tooltip" data-placement="left" data-original-title="Bud Green"><span class="hide">bud-green</span></a>

                </div><!-- /.sidebar-themes -->
              </li>
              <!--/ End navigation - themes -->

              <!-- Start category navbar color -->
              <li class="sidebar-category">
                <span>NAVBAR COLOR</span>
                <span class="pull-right"><i class="fa fa-tint"></i></span>
              </li>
              <!--/ End category navbar color -->

              <!-- Start navigation - navbar color -->
              <li>
                <div class="sidebar-themes navbar-color">

                  <a class="theme bg-dark" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Dark"><span class="hide">dark</span></a>
                  <a class="theme bg-light" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Light"><span class="hide">light</span></a>
                  <a class="theme bg-primary" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Primary"><span class="hide">primary</span></a>
                  <a class="theme bg-success" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Success"><span class="hide">success</span></a>
                  <a class="theme bg-info" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Info"><span class="hide">info</span></a>
                  <a class="theme bg-warning" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Warning"><span class="hide">warning</span></a>
                  <a class="theme bg-danger" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Danger"><span class="hide">danger</span></a>

                </div><!-- /.navbar-color -->
              </li>
              <li class="sidebar-category">
                <span>SIDEBAR COLOR</span>
                <span class="pull-right"><i class="fa fa-tint"></i></span>
              </li>
              <!--/ End category sidebar color -->

              <!-- Start navigation - sidebar color -->
              <li>
                <div class="sidebar-themes sidebar-color">

                  <a class="theme bg-dark" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Dark"><span class="hide">dark</span></a>
                  <a class="theme bg-light" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Light"><span class="hide">light</span></a>
                  <a class="theme bg-primary" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Primary"><span class="hide">primary</span></a>
                  <a class="theme bg-success" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Success"><span class="hide">success</span></a>
                  <a class="theme bg-info" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Info"><span class="hide">info</span></a>
                  <a class="theme bg-warning" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Warning"><span class="hide">warning</span></a>
                  <a class="theme bg-danger" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-original-title="Danger"><span class="hide">danger</span></a>

                </div><!-- /.sidebar-color -->
              </li>
              <!--/ End navigation - sidebar color -->

              <!-- Start category task progress -->
              <li class="sidebar-category">
                <span>TASK PROGRESS</span>
                <span class="pull-right"><i class="fa fa-sliders"></i></span>
              </li>
              <!--/ End category task progress -->

              <!--/ Start navigation - task progress -->
              <li>
                <ul class="list-group sidebar-task">
                  <li class="list-group-item">
                    <p class="details"> <span> Core System </span> <span class="pull-right"> 82% </span> </p>
                    <div class="progress progress-xs progress-striped active no-margin">
                      <div style="width: 82%" class="progress-bar progress-bar-success"> </div>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <p class="details"> <span> Front-End </span> <span class="pull-right"> 67% </span> </p>
                    <div class="progress progress-xs progress-striped active no-margin">
                      <div style="width: 47%" class="progress-bar progress-bar-danger"> </div>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <p class="details"> <span> Back-End </span> <span class="pull-right"> 45% </span> </p>
                    <div class="progress progress-xs progress-striped active no-margin">
                      <div style="width: 47%" class="progress-bar progress-bar-info"> </div>
                    </div>
                  </li>
                </ul>
              </li>
              <!--/ End navigation - task progress -->

              <!-- Start category summary system -->
              <li class="sidebar-category">
                <span>SUMMARY SYSTEM</span>
                <span class="pull-right"><i class="fa fa-bar-chart-o"></i></span>
              </li>
              <!--/ End category summary system -->

              <!-- Start left navigation - summary -->
              <li>
                <ul class="sidebar-summary sparklines">
                  <li>
                    <div class="list-info">
                      <span>Average Users</span>
                      <h4>1, 412, 101</h4>
                    </div>
                    <div class="chart"><span class="average">4,2,3,2,4,2,5,1,2,2,5,3</span></div>
                    <div class="clearfix"></div>
                  </li>
                  <li>
                    <div class="list-info">
                      <span>Daily Traffic</span>
                      <h4>781, 601</h4>
                    </div>
                    <div class="chart"><span class="traffic">1,2,3,2,4,1,5,3,2,4,2,3</span></div>
                    <div class="clearfix"></div>
                  </li>
                  <li>
                    <div class="list-info">
                      <span>Disk Usage</span>
                      <h4>52.2%</h4>
                    </div>
                    <div class="chart"><span class="disk">5,5,3,2,1,3,4,3,2,4,1,3</span></div>
                    <div class="clearfix"></div>
                  </li>
                  <li>
                    <div class="list-info">
                      <span>CPU Usage</span>
                      <h4>67.8%</h4>
                    </div>
                    <div class="chart"><span class="cpu">1,5,3,2,4,5,5,3,2,4,5,3</span></div>
                    <div class="clearfix"></div>
                  </li>
                </ul>
              </li>
              <!--/ End left navigation - summary -->

            </ul>
            <!-- Start right navigation - menu -->
          </div>
        </div><!-- /#sidebar-setting -->
        <div class="tab-pane" id="sidebar-chat">
          <div class="sidebar-chat">

            <form class="form-horizontal">
              <div class="form-group has-feedback">
                <input class="form-control" type="text" placeholder="Search messages...">
                <span class="glyphicon glyphicon-search form-control-feedback"></span>
              </div>
            </form>

            <!-- Start right navigation - menu -->
            <ul class="sidebar-menu niceScroll">

              <!-- Start category family chat -->
              <li class="sidebar-category">
                <span>FAMILY</span>
                <span class="pull-right"><i class="fa fa-home"></i></span>
              </li>
              <!--/ End category family chat -->

              <li>
                <!-- Start chat - contact list -->
                <div class="media-list">
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/10.png" class="img-circle" alt="Daddy Botak">
                      <i class="online"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Daddy Botak</span>
                      <span class="media-meta status">online</span>
                      <span class="media-meta device"><i class="fa fa-globe"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/11.png" class="img-circle" alt="Sarah Tingting">
                      <i class="offline"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Sarah Tingting</span>
                      <span class="media-meta status">offline</span>
                      <span class="media-meta device"><i class="fa fa-globe"></i></span>
                      <span class="media-meta time">7 m</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/26.png" class="img-circle" alt="...">
                      <i class="busy"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Nicolas Olivier</span>
                      <span class="media-meta status">busy</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/12.png" class="img-circle" alt="Harry Potret">
                      <i class="online"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Harry Potret</span>
                      <span class="media-meta status">online</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/26.png" class="img-circle" alt="...">
                      <i class="busy"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Catherine Parker</span>
                      <span class="media-meta status">busy</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                </div><!-- /.media-list -->
                <!--/ End chat - contact list -->
              </li>

              <!-- Start category friends chat -->
              <li class="sidebar-category">
                <span>FRIENDS</span>
                <span class="pull-right"><i class="fa fa-group"></i></span>
              </li>
              <!--/ End category friends chat -->

              <li>
                <!-- Start chat - contact list -->
                <div class="media-list">
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/15.png" class="img-circle" alt="Jeck Joko">
                      <i class="online"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Jeck Joko</span>
                      <span class="media-meta status">online</span>
                      <span class="media-meta device"><i class="fa fa-globe"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/16.png" class="img-circle" alt="Tenny Imoet">
                      <i class="busy"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Tenny Imoet</span>
                      <span class="media-meta status">busy</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/17.png" class="img-circle" alt="Leli Madang">
                      <i class="offline"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Leli Madang</span>
                      <span class="media-meta status">offline</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                      <span class="media-meta time">2 m</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/18.png" class="img-circle" alt="Rebecca Cabean">
                      <i class="offline"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Rebecca Cabean</span>
                      <span class="media-meta status">offline</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                      <span class="media-meta time">8 m</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/26.png" class="img-circle" alt="...">
                      <i class="busy"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">ondoel return</span>
                      <span class="media-meta status">busy</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                </div><!-- /.media-list -->
                <!--/ End chat - contact list -->
              </li>

              <!-- Start category other chat -->
              <li class="sidebar-category">
                <span>OTHERS</span>
                <span class="pull-right"><i class="fa fa-share"></i></span>
              </li>
              <!--/ End category other chat -->

              <li>
                <!-- Start chat - contact list -->
                <div class="media-list">
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/19.png" class="img-circle" alt="Sishy Mawar">
                      <i class="offline"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Sishy Mawar</span>
                      <span class="media-meta status">offline</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                      <span class="media-meta time">23 m</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/20.png" class="img-circle" alt="Mbah Roso">
                      <i class="away"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Mbah Roso</span>
                      <span class="media-meta status">away</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                      <span class="media-meta time">2 h</span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                  <a href="form-validation.html#" class="media">
                    <div class="media-object pull-left has-notif">
                      <img src="../../../img/avatar/35/26.png" class="img-circle" alt="...">
                      <i class="busy"></i>
                    </div><!-- /.media-object -->
                    <div class="media-body">
                      <span class="media-heading">Alma Butoi</span>
                      <span class="media-meta status">busy</span>
                      <span class="media-meta device"><i class="fa fa-mobile"></i></span>
                    </div><!-- /.media-body -->
                  </a><!-- /.media -->
                </div><!-- /.media-list -->
                <!--/ End chat - contact list -->
              </li>

            </ul><!-- /.sidebar-menu -->
            <!-- Start right navigation - menu -->

          </div><!-- /.sidebar-chat -->
        </div><!-- /#sidebar-chat -->
      </div>
    </div>
  </div>
</aside><!-- /#sidebar-right -->
<!--/ END SIDEBAR RIGHT -->

</section><!-- /#wrapper -->
<!--/ END WRAPPER -->
<!-- START @BACK TOP -->
<div id="back-top" class="animated pulse circle">
  <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->

<!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
<!-- START @CORE PLUGINS -->
<script src="../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
<script src="../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
<script src="../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
<script src="../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>
<!--/ END CORE PLUGINS -->

<!-- START @PAGE LEVEL PLUGINS -->
<script src="../../../assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-mockjax/jquery.mockjax.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<!--/ END PAGE LEVEL PLUGINS -->

<!-- START @PAGE LEVEL SCRIPTS -->
<script src="../../../assets/admin/js/apps.js"></script>
<script src="../../../assets/admin/js/pages/blankon.form.validation.js"></script>
<script src="../../../assets/admin/js/demo.js"></script>
<!--/ END PAGE LEVEL SCRIPTS -->
<!--/ END JAVASCRIPT SECTION -->

<!-- START GOOGLE ANALYTICS -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55892530-1', 'auto');
ga('send', 'pageview');

</script>
<!--/ END GOOGLE ANALYTICS -->

<script>
//Add Input Fields
$(document).ready(function() {
  var max_fields = 10; //Maximum allowed input fields
  var wrapper    = $(".wrapper"); //Input fields wrapper
  var add_button = $(".add_fields"); //Add button class or ID
  var x = 1; //Initial input field is set to 1

  //When user click on add input button
  $(add_button).click(function(e){
    e.preventDefault();
    //Check maximum allowed input fields
    if(x < max_fields){
      x++; //input field increment
      //add input field
      $(wrapper).append('<div><label class="col-sm-3 control-label"></label><div class="col-sm-7"> <input type="text" class="form-control" name="school_attended_with_years[]" placeholder="School Attended with Years" value=""> <a href="javascript:void(0);" class="remove_field text-danger">Remove</a><br></div></div>');
    }
  });

  //when user click on remove button
  $(wrapper).on("click",".remove_field", function(e){
    e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
  })
});
</script>

</body>
<!--/ END BODY -->

</html>
