
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>PSGH | *</title>  <!--/ END META SECTION -->

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">

  <!-- START @FONT STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
  <!--/ END FONT STYLES -->

  <!-- START @GLOBAL MANDATORY STYLES -->
  <link href="../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!--/ END GLOBAL MANDATORY STYLES -->

  <!-- START @PAGE LEVEL STYLES -->
  <link href="../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/datatables/css/datatables.responsive.css" rel="stylesheet">
  <!--/ END PAGE LEVEL STYLES -->

  <!-- START @THEME STYLES -->
  <link href="../../../assets/admin/css/reset.css" rel="stylesheet">
  <link href="../../../assets/admin/css/layout.css" rel="stylesheet">
  <link href="../../../assets/admin/css/components.css" rel="stylesheet">
  <link href="../../../assets/admin/css/plugins.css" rel="stylesheet">
  <link href="../../../assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
  <link href="../../../assets/admin/css/custom.css" rel="stylesheet">


  <link href="../../../assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/jasny-bootstrap-fileinput/css/jasny-bootstrap-fileinput.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/chosen_v1.2.0/chosen.min.css" rel="stylesheet">

  <style media="screen">
  @media all {

    .portrait{
      border: solid;
      border-color: black;
      border-width: thin;
      padding-left: 10px;
      padding-right: 25px!important;
      background-color: #ffff;
      width: 350px;
    }
    .event-title{
      font-size: 22px;
      text-align: center;
      margin-left: 13px;
    }
    #qrcode{
      margin-left: 10px;
    }
    #qrcode2{

    }
    .top{
      margin-top: 10px;
    }
    .sponsor{
      background: yellow;
      width: 70px;
      border: solid;
      border-color: black;
      padding-left: 10px;
      text-align: center;
      margin-left: -10px;
    }
  }
  </style>
</head>
<body class="page-sound page-header-fixed page-sidebar-fixed page-footer-fixed">

  <section id="wrapper">
    @include('layouts.header')
    @include('layouts.aside')

    <!-- START @PAGE CONTENT -->
    <section id="page-content">

      <!-- Start page header -->
      <div class="header-content">
        <h2>Payments</h2>

      </div>
      @if(session('success'))
      <br>
      <div class="alert alert-success">
        <p>{{ session('success') }}</p>
      </div>
      @endif
      <div class="body-content animated fadeIn">

        <div class="row">
          <div class="col-md-12">

            <div class="panel panel-tab panel-tab-double shadow">
              <!-- Start tabs heading -->
              <div class="panel-heading no-padding">
                <ul class="nav nav-tabs">
                  <li class="active nav-border nav-border-top-success">
                    <a href="additional-option-system-manage-metadata.html#tab-setting-countries" data-toggle="tab">
                      <i class="icon-globe icons fg-success"></i>
                      <div>
                        <span class="text-strong">Payments</span>
                        <span>List of online payment received</span>
                      </div>
                    </a>
                  </li>

                </ul>
              </div><!-- /.panel-heading -->
              @if($from == '')
              @else
              <br>
              <div class="text-center text-info" style="font-size: 20px;">
                <i> Showing filtered result from <strong>{{ $from }}</strong> to <strong>{{ $to }}</strong></i>
              </div>
              @endif
              <div class="panel-body no-padding">
                <div class="panel panel-default shadow no-margin">

                  <div class="panel-body">
                    <div class="tab-content">
                      <div class="tab-pane fade in active" id="tab-setting-countries">
                        <div class="col-md-12 pull-right">
                          <div class="row form-group">
                            <div class="col-md-2 pull-right">
                              <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#addPayment"> <i class="fa fa-plus"></i> Add New</button><br><br>
                            </div>
                            <div class="col-md-1 pull-right">
                              <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#filterReport">Filter</button><br><br>
                            </div>
                          </div>
                        </div>

                        <table id="example" class="table table-default display table-bordered" style="width:100%">
                          <thead>
                            <tr>
                              <th data-class="expand" class="text-center">Registration No.</th>
                              <th>Receipt No.</th>
                              <th data-hide="phone">Name</th>
                              <th>Dues Type</th>
                              <th>Dues Type Code</th>
                              <th>Amount (GHS)</th>
                              <th>Payment Date</th>
                              <th>Transaction Type</th>
                              <th>Username</th>
                              <th>Description</th>
                            </tr>
                          </thead>
                          <tbody>
                            @php $amount = 0; @endphp
                            @foreach($duesPayments as $duesPayment)
                            @php $user = App\MemberUser::where('id', $duesPayment->user_id)->first();
                            if($user == null){
                            } else{ @endphp
                            @php $member = App\Member::where('id', $user->member_id)->first(); @endphp
                            <tr>
                              <td>{{ $member->Constituent_ID }}</td>
                              @php $check = App\PaymentTransaction::where('member_id', App\MemberUser::where('id', $duesPayment->user_id)->first()->member_id)->first(); @endphp
                              <td>@if($check == null || $check->reference == '' ) {{ $trans  =  App\Transaction::where('user_id', $duesPayment->user_id)->first()->trans_id }} @else {{ $trans = $check->reference }} @endif</td>
                              <td>{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name }}</td>
                              <td>{{ $member->Membership }}</td>
                              <td> @if($member->Membership == 'Member Dues') {{ __('4212') }} @elseif($member->Membership == 'Elderly Dues') {{ __('4214') }} @elseif($member->Membership == 'Newly Qualified Dues') {{ __('4216') }} @elseif($member->Membership == 'Fellows Dues') {{ __('4213') }} @endif</td>
                              <td>{{ App\DuesType::where('display_name', $member->Membership)->first()->amount  }}</td>
                              <td>{{ str_replace('-', '', date("d-m-Y", strtotime(substr($duesPayment->created_at, 0, -8))))}} </td>
                              <td> {{ App\Bank::find(App\PaymentTransaction::where('member_id', $user->member_id )->first()->trans_type)->name }}</td>
                              <td>{{ $user->username }}</td>
                              <td>{{ __('Being Payment for Annual Membership Dues') }}</td>
                            </tr>
                            @php } @endphp
                            @endforeach
                          </tbody>
                          <tfoot>

                            <tr>
                              <th data-class="expand" class="text-center">Registration No.</th>
                              <th>Receipt No.</th>
                              <th data-hide="phone">Name</th>
                              <th>Dues Type</th>
                              <th>Dues Type</th>
                              <th>Amount</th>
                              <th>Payment Date</th>
                              <th>Transaction Type</th>
                              <th>Username</th>
                              <th>Description</th>
                            </tr>
                          </tfoot>
                        </table>

                      </div>
                    </div>
                  </div><!-- /.panel-body -->
                </div><!-- /.panel -->
              </div><!-- /.panel-body -->
            </div><!-- /.panel -->

            <div class="panel panel-tab panel-tab-double shadow">
              <!-- Start tabs heading -->
              <div class="panel-heading no-padding">
                <ul class="nav nav-tabs">
                  <li class="active nav-border nav-border-top-success">
                    <a href="additional-option-system-manage-metadata.html#tab-setting-countries" data-toggle="tab">
                      <i class="icon-globe icons fg-success"></i>
                      <div>
                        <span class="text-strong">Payments</span>
                        <span>List of online and offline payment requests received</span>
                      </div>
                    </a>
                  </li>
                </ul>
              </div><!-- /.panel-heading -->

              <div class="panel-body no-padding">
                <div class="panel panel-default shadow no-margin">
                  <div class="panel-body">
                    <div class="tab-content">
                      <div class="tab-pane fade in active" id="tab-setting-countries1">

                        <table id="example2" class="table table-default display table-bordered" style="width:100%">
                          <thead>
                            <tr>
                              <th data-class="expand" class="text-center">Registration No.</th>
                              <th data-hide="phone">Name</th>
                              <th>Transaction ID</th>
                              <th>Amount (GHS)</th>
                              <th>Payment Date</th>
                              <th>Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($transactions as $paymentTransaction)
                            @php $user = App\MemberUser::where('id', $paymentTransaction->user_id)->first();
                            if($user == null){
                            } else{ @endphp
                            @php $member = App\Member::where('id', $user->member_id)->first(); @endphp
                            <tr>
                              <td>{{ $member->Constituent_ID }}</td>
                              <td>{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name }}</td>
                              <td>{{ $paymentTransaction->trans_id }}</td>
                              <td>{{ App\DuesType::where('display_name', $member->Membership)->first()->amount }}</td>
                              <td>{{ $paymentTransaction->created_at }} </td>
                              <td>@if($paymentTransaction->status == 'paid') <span class="fa fa-circle text-success">PAID</span> @else  <span class="fa fa-circle text-warning"> PENDING</span>@endif</td>
                            </tr>
                            @php } @endphp
                            @endforeach
                          </tbody>
                          <tfoot>
                            <tr>
                              <th data-class="expand" class="text-center">Registration No.</th>
                              <th data-hide="phone">Name</th>
                              <th>Transaction ID</th>
                              <th>Amount (GHS)</th>
                              <th>Payment Date</th>
                              <th>Status</th>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div><!-- /.panel-body -->
                </div>
              </div>
            </div><!-- /.panel -->

          </div>
        </div>
      </div>
      <footer class="footer-content">
        2014 - <span id="copyright-year"></span> &copy; Blankon Admin. Created by <a href="http://djavaui.com/" target="_blank">Djava UI</a>, Yogyakarta ID
        <span class="pull-right">0.01 GB(0%) of 15 GB used</span>
      </footer>
    </section>
  </section>
  <!-- <a href="javascript:void(0);" class="runner btn btn-mini btn-success btn-block" >Success</a> -->

  <!-- START @BACK TOP -->
  <div id="back-top" class="animated pulse circle">
    <i class="fa fa-angle-up"></i>
  </div><!-- /#back-top -->
  <!--/ END BACK TOP -->

  <div class="modal mkapp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        <style>
        .switch {
          position: relative;
          display: inline-block;
          width: 60px;
          height: 34px;
        }

        .switch input {
          opacity: 0;
          width: 0;
          height: 0;
        }

        .slider {
          position: absolute;
          cursor: pointer;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          background-color: #ccc;
          -webkit-transition: .4s;
          transition: .4s;
        }

        .slider:before {
          position: absolute;
          content: "";
          height: 26px;
          width: 26px;
          left: 4px;
          bottom: 4px;
          background-color: white;
          -webkit-transition: .4s;
          transition: .4s;
        }

        input:checked + .slider {
          background-color: #2196F3;
        }

        input:focus + .slider {
          box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
          -webkit-transform: translateX(26px);
          -ms-transform: translateX(26px);
          transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
          border-radius: 34px;
        }

        .slider.round:before {
          border-radius: 50%;
        }
        </style>

        <div class="modal-body">
          <div class="col-md-6 text-center" style="margin-left: 130px;">
            <form class="" action="register-as-paid" method="post">
              @csrf
              <input type="hidden" name="constituent_id" id="constituent_id" value="">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <select class="form-control" name="amount">
                      <option value="850">GH850</option>
                      <option value="750">GH750</option>
                      <option value="175">GH175</option>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <label class="switch text-center">
                      <input type="checkbox" name="checkbox" checked value="">
                      <span class="slider"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <br><br>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-theme"> <i class="fa fa-arrow-right"></i> Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade modal-filder-badge" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Filter Members</h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <form class="" action="filter-members" method="get">
              <select class="form-control" name="value" required>
                <option value="">Select</option>
                <option value="1">Paid</option>
                <option value="2">Not Paid</option>
                <option value="3">Registered & Paid</option>
                <option value="4">Registered Not Paid</option>
                <option value="5">Not Registered but Paid</option>
                <option value="6">Not Registered & Not Paid</option>
              </select>
            </div>
            <br><br>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-theme"> <i class="fa fa-arrow-right"></i> Go</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="filterReport" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Filter Report</h4>
        </div>
        <div class="modal-body">
          <form class="" action="{{ route('filterReport') }}" method="post">
            @csrf
            <div class="form-group">
              <div class="row">
                <div class="col-md-5">
                  From:  <input type="date" name="from" value="">
                </div>
                <div class="col-md-5">
                  To:  <input type="date" name="to" value="">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>   <button type="submit" class="btn btn-success" name="button">OK</button>
          </div>

        </form>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div class="modal fade" id="addPayment" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Adding New Dues Received</h4>
        </div>
        <div class="modal-body">
          <form class="" action="#" data-layout="topCenter" data-type="success" id="payment-form" method="post">
            @csrf
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label for="member_name">Member Name: </label>
                  <select data-placeholder="Choose a Member" id="member_name" name="constituent_id" class="chosen-select mb-15" tabindex="2" required>
                    <option value=""></option>
                    @foreach($members = App\Member::all() as $member)
                    <option value="{{ $member->Constituent_ID }}">{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name }} <small>{{ $member->Constituent_ID }}</small> </option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-3">
                  <label for="dues_type">Dues Type: </label>
                  <input type="text" name="dues_type" id="dues_type" class="form-control" value="" readonly required>
                </div>
                <div class="col-md-3">
                  <label for="amount">Amount: </label>
                  <input type="text" id="amount" name="amount" class="form-control" value="" required>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <label for="bank">Bank: </label>
                  <select data-placeholder="Choose a Bank" id="bank" name="bank" class="chosen-select mb-15" tabindex="2" required>
                    <option value=""></option>
                    @foreach($banks = App\Bank::all() as $bank)
                    <option value="{{ $bank->id }}">{{ $bank->name }} </option>
                    @endforeach
                  </select>
                </div>
                <div class="col-md-6">
                  <label for="reference_no">Reference No.: </label>
                  <input type="text" id="reference_no" name="reference_no" class="form-control" value="" required>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <label for="payment_date">Payment Date: </label>
                  <input type="date" class="form-control" id="payment_date" name="payment_date" value="" required>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>   <button type="submit" class="btn btn-success" name="button">OK</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script src="../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
  <script src="../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>

  <script src="../../../assets/global/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jasny-bootstrap-fileinput/js/jasny-bootstrap.fileinput.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/holderjs/holder.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-autosize/jquery.autosize.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js"></script>


  <script src="../../../assets/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/datatables.responsive.js"></script>
  <script src="../../../assets/admin/js/apps.js"></script>
  <script src="../../../assets/admin/js/pages/investor/blankon.investor.additional.option.system.metadata.js"></script>
  <script src="../../../assets/admin/js/custom-noty.js"></script>
  <script src="../../../assets/global/plugins/bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js"></script>

  <script src="../../../assets/admin/js/demo.js"></script>
  <script type="text/javascript" src="js/jquery.qrcode.min.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" charset="utf-8"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js" charset="utf-8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" charset="utf-8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" charset="utf-8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" charset="utf-8"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js" charset="utf-8"></script>



  <!--/ END PAGE LEVEL PLUGINS -->

  <!-- START @PAGE LEVEL SCRIPTS -->
  <script src="../../../assets/admin/js/pages/blankon.form.element.js"></script>

  <script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
      dom: 'Bfrtip',
      buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
      ]
    } );
  } );
  $(document).ready(function() {
    $('#example2').DataTable( {
      dom: 'Bfrtip',
      buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
      ]
    } );
  } );
  $(document).ready(function() {
    $('#example1').DataTable( {
      dom: 'Bfrtip',
      buttons: [
        'copyHtml5',
        'excelHtml5',
        'csvHtml5',
        'pdfHtml5'
      ]
    } );
  } );
  $(document).ready(function(){
    $('.btnClickFilter').on('click', function(){
      $('.modal-filder-badge').modal('show');
    });
    $('#datatable-setting-countries').on('click', '.btnPrintBadge', function(e){
      e.preventDefault();
      var $data = $(this).data('class');
      console.log($data);
      $.ajax(
        {
          url: 'view-member-badge/'+$data,
          type: 'get',
          success: function(response){
            if (response['data'] === 'success') {
              console.log(response['response']['Constituent_ID']);
              $('#modal-header').html(response['response']['First_Name']+ ' ' +response['response']['Middle_Name']+ ' ' +response['response']['Last_Name']);
              $('#mem-code').html(response['response']['Constituent_ID']);
              $('#first_name').html(response['response']['First_Name']);
              $('.bs-example-modal-lg').modal('show');
            }else {
              console.log('error');
              console.log(response);
            }
          }
        }
      )
    });
  });
  </script>

<script type="text/javascript">
$('#btnPrint').on('click', function(e){
  print($('.portrait'));
});
</script>
<script type="text/javascript">
$(document).ready(function(e){
  $('#example').on('click', '.mkap', function(){
    $id = $(this).data('class');
    $('.mkapp').modal('show');
    $('#constituent_id').val($id);
  });
});
$(document).ready(function(e){
  $('#example1').on('click', '.mkap', function(){
    $id = $(this).data('class');
    $('.mkapp').modal('show');
    $('#constituent_id').val($id);
  });
});
$(document).ready(function(e){
  $('#example2').on('click', '.mkap', function(){
    $id = $(this).data('class');
    $('.mkapp').modal('show');
    $('#constituent_id').val($id);
  });
  $('#member_name').on('change', function(e){
    e.preventDefault();
    $.ajax({
      url: 'fetch-member/'+$(this).val(),
      type: 'get',
      success: function(response){
        console.log(response);
        $('#dues_type').val(response.member.Membership);
        $('#amount').val(response.dues_type.amount);
      }
    })
  })
});

</script>
</body>
</html>
