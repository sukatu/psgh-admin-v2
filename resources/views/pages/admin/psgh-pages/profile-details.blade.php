
<label for="Cc"  class="col-sm-2 control-label">Category Details:</label>
<div class="col-sm-10" id="profile-categories">
  <select class="chosen-select" id="profile-category-details" name="profile-categories">
    <option value=""></option>
    @foreach($profile_details as $profile_detail)
      <option value="{{ $profile_detail->display_name }}">{{ $profile_detail->display_name }}</option>
    @endforeach
  </select>
  <span class="invalid-feedback text-danger profile-category-details-error" style="display: none;" role="alert">
    <strong>{{ __('This field is required') }}</strong>
  </span>
  <script src="../../../assets/admin/js/pages/blankon.form.layout.js"></script>
  <script src="{{ asset('assets/admin/js/jquery-ajax.js')}}" charset="utf-8"></script>
</div>
<script type="text/javascript"> 
  $(document).ready(function(){
    $('#profile-category-details').on('change', function(e){
      $('.profile-category-details-error').hide();
    });
  });
</script>
