@extends('layouts.tables')
@section('content')
<div class="header-content">
  <h3>Registered Members:  {{ App\EventRegistrant::count() }}</h3>

</div><!-- /.header-content -->
<!--/ End page header -->

<!-- Start body content -->
<div class="body-content animated fadeIn">

  <div class="row">
    <div class="col-md-12">

      <div class="panel panel-tab panel-tab-double shadow">
        <!-- Start tabs heading -->
        <div class="panel-heading no-padding">
          <ul class="nav nav-tabs">
            <li class="active nav-border nav-border-top-success">
              <a href="additional-option-system-manage-metadata.html#tab-setting-countries" data-toggle="tab">
                <i class="icon-globe icons fg-success"></i>
                <div>
                  <span class="text-strong">Members</span>
                  <span>List members default</span>
                </div>
              </a>
            </li>

          </ul>
          <a href="{{ route('badging') }}" class="btn btn-success btn-add-setting-countries" style="position: absolute;right: 12px;top: 12px;">Generate Badge &nbsp;<i class="fa fa-plus"></i></a>
          <a href="additional-option-system-manage-metadata.html#" class="btn btn-primary btn-add-setting-provinces" style="position: absolute;right: 12px;top: 12px;display: none;">Add New &nbsp;<i class="fa fa-plus"></i></a>
          <a href="additional-option-system-manage-metadata.html#" class="btn btn-danger btn-add-setting-currency-types" style="position: absolute;right: 12px;top: 12px;display: none;">Add New &nbsp;<i class="fa fa-plus"></i></a>
          <a href="additional-option-system-manage-metadata.html#" class="btn btn-info btn-add-setting-nationality" style="position: absolute;right: 12px;top: 12px;display: none;">Add New &nbsp;<i class="fa fa-plus"></i></a>
        </div><!-- /.panel-heading -->

        <div class="panel-body no-padding">
          <div class="panel panel-default shadow no-margin">
            <div class="panel-body">
              <div class="col-sm-7  pull-right">
                <a href="{{ route('filter', 1) }}" class="btn btn-success">Registered</a>
                <a href="{{ route('filter', 2) }}" class="btn btn-info">Unregistered</a>
              </div>
              <br><br>
              <div class="col-sm-7  pull-right">
                <select id="members-title" class="chosen-select" style="display: none;">
                  @foreach($members_title = App\Member::all() as $title)
                  <option value=""></option>
                  <option value="{{ $title->Constituent_ID }}">{{ $title->title.' '.$title->First_Name.' '.$title->Middle_Name.' '.$title->Last_Name }} <div class="pull-right">
                    ({{ $title->Constituent_ID }})
                  </div> </option>
                  @endforeach
                </select>
              </div>
              <div class="tab-content">
                <div class="tab-pane fade in active" id="tab-setting-countries">
                  <table class="table table-default table-bordered">
                    <thead>
                      <tr>
                        <th data-class="expand" class="text-center" style="width: 90px;">Registration No.</th>
                        <th data-hide="phone">Full Name</th>
                        <th>Date</th>
                        <th style="width: 90px;" data-hide="registered">Registration Status</th>
                        <th class="text-center" style="width: 100px; min-width: 100px;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($members as $member)
                      <tr>
                        <td class="text-center">{{ $member->Constituent_ID }}</td>
                        <td>{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name }}</td>
                        <td>@if (App\EventRegistrant::where('member_id', $member->id)->first() == null)  <p class="text-info">{{ '-' }}</p> @else <p class="text-success">{{ App\EventRegistrant::where('member_id', $member->id)->first()->created_at }}  </p> @endif</td>
                        <td>@if (App\EventRegistrant::where('member_id', $member->id)->first() == null)  <p class="text-info">{{ 'Unregistered' }}</p> @else <p class="text-success">{{ 'Registered' }} <i class="fa fa-check"></i> </p> @endif</td>
                        <td class="text-center">
                          @if (App\EventRegistrant::where('member_id', $member->id)->first() == null) <a href="#" class="btn btn-success btnPrintBadge" data-class="{{ $member->Constituent_ID }}" data-placement="top" data-title="Detail"><i class="fa fa-user"></i> <i class="fa fa-arrow-right"></i> <i class="fa fa-list"></i> </a> @else @endif
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th data-class="expand">Member Code</th>
                        <th data-hide="phone">Name</th>
                        <th>Date</th>
                        <th data-hide="registered">Registration Status</th>
                        <th class="text-center" style="width: 150px; min-width: 150px;">Action</th>
                      </tr>
                    </tfoot>
                  </table>
                  {{ $members->links() }}
                </div>
              </div>
            </div><!-- /.panel-body -->
          </div><!-- /.panel -->
        </div><!-- /.panel-body -->
      </div><!-- /.panel -->
    </div>
  </div>
</div>
<!-- START @BACK TOP -->
<div id="back-top" class="animated pulse circle">
  <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->
<div class="modal fade bs-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Confirm Member Event Registration</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
        <form class="attendee-form" action="#" method="post">
          @csrf
          <div class="col-md-12">
            <input type="hidden" name="member_code" id="mem-code" value="">
            <div class="row">
              <div class="col-md-4">
                <span class="fa fa-user btn btn-success" style="font-size: 30px;"></span>
              </div>
              <div class="col-md-4">
                <img src="{{ asset('img/LivelyAngryGarpike-size_restricted.gif') }}" style="height: 50px;" alt="">
              </div>
              <div class="col-md-4">
                <span class="fa fa-list btn btn-success" style="font-size: 30px;"></span>
              </div>
            </div>
          </div>
        </div>
        <br><br>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-theme">Procceed</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg1" id="exampleModalCenter" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Saving Event Registration</h5>
      </div>
      <div class="modal-body text-center">
        <div class="col-md-12">
          <input type="hidden" name="member_code" id="mem-code" value="">
          <div class="row">
            <div class="progress progress-striped active">
              <div class="progress-bar progress-bar-success hidden-ie" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                <span class="sr-only">65% Complete (success)</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br><br>
    </div>
  </div>
</div>
<div class="modal fade bs-example-modal-lg12" id="exampleModalCenter" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Status</h5>
      </div>
      <div class="modal-body text-center">
        <div class="col-md-12">
          <h2>Saved!</h2>
        </div>
      </div>
      <br><br>
      <div class="modal-footer">
        <a href="/register-attendee"><button type="" class="btn btn-theme">Ok</button></a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('.btnPrintBadge').on('click', function(e){
    e.preventDefault();
    var $data = $(this).data('class');
    console.log($data);
    $.ajax(
      {
        url: 'view-member-badge/'+$data,
        type: 'get',
        success: function(response){
          if (response['data'] === 'success') {
            console.log(response['response']['Constituent_ID']);
            $('#modal-header').html(response['response']['First_Name']+ ' ' +response['response']['Middle_Name']+ ' ' +response['response']['Last_Name']);
            $('#mem-code').val(response['response']['id']);
            $('.bs-example-modal-lg').modal('show');
          }else {
            console.log('error');
            console.log(response);
          }
        }
      }
    )
  });
});
</script>

<!-- START GOOGLE ANALYTICS -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55892530-1', 'auto');
ga('send', 'pageview');

</script>
<!--/ END GOOGLE ANALYTICS -->
<script type="text/javascript">
$(document).ready(function(e){
  $('#qrcode').qrcode({width: 45,height: 45,text: "91000"});
  $('#qrcode2').qrcode({width: 65,height: 65,text: "2333, Edward, Amponsah, Mr, Member, Western, Community, Industry, EDDYAMPS PHARMACY LTD, 024444444"});
});

</script>
<script type="text/javascript">
$(document).ready(function(){
  $('.attendee-form').on('submit', function (e){
    e.preventDefault();
    var $mem_id = $('#mem-code').val();
    console.log($mem_id);
    $('.bs-example-modal-lg').modal('hide');
    $('.bs-example-modal-lg1').modal('show');
    $.ajax({
      url: 'register-event-attendee',
      type: 'POST',
      data: new FormData(this),
      cache: false,
      contentType: false,
      processData: false,
      success: function(response){
        console.log(response);
        if (response === 'saved') {
          $('.bs-example-modal-lg1').modal('hide');
          $('.bs-example-modal-lg12').modal('show');
        }else{
          console.log(response);
        }
      }
    })
  });
});
</script>
<script type="text/javascript">
$('#btnPrint').on('click', function(e){
  print($('.portrait'));
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#members-title').on('change', function(e){
    var $constituent_id = $(this).val();
    $.ajax(
      {
        url:"fetch-member-registration/"+$constituent_id,
        type:"get",
        success:function(response){
          $('#tab-setting-countries').fadeIn('slow').html(response);
        }
      }
    )
  });
});
</script>


@endsection
