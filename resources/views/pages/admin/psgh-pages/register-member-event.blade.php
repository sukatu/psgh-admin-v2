    <table class="table table-default table-bordered">
      <thead>
        <tr>
          <th data-class="expand" class="text-center" style="width: 90px;">Registration No.</th>
          <th data-hide="phone">Full Name</th>
          <th style="width: 90px;" data-hide="registered">Registration Status</th>
          <th class="text-center" style="width: 100px; min-width: 100px;">Action</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="text-center">{{ $member->Constituent_ID }}</td>
          <td>{{ $member->First_Name.' '.$member->Middle_Name.' '.$member->Last_Name }}</td>
          <td>@if (App\EventRegistrant::where('member_id', $member->id)->first() == null)  <p class="text-info">{{ 'Unregistered' }}</p> @else <p class="text-success">{{ 'Registered' }} <i class="fa fa-check"></i> </p> @endif</td>
          <td class="text-center">
            @if (App\EventRegistrant::where('member_id', $member->id)->first() == null) <a href="#" class="btn btn-success btnPrintBadge" data-class="{{ $member->Constituent_ID }}" data-placement="top" data-title="Detail"><i class="fa fa-user"></i> <i class="fa fa-arrow-right"></i> <i class="fa fa-list"></i> </a> @else @endif
          </td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <th data-class="expand">Member Code</th>
          <th data-hide="phone">Name</th>
          <th data-hide="registered">Registration Status</th>
          <th class="text-center" style="width: 150px; min-width: 150px;">Action</th>
        </tr>
      </tfoot>
    </table>
    <script type="text/javascript">
    $(document).ready(function(){
      $('.btnPrintBadge').on('click', function(e){
        e.preventDefault();
        var $data = $(this).data('class');
        console.log($data);
        $.ajax(
          {
            url: 'view-member-badge/'+$data,
            type: 'get',
            success: function(response){
              if (response['data'] === 'success') {
                console.log(response['response']['Constituent_ID']);
                $('#modal-header').html(response['response']['First_Name']+ ' ' +response['response']['Middle_Name']+ ' ' +response['response']['Last_Name']);
                $('#mem-code').val(response['response']['id']);
                $('.bs-example-modal-lg').modal('show');
              }else {
                console.log('error');
                console.log(response);
              }
            }
          }
        )
      });
    });
    </script>
