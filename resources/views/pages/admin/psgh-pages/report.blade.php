@extends('layouts.gen')
@section('content')
<style media="screen">

.portrait{
  border: solid;
  border-color: black;
  border-width: thin;
  padding-left: 10px;
  padding-right: 25px!important;
  background-color: #ffff;
  width: 350px;
}
.event-title{
  font-size: 22px;
  text-align: center;
  margin-left: 13px;
}
#qrcode{
  margin-left: 10px;
}
#qrcode2{

}
.top{
  margin-top: 10px;
}
.sponsor{
  background: yellow;
  width: 70px;
  border: solid;
  border-color: black;
  padding-left: 10px;
  text-align: center;
  margin-left: -10px;
}
</style>
<button class="btn btn-theme" data-toggle="modal" data-target=".bs-example-modal-lg">Large modal</button>
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Badge Reporting for <i id="modal-header"></i> </h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="col-md-6">
            <div id="portrait" class="portrait">
              <div class="row top">
                <div class="col-md-2">
                  <img src="img/logo (2).png" width="60" alt="">
                </div>
                <div class="col-md-8 text-center">
                  <strong>  <h4 class="event-title">PSGH<i class="text-danger">AGM2019</i><sub>ACCRA</sub></h4></strong>
                </div>
                <div class="col-md-2">
                      <div id="qrcode"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                <strong><h5 class="text-center">31st JULY - 4th AUG</h5></strong>
                </div>
              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                  <h2 class="text-center">EDWARD</h2>
                  <h2 class="text-center"> AMPONSAH(MR)</h2>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                  <h4 class="text-center">2333</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                  <h4 class="text-center">MEMBER</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                  <h4 class="text-center">WESTERN</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                  <h3 class="text-center">Community, Industry</h3>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                  <h4 class="text-center">EDDYAMPS PHARMACY LTD.</h4>
                  </strong>
                </div>

              </div>

              <div class="row">

                <div class="col-md-12 pull-left">
                  <div class="col-md-2">
                    <div id="qrcode2"></div>
                  </div>
                  <div class="col-md-2 pull-right">
                    <div class="sponsor">
                  <strong>
                      <p>Denk
                        Pharma</p>
                      </strong>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-theme"> <i class="fa fa-print"></i> Print</button>
        </div>
      </div>
    </div>
  </div>
</div>
  @endsection
