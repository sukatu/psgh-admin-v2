
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- START @HEAD -->
<head>
  <!-- START @META SECTION -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>PSGH | Admin</title>  <!--/ END META SECTION -->

  <!-- START @FAVICONS -->
  <link href="../../../img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="../../../img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="../../../img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="../../../img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
  <link href="../../../img/ico/html/apple-touch-icon.png" rel="shortcut icon">
  <!--/ END FAVICONS -->

  <!-- START @FONT STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
  <!--/ END FONT STYLES -->

  <!-- START @GLOBAL MANDATORY STYLES -->
  <link href="../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!--/ END GLOBAL MANDATORY STYLES -->

  <!-- START @PAGE LEVEL STYLES -->
  <link href="../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/datatables/css/datatables.responsive.css" rel="stylesheet">
  <!--/ END PAGE LEVEL STYLES -->

  <!-- START @THEME STYLES -->
  <link href="../../../assets/admin/css/reset.css" rel="stylesheet">
  <link href="../../../assets/admin/css/layout.css" rel="stylesheet">
  <link href="../../../assets/admin/css/components.css" rel="stylesheet">
  <link href="../../../assets/admin/css/plugins.css" rel="stylesheet">
  <link href="../../../assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
  <link href="../../../assets/admin/css/custom.css" rel="stylesheet">
  <!--/ END THEME STYLES -->

  <!-- START @IE SUPPORT -->
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="../../../assets/global/plugins/bower_components/html5shiv/dist/html5shiv.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/respond-minmax/dest/respond.min.js"></script>
  <![endif]-->
  <!--/ END IE SUPPORT -->
  <style media="screen">
  @media all {

    .portrait{
      border: solid;
      border-color: black;
      border-width: thin;
      padding-left: 10px;
      padding-right: 25px!important;
      background-color: #ffff;
      width: 350px;
    }
    .event-title{
      font-size: 22px;
      text-align: center;
      margin-left: 13px;
    }
    #qrcode{
      margin-left: 10px;
    }
    #qrcode2{

    }
    .top{
      margin-top: 10px;
    }
    .sponsor{
      background: yellow;
      width: 70px;
      border: solid;
      border-color: black;
      padding-left: 10px;
      text-align: center;
      margin-left: -10px;
    }
  }
  </style>
</head>
<body class="page-sound page-header-fixed page-sidebar-fixed page-footer-fixed">

  <section id="wrapper">
    @include('layouts.header')
    @include('layouts.aside')

    <!-- START @PAGE CONTENT -->
    <section id="page-content">

      <!-- Start page header -->
      <div class="header-content">
        <h2>Add User</h2>

      </div>
      <div class="body-content animated fadeIn">

        <div class="row">
          <div class="col-md-12">
            @if(session('success'))
            <div class="alert alert-success">
              <div class="">
                {{ session('success') }}
              </div>
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger">
              <div class="">
                {{ session('error') }}
              </div>
            </div>
            @endif
            <div class="panel panel-tab panel-tab-double shadow">
              <!-- Start tabs heading -->
              <div class="panel-heading no-padding">
                <ul class="nav nav-tabs">
                  <li class="active nav-border nav-border-top-success">
                    <a href="additional-option-system-manage-metadata.html#tab-setting-countries" data-toggle="tab">
                      <i class="icon-globe icons fg-success"></i>
                      <div>
                        <span class="text-strong">Roles and Permissions</span>
                      </div>
                    </a>
                  </li>

                </ul>
              </div><!-- /.panel-heading -->

              <div class="panel-body no-padding">
                <div class="panel panel-default shadow no-margin">
                  <div class="panel-body">
                    <div class="tab-content">
                      <div class="tab-pane fade in active" id="tab-setting-countries">
                        <button type="button" class="btn btn-primary pull-right" style="margin-right: auto;" name="button"><i class="demo-plus"></i>Add New Role</button>
                      <form class="" action="update-permission" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Role: </label>
                              <select class="selectpicker" id="role" name="user_type" data-live-search="true" data-width="100%">
                                <option>-- Select --</option>
                                <option value="super_admin">Super Admin</option>
                                <option value="admin">Admin</option>
                                <option value="guest">Guest</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="container" id="fetch_permissions">

                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div><!-- /.panel-body -->
            </div><!-- /.panel -->
          </div><!-- /.panel-body -->
        </div><!-- /.panel -->
      </div>
    </div>
  </div>
  <footer class="footer-content">
    2014 - <span id="copyright-year"></span> &copy; Blankon Admin. Created by <a href="http://djavaui.com/" target="_blank">Djava UI</a>, Yogyakarta ID
    <span class="pull-right">0.01 GB(0%) of 15 GB used</span>
  </footer>
</section>
</section>

<!-- START @BACK TOP -->
<div id="back-top" class="animated pulse circle">
  <i class="fa fa-angle-up"></i>
</div><!-- /#back-top -->
<!--/ END BACK TOP -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">

    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Badge Reporting for <i id="modal-header"></i> </h4>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
          <div class="col-md-6">
            <div id="portrait" class="portrait">
              <div class="row top">
                <div class="col-md-2">
                  <img src="img/logo (2).png" width="60" alt="">
                </div>
                <div class="col-md-8 text-center">
                  <strong>  <h4 class="event-title">PSGH<i class="text-danger">AGM2019</i><sub>ACCRA</sub></h4></strong>
                </div>
                <div class="col-md-2">
                  <div id="qrcode"></div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <strong><h5 class="text-center">13th - 18th AUG</h5></strong>
                </div>
              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h2 class="text-center" id="first_name">DORA</h2>
                    <h2 class="text-center" id="middle_name">ERBYNN</h2>
                    <h2 class="text-center" id="last_name"></h2>
                    <h3 class="text-center" id="title">(MRS)</h3>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">2333</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">MEMBER</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">WESTERN</h4>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h3 class="text-center">Community, Industry</h3>
                  </strong>
                </div>

              </div>
              <div class="row">

                <div class="col-md-12">
                  <strong>
                    <h4 class="text-center">EDDYAMPS PHARMACY LTD.</h4>
                  </strong>
                </div>

              </div>

              <div class="row">

                <div class="col-md-12 pull-left">
                  <div class="col-md-2">
                    <div id="qrcode2"></div>
                  </div>
                  <div class="col-md-2 pull-right">
                    <div class="sponsor">
                      <strong>
                        <p>Denk
                          Pharma</p>
                        </strong>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <button type="button" id="btnPrint" name="button">Print</button>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-theme"> <i class="fa fa-print"></i> Print</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade modal-filder-badge" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Filter Badge Report </h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <form class="" action="filter-badge-reporting" method="get">
              <select class="form-control" name="value" required>
                <option value="">Select</option>
                <option value="NEW MEMBER">NEW MEMBER</option>
                <option value="MEMBER">MEMBER</option>
                <option value="FELLOW">FELLOW</option>
                <option value="DECEASED">DECEASED</option>
              </select>
            </div>
            <br><br>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-theme"> <i class="fa fa-print"></i> Print Preview</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--/ END ADDITIONAL ELEMENT -->

  <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
  <!-- START @CORE PLUGINS -->
  <script src="../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
  <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
  <script src="../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
  <script src="../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>
  <!--/ END CORE PLUGINS -->

  <!-- START @PAGE LEVEL PLUGINS -->
  <script src="../../../assets/global/plugins/bower_components/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/jquery.dataTables.min.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/dataTables.bootstrap.js"></script>
  <script src="../../../assets/global/plugins/bower_components/datatables/js/datatables.responsive.js"></script>
  <!--/ END PAGE LEVEL PLUGINS -->

  <!-- START @PAGE LEVEL SCRIPTS -->
  <script src="../../../assets/admin/js/apps.js"></script>
  <script src="../../../assets/admin/js/pages/investor/blankon.investor.additional.option.system.metadata.js"></script>
  <script src="../../../assets/admin/js/demo.js"></script>
  <script type="text/javascript" src="js/jquery.qrcode.min.js"></script>

  <!--/ END PAGE LEVEL SCRIPTS -->
  <!--/ END JAVASCRIPT SECTION -->
  <script type="text/javascript">
  $(document).ready(function(){
    $('.btnClickFilter').on('click', function(){
      $('.modal-filder-badge').modal('show');
    });
    $('#datatable-setting-countries').on('click', '.btnPrintBadge', function(e){
      e.preventDefault();
      var $data = $(this).data('class');
      console.log($data);
      $.ajax(
        {
          url: 'view-member-badge/'+$data,
          type: 'get',
          success: function(response){
            if (response['data'] === 'success') {
              console.log(response['response']['Constituent_ID']);
              $('#modal-header').html(response['response']['First_Name']+ ' ' +response['response']['Middle_Name']+ ' ' +response['response']['Last_Name']);
              $('#mem-code').html(response['response']['Constituent_ID']);
              $('#first_name').html(response['response']['First_Name']);
              $('.bs-example-modal-lg').modal('show');
            }else {
              console.log('error');
              console.log(response);
            }
          }
        }
      )
    });
  });
  </script>

  <!-- START GOOGLE ANALYTICS -->
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55892530-1', 'auto');
  ga('send', 'pageview');

  </script>
  <!--/ END GOOGLE ANALYTICS -->
  <script type="text/javascript">
  $(document).ready(function(e){
    $('#qrcode').qrcode({width: 45,height: 45,text: "91000"});
    $('#qrcode2').qrcode({width: 65,height: 65,text: "2333, Edward, Amponsah, Mr, Member, Western, Community, Industry, EDDYAMPS PHARMACY LTD, 024444444"});
  });

  </script>
  <script type="text/javascript">
  $('#btnPrint').on('click', function(e){
    print($('.portrait'));
  });
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#role').on('change', function(e){
    var role_name = this.value;
    $.ajax({
      url: 'fetch-permissions/'+role_name,
      type: 'GET',
      success: function(response){
        $('#fetch_permissions').fadeOut();
        $('#fetch_permissions').fadeIn('slow').html(response);
      }
    })
  });
});
</script>
</body>
<!--/ END BODY -->

</html>
