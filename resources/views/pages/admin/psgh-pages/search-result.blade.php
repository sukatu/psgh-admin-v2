@extends('layouts.tables')
@section('content')
                <!-- Start body content -->
                <div class="body-content animated fadeIn">

                    <div class="row">
                        <div class="col-md-12">
                            <form action="#" class="search-basic-form">
                                <div class="input-group">
                                    <div class="input-cont">
                                        <input placeholder="Search..." class="form-control" type="text">
                                    </div>
												<span class="input-group-btn">
												<button type="button" class="btn">
                                                    <i class="fa fa-search"></i>
                                                </button>
												</span>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div id="js-filters-tabs" class="cbp-l-filters-big">
                                <div data-filter=".web" class="cbp-filter-item-active cbp-filter-item">
                                    Search Result
                                </div>
                            </div>
                            <div id="js-grid-tabs" class="cbp cbp-l-grid-tabs">
                                <div class="cbp-item web">
                                    <div class="search-basic-list">
                                        <h4>
                                            <a href="http://djavaui.com" target="_blank">We are Djava UI team</a>
                                        </h4>
                                        <a href="http://djavaui.com" target="_blank">http://djavaui.com</a>
                                        <p>
                                            Welcome to Djava UI, We build clean and responsive website.  A team of Indonesian website developers, very nice to be able to share our work with you.
                                        </p>
                                    </div>
                                    
                                    <ul class="pagination">
                                        <li>
                                            <a href="javascript:void(0)">Prev </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                1 </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                2 </a>
                                        </li>
                                        <li class="active">
                                            <a href="javascript:void(0)">
                                                3 </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                4 </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                5 </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                Next </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="cbp-item web-image">
                                    <div class="search-basic-list" style="margin-top: 0">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="http://djavaui.com">
                                                    <img data-no-retina class="media-object" src="../../../img/media/realistic/1.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="http://djavaui.com" target="_blank">We are Djava UI team</a>
                                                </h4>
                                                <a href="http://djavaui.com" target="_blank">http://djavaui.com</a>
                                                <p>
                                                    Welcome to Djava UI, We build clean and responsive website.  A team of Indonesian website developers, very nice to be able to share our work with you.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-basic-list">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="../../../production/admin/html/dashboard.html">
                                                    <img data-no-retina class="media-object" src="../../../img/media/realistic/2.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="../../../production/admin/html/dashboard.html" target="_blank">Blankon - Fullpack admin theme </a>
                                                </h4>
                                                <a href="../../../production/admin/html/dashboard.html" target="_blank">http://themes.djavaui.com/blankon-fullpack-admin-theme/production/admin/html/dashboard.html </a>
                                                <p>
                                                    Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-basic-list">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="../../../production/admin/html/blog-grid-type-2.html">
                                                    <img data-no-retina class="media-object" src="../../../img/media/realistic/3.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="../../../production/admin/html/blog-grid-type-2.html" target="_blank">Nice blog grid</a>
                                                </h4>
                                                <a href="../../../production/admin/html/blog-grid-type-2.html" target="_blank">http://themes.djavaui.com/blankon-fullpack-admin-theme/production/admin/html/blog-grid-type-2.html</a>
                                                <p>
                                                    You can use any type of HTML content: text, images, videos, just choose what is best for you. It plays nice with your existing HTML and CSS, making it a great choice for dynamic and responsive layouts. It's perfect for portfolios, galleries, team members, blog posts or any other ordered content.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-basic-list">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="../../../production/admin/html/project-team.html">
                                                    <img data-no-retina class="media-object" src="../../../img/media/realistic/4.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="../../../production/admin/html/project-team.html" target="_blank">Meet a our team</a>
                                                </h4>
                                                <a href="../../../production/admin/html/project-team.html" target="_blank">http://themes.djavaui.com/blankon-fullpack-admin-theme/production/admin/html/project-team.html</a>
                                                <p>
                                                    You can customize everything from grid layout, lightbox style to every possible animation. With just a little HTML, CSS ans JS you can customize the plugin to fulfill your needs. Thumbs Up: if you click on the items below a full screen modal popup (singlePage) will open.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-basic-list">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="../../../production/admin/html/project-portfolio.html">
                                                    <img data-no-retina class="media-object" src="../../../img/media/realistic/5.jpg" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">
                                                    <a href="../../../production/admin/html/project-portfolio.html" target="_blank">Handle your project on here</a>
                                                </h4>
                                                <a href="../../../production/admin/html/project-portfolio.html" target="_blank">http://themes.djavaui.com/blankon-fullpack-admin-theme/production/admin/html/project-team.html</a>
                                                <p>
                                                    Project portfolio has built in lightbox and full screen modal popup(singlePage) to show any content you want. Check it out by clicking on `more info` or `view larger` buttons when you hover over the items below. Don't forget that you have a lot of options and presets on the side left of the screen to customize your plugin experience.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="pagination">
                                        <li>
                                            <a href="javascript:void(0)">Prev </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                1 </a>
                                        </li>
                                        <li class="active">
                                            <a href="javascript:void(0)">
                                                2 </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                3 </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                4 </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                5 </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)">
                                                Next </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- /.body-content -->
                <!--/ End body content -->
@endsection
