<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>PSGH | Admin</title>
    <link href="../../../img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="../../../img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="../../../img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="../../../img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
  <link href="../../../img/ico/html/apple-touch-icon.png" rel="shortcut icon">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../../assets/admin/css/reset.css" rel="stylesheet">
  <link href="../../../assets/admin/css/layout.css" rel="stylesheet">
  <link href="../../../assets/admin/css/components.css" rel="stylesheet">
  <link href="../../../assets/admin/css/plugins.css" rel="stylesheet">
  <link href="../../../assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
  <link href="../../../assets/admin/css/custom.css" rel="stylesheet">
</head>
<body class="page-header-fixed page-sidebar-fixed">
  <section id="wrapper">
    @include('layouts.header')
    @include('layouts.aside')
    <section id="page-content">
      <div class="body-content animated fadeIn">

          @if(session('success'))
          <div class="alert alert-success">
            {{ session('success') }}
          </div>
          @elseif(session('error'))
          <div class="alert alert-danger">
            {{ session('success') }}
          </div>
          @endif
        <div class="row">
          <div class="col-md-12">
            <div class="panel rounded shadow">
              <div class="panel-heading">
                <div class="pull-left">
                  <h3 class="panel-title">Basic Information</h3>
                </div><!-- /.pull-left -->
                <div class="pull-right">
                  <button class="btn btn-sm" data-action="collapse" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Collapse"><i class="fa fa-angle-up"></i></button>
                  <button class="btn btn-sm" data-action="remove" data-container="body" data-toggle="tooltip" data-placement="top" data-title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /.pull-right -->
                <div class="clearfix"></div>
              </div>
              <div class="panel-body no-padding">
          <form class="form-horizontal form-bordered" method="POST" action="validateSessionForm" role="form" id="">
            @if(count($errors))
              <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <br/>
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
              @csrf
              <div class="form-body">
                <div class="form">
                  <input type="hidden" name="form[]" value="">
                  <legend class="text-center">Session Form</legend>
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Event Title <span class="asterisk">*</span></label>
                  <div class="col-sm-7">
                    <select class="form-control" name="event_id[]">
                    @foreach($events = App\Event::all() as $event)
                    <option value="{{ $event->id }}">{{ $event->name}}</option>
                    @endforeach
                    </select>
                  </div>
                </div><!-- /.form-group -->

                <div class="form-group {{ $errors->has('session_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Session Name</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('session_name') }}" class="form-control input-sm" name="session_name[]" placeholder="Session Name" required>
                  </div>
                  <span class="text-danger">{{ $errors->first('session_name') }}</span>
                </div><!-- /.form-group -->
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Start Date</label>
                  <div class="col-sm-5">
                    <input type="date" name="start_date[]" value="" class="form-control" placeholder="Start Date" required>
                  </div>
                  <div class="col-sm-2">
                    <input type="time" name="start_time[]" class="form-control" value="" placeholder="Start Time" required>

                  </div>
                </div>
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">End Date</label>
                  <div class="col-sm-5">
                    <input type="date" name="end_date[]" value="" class="form-control" placeholder="End Date" required>
                  </div>
                  <div class="col-sm-2">
                    <input type="time" name="end_time[]" class="form-control" value="" placeholder="End Time" required>

                  </div>
                </div>
                <div class="form-group {{ $errors->has('session_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label">Presenter</label>
                  <div class="col-sm-7">
                    <input type="text" value="{{ old('presenter') }}" class="form-control input-sm" name="presenter[]" placeholder="Presenter" required>
                  </div>
                  <span class="text-danger">{{ $errors->first('presenter') }}</span>
                </div><!-- /.form-group -->

              </div>
                <div class="form-group {{ $errors->has('session_name') ? 'has-error' : '' }}">
                  <label class="col-sm-3 control-label"></label>
                  <div class="col-sm-7">
                    <button type="button" class="btn btn-success addForm" name="button"> <i class="fa fa-plus"></i> Add New  </button>
                  </div>
                </div><!-- /.form-group -->

                <div class="form-group pull-right">
                  <div class="col-sm-7">
                  <button type="submit" class="btn btn-primary" name="button">Save</button>
                </div>
                </div>
              </div>
            </div><!-- /.panel-body -->
          </form>
          </div><!-- /.panel -->
          <!--/ End basic validation -->
        </div>
      </div><!-- /.row -->

  </div>
  <footer class="footer-content">
    2014 - <span id="copyright-year"></span> &copy; Blankon Admin. Created by <a href="http://djavaui.com/" target="_blank">Djava UI</a>, Yogyakarta ID
    <span class="pull-right">0.01 GB(0%) of 15 GB used</span>
  </footer><!-- /.footer-content -->
  <!--/ End footer content -->

</section>
</section>
<div id="back-top" class="animated pulse circle">
  <i class="fa fa-angle-up"></i>
</div>
<script src="../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
<script src="../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
<script src="../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
<script src="../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>
<!--/ END CORE PLUGINS -->

<!-- START @PAGE LEVEL PLUGINS -->
<script src="../../../assets/global/plugins/bower_components/chosen_v1.2.0/chosen.jquery.min.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-mockjax/jquery.mockjax.js"></script>
<script src="../../../assets/global/plugins/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<!--/ END PAGE LEVEL PLUGINS -->

<!-- START @PAGE LEVEL SCRIPTS -->
<script src="../../../assets/admin/js/apps.js"></script>
<script src="../../../assets/admin/js/pages/blankon.form.validation.js"></script>
<script src="../../../assets/admin/js/demo.js"></script>
<!--/ END PAGE LEVEL SCRIPTS -->
<!--/ END JAVASCRIPT SECTION -->

<!-- START GOOGLE ANALYTICS -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-55892530-1', 'auto');
ga('send', 'pageview');

</script>
<!--/ END GOOGLE ANALYTICS -->

<script>
//Add Input Fields
$(document).ready(function() {
  var max_fields = 10; //Maximum allowed input fields
  var wrapper    = $(".wrapper"); //Input fields wrapper
  var add_button = $(".add_fields"); //Add button class or ID
  var x = 1; //Initial input field is set to 1

  //When user click on add input button
  $(add_button).click(function(e){
    e.preventDefault();
    //Check maximum allowed input fields
    if(x < max_fields){
      x++; //input field increment
      //add input field
      $(wrapper).append('<div><label class="col-sm-3 control-label"></label><div class="col-sm-7"> <input type="text" class="form-control" name="school_attended_with_years[]" placeholder="School Attended with Years" value=""> <a href="javascript:void(0);" class="remove_field text-danger">Remove</a><br></div></div>');
    }
  });

  //when user click on remove button
  $(wrapper).on("click",".remove_field", function(e){
    e.preventDefault();
    $(this).parent('div').remove(); //remove inout field
    x--; //inout field decrement
  })
});
</script>
<script type="text/javascript">
$('.addForm').click(function(){
  var currentCount =  $('.form').length;
  var newCount = currentCount+1;
  var lastRepeatingGroup = $('.form').last();
  var newSection = lastRepeatingGroup.clone();
  newSection.insertAfter(lastRepeatingGroup);
  newSection.find("input").each(function (index, input) {
      input.id = input.id.replace("_" + currentCount, "_" + newCount);
      input.name = input.name.replace("_" + currentCount, "_" + newCount);
  });
  newSection.find("label").each(function (index, label) {
      var l = $(label);
      l.attr('for', l.attr('for').replace("_" + currentCount, "_" + newCount));
  });
  return false;
});

</script>
</body>
<!--/ END BODY -->

</html>
