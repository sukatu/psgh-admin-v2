<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- START @HEAD -->
<head>
  <!-- START @META SECTION -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Blankon is a theme fullpack admin template powered by Twitter bootstrap 3 front-end framework. Included are multiple example pages, elements styles, and javascript widgets to get your project started.">
  <meta name="keywords" content="admin, admin template, bootstrap3, clean, fontawesome4, good documentation, lightweight admin, responsive dashboard, webapp">
  <meta name="author" content="Djava UI">
  <title>PSGH | Admin</title>
  <!--/ END META SECTION -->

  <!-- START @FAVICONS -->
  <link href="../../../img/ico/html/apple-touch-icon-144x144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
  <link href="../../../img/ico/html/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
  <link href="../../../img/ico/html/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
  <link href="../../../img/ico/html/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed">
  <link href="../../../img/ico/html/apple-touch-icon.png" rel="shortcut icon">
  <!--/ END FAVICONS -->

  <!-- START @FONT STYLES -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Oswald:700,400" rel="stylesheet">
  <!--/ END FONT STYLES -->

  <!-- START @GLOBAL MANDATORY STYLES -->
  <link href="../../../assets/global/plugins/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!--/ END GLOBAL MANDATORY STYLES -->

  <!-- START @PAGE LEVEL STYLES -->
  <link href="../../../assets/global/plugins/bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../../assets/global/plugins/bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css" rel="stylesheet">
  <!--/ END PAGE LEVEL STYLES -->

  <!-- START @THEME STYLES -->
  <link href="../../../assets/admin/css/reset.css" rel="stylesheet">
  <link href="../../../assets/admin/css/layout.css" rel="stylesheet">
  <link href="../../../assets/admin/css/components.css" rel="stylesheet">
  <link href="../../../assets/admin/css/plugins.css" rel="stylesheet">
  <link href="../../../assets/admin/css/themes/default.theme.css" rel="stylesheet" id="theme">
  <link href="../../../assets/admin/css/custom.css" rel="stylesheet">
</head>
<style media="screen">
@media print {
  #printNot{
    display: none;
  }
  a[href]:after {
   content: " (" attr(href) ")";
 }
  body {
    margin: 0;
    color: #000;
    background-color: #fff;
  }
  .sponsor{
    background: yellow;
  }
  .page-link{
    display: none;
  }
}
a, .pagination > .active > span {
    background-color: #089241;
    border: 1px solid #089241;
}
.pagination > li > a {
    color: #089241;
}

</style>
<body>

  <section>
    <section>
      <div class="body-content animated fadeIn">
        <div class="col-md-12" style="">
          <div class="col-md-6 pull-left " style="border: solid; border-color: black; width: 7cm; height: 10cm; border-width: thin; margin-left: 10px; border-bottom-width: 10px; border-bottom-color: grey; border-top-width: 10px; border-top-color: grey;">
            <div class="row">
              <div class="col-md-2 pull-left">
                <img src="img/logo (2).png"  width="60"  style="margin-top: 10px;" alt="">
              </div>
              <div class="col-md-8 pull-left">
                <strong><h5 class="event-title" style="font-size: 15px; margin-left: -13px;">PSGH<i class="text-danger">AGM2019</i></h5></strong>
                <div class="text-center">
                  <p>Accra</p>
                </div>
              </div>

              <div class="col-md-2 pull-right" style="margin-top: -62px;">
                <div class="" style=" filter: brightness(100%);"> {!! QrCode::size(60, 35)->generate($member->Constituent_ID); !!}</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <strong><h5 class="text-center" style="margin-top: 1px;">13th - 18th AUG</h5></strong>
              </div>
            </div>
            <div class="row">

              <div class="col-md-12" style="margin-top: -20px;">
                <strong>
                  <h2 class="text-center" style="font-size: 25px; font-weight: bold;" id="first_name">{{ $member->First_Name}}</h2>
                  <h2 class="text-center" style="font-size: 18px; margin-top: -9px;" id="middle_name">{{ $member->Middle_Name }}</h2>
                  <h2 class="text-center" style="font-size: 18px; margin-top: -9px;" id="last_name">{{ $member->Last_Name }}</h2>
                  <h3 class="text-center" style="font-size: 10px; margin-top: -9px;" id="title">({{ $member->Member_Name_Title}})</h3>
                </strong>
              </div>

            </div>
            <div class="row">

              <div class="col-md-12">
                <strong>
                  <h4 class="text-center" style="font-size: 14px; margin-top: -1px;">{{ $member->Constituent_ID }}</h4>
                </strong>
              </div>

            </div>
            <div class="row">

              <div class="col-md-12">
                <strong>
                  <h4 class="text-center" style="font-size: 20px; margin-top: -1px;">{{ $member->Member_Type_Code }}</h4>
                </strong>
              </div>

            </div>
            <div class="row">

              <div class="col-md-12">
                <strong>
                  <h4 class="text-center" style="font-size: 15px; margin-top: -9px;">{{ $member->Regional_Branch_Chapter }}</h4>
                </strong>
              </div>

            </div>
          
            <div class="row">
              <div class="col-md-12">
                <strong>
                  <h4 class="text-center" style="font-size: 14px; margin-top: -9px;">{{ $member->Employer_Name }}</h4>
                </strong>
              </div>

            </div>
            <div class="row">
              <div class="text-center" style="margin-bottom: 10px;">
                <img src="{{ asset('43246783-1ee2-49c1-8f78-bddda947e72a.jpeg') }}" width="70" style="position: static;" alt="">
              </div>
          </div>
        </div>
        </div>
      </section>

    </section><!-- /#wrapper -->


    <!-- START @BACK TOP -->
    <div id="back-top" class="animated pulse circle">
      <i class="fa fa-angle-up"></i>
    </div><!-- /#back-top -->
    <!--/ END BACK TOP -->

    <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
    <!-- START @CORE PLUGINS -->
    <script src="../../../assets/global/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../../../assets/global/plugins/bower_components/jquery-cookie/jquery.cookie.js"></script>
    <script src="../../../assets/global/plugins/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/handlebars.js"></script>
    <script src="../../../assets/global/plugins/bower_components/typehead.js/dist/typeahead.bundle.min.js"></script>
    <script src="../../../assets/global/plugins/bower_components/jquery-nicescroll/jquery.nicescroll.min.js"></script>
    <script src="../../../assets/global/plugins/bower_components/jquery.sparkline.min/index.js"></script>
    <script src="../../../assets/global/plugins/bower_components/jquery-easing-original/jquery.easing.1.3.min.js"></script>
    <script src="../../../assets/global/plugins/bower_components/ionsound/js/ion.sound.min.js"></script>
    <script src="../../../assets/global/plugins/bower_components/bootbox/bootbox.js"></script>
    <script src="../../../assets/global/plugins/bower_components/retina.js/dist/retina.min.js"></script>
    <!--/ END CORE PLUGINS -->

    <!-- START @PAGE LEVEL PLUGINS -->
    <script src="../../../assets/global/plugins/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
    <!--/ END PAGE LEVEL PLUGINS -->

    <!-- START @PAGE LEVEL SCRIPTS -->
    <script src="../../../assets/admin/js/apps.js"></script>
    <script src="../../../assets/admin/js/pages/blankon.ui.feature.bootstrap.tour.js"></script>
    <script src="../../../assets/admin/js/demo.js"></script>
    <!--/ END PAGE LEVEL SCRIPTS -->
    <!--/ END JAVASCRIPT SECTION -->
    <script type="text/javascript" src="js/jquery.qrcode.min.js"></script>

    <!-- START GOOGLE ANALYTICS -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-55892530-1', 'auto');
    ga('send', 'pageview');

  </script>
  <!--/ END GOOGLE ANALYTICS -->
  <script type="text/javascript">


</script>
<script type="text/javascript">
$('#btnPrint').on('click', function(e){
  print($('.portrait'));
});
</script>
</body>
<!--/ END BODY -->

</html>
