@extends('layouts.tables')
@section('content')
<div class="header-content">
  <h2><i class="fa fa-pencil"></i> Compose <span>mail compose sample</span></h2>
  <div class="breadcrumb-wrapper hidden-xs">
    <span class="label">You are here:</span>
    <ol class="breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{ url('/home') }}">Dashboard</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="{{ url()->previous() }}">Mail</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li class="active">Compose</li>
    </ol>
  </div>
</div>
<div class="body-content animated fadeIn">
  <div class="row compose-mail-wrapper">
    <div class="col-sm-12">

      <form class="form-horizontal sms-form" method="post" action="#">
        @csrf
        <div class="panel rounded shadow panel-danger">
          <div class="panel-heading">
            <div class="pull-left">
              <h3 class="panel-title">View message</h3>
            </div>
            <div class="pull-right">
              <form action="#" class="form-horizontal mr-5 mt-3">
                <div class="form-group no-margin no-padding has-feedback search-mail">
                  <button type="submit" class="btn btn-theme fa fa-search form-control-feedback"></button>
                </div>
              </form>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="panel-sub-heading inner-all">
            <div class="pull-left">
              <ul class="list-inline no-margin">
                <li>
                  <a href="{{ url()->previous() }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                </li>
                <!-- <li class="hidden-xs">
                <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save Draft</a>
              </li> -->
            </ul>
          </div>
          <div class="pull-right">
            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> Send SMS</button>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
          <div class="compose-mail">
            <div class="form-group">
              <label for="to" class="col-sm-2 control-label">To:</label>
              <div class="col-sm-10">
                <div class="input-group">
                  <select class="form-control @error('recipient') is-invalid @enderror" id="recipient" name="group-type" >
                    <option value="{{ old('recipient') }}">{{ old('recipient') }}</option>
                    <option value="sendToAllMemmbers">All Members</option>
                    <option value="Profile">Profile</option>
                  </select>
                  @error('recipient')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>
            </div>
            <div id="ccbcc" class="collapse">
              <div class="form-group">
                <label for="Cc" class="col-sm-2 control-label">Profile Category:</label>
                <div class="col-sm-10">
                  <select class="form-control" id="profile" name="profile">
                    <option value=""></option>
                    @foreach($profiles as $profile)
                    <option value="{{ $profile->id }}">{{ $profile->display_name }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="form-group profile-categories-section">

              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <textarea name="message" id="compose-editor" class="form-control @error('message') is-invalid @enderror" rows="10" placeholder="Write your content here...">{{ old('message') }}</textarea>
              @error('message')
              <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <!-- <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
          <label for="">Now:</label>
          <input type="radio" name="send-at" value="0">
          <input type="radio" name="send-at" id="schedule" value="1"> <span class="scheduleDateData">  </span> <span class="scheduleTimeData"></span>
          <input type=hidden name="schedule" class="schedule-input" value="">
          <input type=hidden name="scheduleTimeData" class="scheduleTimeData-input" value="">
          <input type=hidden name="scheduleDateData" class="scheduleDateData-input" value="">
        </div>
      </div> -->
    </div>
  </div>
  <!-- <div class="panel-footer">
  <div class="pull-right">
  <button class="btn btn-danger btn-sm">Cancel</button>
  <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-send"></i> Send SMS</button>
</div>
<div class="clearfix"></div>
</div> -->
@if(session('success'))
<div class="alert alert-success">
  {{ session('success') }}
</div>
@endif
</div>
</form>
</div>
</div>
</div>

@endsection
