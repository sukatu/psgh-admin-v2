<p>Dear <strong>{{ $first_name }} {{ $last_name}} ({{ $constituent_id }})</strong></p>

<p>Congratulations,</p>

<p>You have earned <strong>{{$session_point}}</strong> CPD Credits for attending the Event Session <strong>'{{$session_name}}'</strong> </p>

<p>We appreciate your registration, attendance and participation.</p>

<p>Continue to patronize all PSGH activities.</p>

<p>Thanks</p>
<p>Admin</p>
