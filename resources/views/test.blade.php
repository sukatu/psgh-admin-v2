<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
  <title>Tests</title>
  <link rel="stylesheet" href="swal/bower_components/qunit/qunit/qunit.css">
  <link rel="stylesheet" href="swal/dist/sweetalert.css">
</head>
<body>


  <script src="swal/dist/sweetalert.min.js"></script>

  <script src="swal/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="swal/bower_components/qunit/qunit/qunit.js"></script>

  <!-- For Travis CI -->
  <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  <script src="https://code.jquery.com/qunit/qunit-1.18.0.js"></script>

  <script src="swal/test/tests.js"></script>
  <script type="text/javascript">
    swal("Here's a message!")
  </script>
</body>
</html>
