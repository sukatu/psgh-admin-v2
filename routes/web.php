<?php
Auth::routes();

Route::get('/', function () {
  return view('pages.admin.psgh-pages.admin-signin');
})->name('signing-view');


Route::group(['middleware'=>['auth', 'UserType']], function() {
  Route::post('/validateRegistrationForm', 'MembersController@update')->name('validateRegistrationForm');
  Route::get('/home', 'HomeController@index')->name('home');
  Route::any('/dashboard', 'HomeController@dashboard')->name('dashboard');
  Route::get('/lost-password', 'AuthController@lost_password')->name('lost-password');
  Route::post('/confirm-password', 'AuthController@confirm_email')->name('confirm-email');
  Route::get('/verify-reset-link/{token}', 'AuthController@verify_token')->name('verify-token');
  Route::get('/reset-password', 'AuthController@reset_password')->name('reset-password');
  Route::post('password-update', 'AuthController@update_password')->name('password-update');
  Route::get('confirm-reset', 'HomeController@confirm_reset')->name('confirm-reset');
  Route::get('confirm-email-view', 'HomeController@confirm_email')->name('confirm_email');
  Route::get('/member-list', 'MembersController@index')->name('member-list');
  Route::get('fetch-member/{constituent_id}', 'MembersController@getMember');
  Route::get('/fetch-members', 'MembersController@fetch_ajax');
  Route::get('/member-details/{id}', 'MembersController@show')->name('member-details');
  Route::get('/edit-details/{id}', 'MembersController@edit')->name('edit-details');
  Route::get('delete-details/{id}', 'MembersController@destroy')->name('delete-details');
  Route::get('/member-registration', 'MembersController@register')->name('member-registration');
  Route::post('/validateRegistrationForm', 'MembersController@validateRegistrationForm')->name('validateRegistrationForm');
  Route::get('/badge-report', 'EventController@badgeIndex')->name('badge-report');
  Route::get('/view-member-badge/{id}', 'EventController@view_member_badge');
  Route::get('/report', 'EventController@report');
  Route::get('/badging', 'EventController@badgeGenerator')->name('badging');
  Route::get('/registrants', 'EventController@registrants')->name('registrants');
  Route::get('/attendees', 'EventController@attendees')->name('attendees');
  Route::get('/register-attendee', 'EventController@register_attendees')->name('register-attendee');
  Route::post('/register-event-attendee','EventController@register_event_attendee')->name('register-event-attendee');
  Route::get('/filter-badge-reporting', 'EventController@filter_badge_reporting');
  Route::get('generator', 'EventController@qrCodeGenerator');
  Route::post('/register-member-for-event', 'EventController@register_member')->name('register-member-for-event');
  Route::get('/sessions', 'EventController@sessions')->name('sessions');
  Route::get('/add-session', 'EventController@show_session_form')->name('add-session');
  Route::post('/validateSessionForm', 'EventController@validateSessionForm')->name('validateSessionForm');
  Route::get('/session/{id}', 'EventController@session')->name('session');
  Route::post('/update_session/{id}', 'EventController@update_session')->name('update_session');
  Route::get('/delete-session/{id}', 'EventController@delete_session')->name('delete-session');
  Route::get('/session-attendees/{id}', 'EventController@session_attendees')->name('session-attendees');
  Route::post('/register-member-for-session/{id}', 'EventController@register_session')->name('register-member-for-session');
  Route::get('/mail', 'MailController@compose')->name('mail');
  Route::post('/sendMail', 'MailController@dispatcha')->name('bulkMail');
  Route::get('/update-email', 'MembersController@update_mail');
  Route::get('/search-member/{Constituent_ID}', 'MembersController@search_member');
  Route::get('search-result-page', 'SearchController@index')->name('search');
  Route::get('draft', 'MailController@draft')->name('draft');
  Route::get('/accounting', 'AccountingController@index')->name('accounting');
  Route::get('/filter-members', 'AccountingController@filter')->name('filter-members');
  Route::get('/roles-permissions', 'PermissionsController@index')->name('roles-permissions');
  Route::get('/explore-users', 'UsersController@list')->name('explore-users');
  Route::post('add-payment-form', 'AccountingController@addPayment')->name('add-payment-form');
  Route::get("emailTry", "InvoiceAttachmentController@sendEmail");

  Route::group(['middleware' => ['role:super_admin']], function() {
        Route::get('add-user',['middleware' => ['permission:add-users'], 'uses' => 'UsersController@index'])->name('add-user');
        Route::post('/add-user-form', ['middleware' => ['permission:add-users'], 'uses' => 'UsersController@store'])->name('create-user');
        Route::get('/user-details/{id}', ['middleware' => ['permission:view-users'], 'uses' => 'UsersController@show'])->name('user-details');

        Route::post('/update-user/{id}', ['middleware' => ['permission:edit-users'], 'uses' => 'UsersController@update'])->name('update-user');

        Route::post('update-permission', 'PermissionsController@update');

        Route::get('fetch-permissions/{role_name}', 'PermissionsController@fetchPermissions');

  });

  Route::post('/register-as-paid', 'AccountingController@registerPaid');
  Route::post('/print-single-badge',  'EventController@singleBadgeGenerator')->name('print-single-badge');
  Route::get('/fetch-member-registration/{id}', 'EventController@fetch_member_registration')->name('fetch-member-registration');
  Route::get('/sendAttendanceJob', 'EventController@sendAttendanceJob');
  Route::get('/filter/{value}', 'EventController@filter_ru')->name('filter');
  Route::get('/member-promotions', 'MembersController@promotions')->name('member-promotions');
  Route::get('/membership', 'AccountingController@dues_types')->name('membership');
  Route::get('fetch-dues-type/{id}', 'HomeController@fetch_dues_type');
  Route::post('edit-dues-type-form', 'HomeController@update_dues_type')->name('edit-dues-type-form');
  Route::get('setting-dues-type/{id}', 'HomeController@setting_dues_view')->name('setting-dues-type');
  Route::get('unassign-membership/{cid}/{mid}', 'HomeController@unassign_membership')->name('unassign-membership');
  Route::post('bulk-promote-membership', 'MembersController@bulk_promotion')->name('bulk-promote-membership');
  Route::get('sms', 'SmsController@index')->name('sms');
  Route::post('send-sms', 'SmsController@sendSms')->name('send-sms');
  Route::post('get-sms-summary', 'SmsController@getSummary');
  Route::get('export-view', 'MembersController@export')->name('export-view');
  Route::post('send-confirm-sms', 'SmsController@sendConfirmSms');
  Route::get('get-profile-categories/{id}', 'MembersController@getProfileCategory');
  Route::get('payment', 'AccountingController@payment')->name('payment');
  Route::post('filterReport', 'AccountingController@filterReport')->name('filterReport');
  Route::get('iPay/{id}', 'AccountingController@iPay');
  Route::get('update_session_attendance', 'EventController@updateSessionAttendance');

});

Route::get('import-ecobank-payers', 'AccountingController@importEco');

Route::get('form', function(){
  return view('pages.admin.psgh-pages.ui-feature-notifications');
});
